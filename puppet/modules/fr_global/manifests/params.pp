class
	fr_global::params
{

	$dir_root = "/fr"
	$dir_bin = "${dir_root}/third-party"
	$dir_download = "${dir_root}/download"
	$dir_services = "${dir_root}/services"
	$dir_deploy = "${dir_root}/deploy"
	$dir_frlogdir = "/frlogdir"


  file{
    [
      "$dir_root",
      "$dir_bin",
      "$dir_download",
      "$dir_services",
      "$dir_deploy",
      "$dir_frlogdir"
    ]:
    ensure => "directory"
  }
  
  $unsupported = "UNSUPPORTED"

  #TODO: build this at runtime based no OS
  $sys_path = $operatingsystem ? {
     /(?i-mx:ubuntu|debian)/        => "/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/sbin",
     /(?i-mx:centos|fedora|redhat)/ => "/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/sbin",
     /(?i-mx:darwin)/ => "$fr_global::params::unsupported"
   }

}
