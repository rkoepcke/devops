class
  ds_regionrel::install
inherits
  ds_regionrel
{

  $ds_regionrel_required_paths = [
    $ds_regionrel::ds_regionrel_deploy_dir,
    "$ds_regionrel::ds_regionrel_deploy_dir/bin",
    "$ds_regionrel::ds_regionrel_deploy_dir/conf",
    "$ds_regionrel::ds_regionrel_deploy_dir/lib",
    "$ds_regionrel::ds_regionrel_deploy_dir/pllib",
    "$ds_regionrel::ds_regionrel_deploy_dir/plconf",
    $ds_regionrel::ds_regionrel_frlogdir,
    "${ds_regionrel::ds_regionrel_frlogdir}/logs",
    "${ds_regionrel::ds_regionrel_frlogdir}/dpflogs",
  ]

  file { $ds_regionrel_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_regionrel::_ds_regionrel_user,
    group  => $ds_regionrel::_ds_regionrel_group,
    before => File['fetch regionrel dbf library tar file'],
  }

  file {'fetch regionrel dbf library tar file':
     ensure  => 'file',
     require => File[$ds_regionrel_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-rr-lib.tar",
     owner   => $ds_regionrel::_ds_regionrel_user,
     group   => $ds_regionrel::_ds_regionrel_group,
     source  => "puppet:///modules/ds_regionrel/dpf-rr-lib.tar"
  }

   exec { 'extract-rr-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-rr-lib.tar --directory ${ds_regionrel_deploy_dir}/lib",
     require => File[$ds_regionrel_required_paths],
     user    => $ds_regionrel::_ds_regionrel_user,
     group   => $ds_regionrel::_ds_regionrel_group,
     path    => '/bin',
  }

  File['fetch regionrel dbf library tar file'] -> Exec['extract-rr-dpf-lib']

  file {'fetch regionrel library tar file':
     ensure  => 'file',
     require => File[$ds_regionrel_required_paths],
     path    => "${fr_global::params::dir_root}/download/regionrel-lib.tar",
     owner   => $ds_regionrel::_ds_regionrel_user,
     group   => $ds_regionrel::_ds_regionrel_group,
     source  => "puppet:///modules/ds_regionrel/regionrel-lib.tar"
  }

   exec { 'extract-rr-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/regionrel-lib.tar --directory ${ds_regionrel_deploy_dir}/pllib",
     require => File[$ds_regionrel_required_paths],
     user    => $ds_regionrel::_ds_regionrel_user,
     group   => $ds_regionrel::_ds_regionrel_group,
     path    => '/bin',
  }

  File['fetch regionrel library tar file'] -> Exec['extract-rr-lib']

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    content => template('ds_regionrel/log4j2.xml.erb'),
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    content => template('ds_regionrel/config.properties.erb'),
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0755',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    content => template('ds_regionrel/startup.sh.erb'),
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0755',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    content => template('ds_regionrel/shutdown.sh.erb'),
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/plconf/RegionRelevancyScoringPipeline.xml":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    content => template('ds_regionrel/RegionRelevancyScoringPipeline.xml.erb'),
  }


  file { "${ds_regionrel::ds_regionrel_deploy_dir}/DataProcessingFramework.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/DataProcessingFramework-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/RegionRelevancyScoringPipeline.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/RegionRelevancyScoringPipeline-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/RegionRelevancyScorer.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/RegionRelevancyScorer-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/BasicUtils.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/BasicUtils-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/lib/Solr47Utils.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/Solr47Utils-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/SQLUtils.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/SQLUtils-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/DocUtils.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/DocUtils-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/NLPUtils.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/NLPUtils-1.0-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/pllib/FRCoreNLP-3.5.2-DS.jar":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/FRCoreNLP-3.5.2-DS.jar",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/spring-config.xml",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/castormapping.xml",
  }

  file { "${ds_regionrel::ds_regionrel_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_regionrel_required_paths],
    mode    => '0644',
    owner   => $ds_regionrel::_ds_regionrel_user,
    group   => $ds_regionrel::_ds_regionrel_group,
    source  =>  "puppet:///modules/ds_regionrel/castor.properties",
  }

  file { "/etc/init.d/frregionrelevancy":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_regionrel/frregionrelevancy.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frregionrelevancy.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frregionrelevancy'],
        owner   => root,
        group   => root,
        content => template('ds_regionrel/frregionrelevancy.service.erb'),
      }
  }

}
