class
  ds_regionrel
  (
    $ds_regionrel_jdk_version = undef,
    $ds_regionrel_user = undef,
    $ds_regionrel_group = undef,
    $ds_regionrel_dsdoc_solr_url = undef,
    $ds_regionrel_sitedocid_start = undef,
    $ds_regionrel_update_api_url = undef,
    $ds_regionrel_storage_service_url = undef,
    $ds_regionrel_mysql_db = undef,
    $ds_regionrel_mysql_dbname = undef,
    $ds_regionrel_mysql_db_user = undef,
    $ds_regionrel_mysql_db_passwd = undef,
    $ds_regionrel_app_name = "frregionrelevancy",
  )
{
  include ds_regionrel::params

  notify { 'ds_regionrel deploy dir':
    message => $_deploy_dir
  }

  if $ds_regionrel::ds_regionrel_jdk_version == undef {
    $_ds_regionrel_jdk_version = $ds_regionrel::params::ds_regionrel_jdk_version
  } else {
    $_ds_regionrel_jdk_version = $ds_regionrel::ds_regionrel_jdk_version
  }

  if $ds_regionrel::ds_regionrel_user == undef {
    $_ds_regionrel_user = $ds_regionrel::params::ds_regionrel_user
  } else {
    $_ds_regionrel_user = $ds_regionrel::ds_regionrel_user
  }

  if $ds_regionrel::ds_regionrel_group == undef {
    $_ds_regionrel_group = $ds_regionrel::params::ds_regionrel_group
  } else {
    $_ds_regionrel_group = $ds_regionrel::ds_regionrel_group
  }

  if $ds_regionrel::ds_regionrel_update_api_url == undef {
    $_ds_regionrel_update_api_url = $ds_regionrel::params::ds_regionrel_update_api_url
  } else {
    $_ds_regionrel_update_api_url = $ds_regionrel::ds_regionrel_update_api_url
  }

  if $ds_regionrel::ds_regionrel_storage_service_url == undef {
    $_ds_regionrel_storage_service_url = $ds_regionrel::params::ds_regionrel_storage_service_url
  } else {
    $_ds_regionrel_storage_service_url = $ds_regionrel::ds_regionrel_storage_service_url
  }

  if $ds_regionrel::ds_regionrel_dsdoc_solr_url == undef {
    $_ds_regionrel_dsdoc_solr_url = $ds_regionrel::params::ds_regionrel_dsdoc_solr_url
  } else {
    $_ds_regionrel_dsdoc_solr_url = $ds_regionrel::ds_regionrel_dsdoc_solr_url
  }

  if $ds_regionrel::ds_regionrel_sitedocid_start == undef {
    $_ds_regionrel_sitedocid_start = $ds_regionrel::params::ds_regionrel_sitedocid_start
  } else {
    $_ds_regionrel_sitedocid_start = $ds_regionrel::ds_regionrel_sitedocid_start
  }

  if $ds_regionrel::ds_regionrel_mysql_db == undef {
    $_ds_regionrel_mysql_db = $ds_regionrel::params::ds_regionrel_mysql_db
  } else {
    $_ds_regionrel_mysql_db = $ds_regionrel::ds_regionrel_mysql_db
  }

  if $ds_regionrel::ds_regionrel_mysql_dbname == undef {
    $_ds_regionrel_mysql_dbname = $ds_regionrel::params::ds_regionrel_mysql_dbname
  } else {
    $_ds_regionrel_mysql_dbname = $ds_regionrel::ds_regionrel_mysql_dbname
  }

  if $ds_regionrel::ds_regionrel_mysql_db_user == undef {
    $_ds_regionrel_mysql_db_user = $ds_regionrel::params::ds_regionrel_mysql_db_user
  } else {
    $_ds_regionrel_mysql_db_user = $ds_regionrel::ds_regionrel_mysql_db_user
  }

  if $ds_regionrel::ds_regionrel_mysql_db_passwd == undef {
    $_ds_regionrel_mysql_db_passwd = $ds_regionrel::params::ds_regionrel_mysql_db_passwd
  } else {
    $_ds_regionrel_mysql_db_passwd = $ds_regionrel::ds_regionrel_mysql_db_passwd
  }

  $ds_regionrel_deploy_dir = "/fr/deploy/${ds_regionrel_app_name}"
  $ds_regionrel_frlogdir = "/frlogdir/${ds_regionrel_app_name}"
  $ds_regionrel_jdk_dir = "/fr/third-party/${_ds_regionrel_jdk_version}"
  $ds_regionrel_pl_lib = "${ds_regionrel_deploy_dir}/pllib"
  $ds_regionrel_pl_conf = "${ds_regionrel_deploy_dir}/plconf"

  class{'ds_regionrel::install':
    require => Class['fr_global::params'],
  }
}
