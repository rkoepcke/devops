class
  tomcat
  (
    $deploy_dir     = undef,
    $tomcat_jdk_version = undef,
    $version_major  = undef,
    $version_minor  = undef,
    $version_patch  = undef,
    $tomcat_http_port = undef,
    $tomcat_https_port = undef,
    $tomcat_shutdown_port = undef,
    $tomcat_ajp_port = undef,
  )
{
  include tomcat::params

  if $version_major == undef {
    $tc_version_major = $tomcat::params::version_major
  } else {
    $tc_version_major = $version_major
  }

  if $version_minor == undef {
    $tc_version_minor = $tomcat::params::version_minor
  } else {
    $tc_version_minor = $version_minor
  }

  if $version_patch == undef {
    $tc_version_patch = $tomcat::params::version_patch
  } else {
    $tc_version_patch = $version_patch
  }

  if $tomcat_jdk_version == undef {
    $_tomcat_jdk_version = $tomcat::params::tomcat_jdk_version
  } else {
    $_tomcat_jdk_version = $tomcat_jdk_version
  }

  $version = "${tc_version_major}.${tc_version_minor}.${tc_version_patch}"
  $package = "apache-tomcat-${version}.tar.gz"

  notify { 'tomcat version set':
    name    =>  'Setting Tomcat version',
    message =>  $tomcat::package
  }

  if $deploy_dir == undef {
    $_deploy_dir = "${tomcat::params::deploy_dir_default}/apache-tomcat-${version}"
    $tomcat_deploy_dir = $tomcat::params::deploy_dir_default
  }else{
    $_deploy_dir = "${deploy_dir}/apache-tomcat-${version}"
    $tomcat_deploy_dir = $deploy_dir
  }

  notify { 'tomcat deploy dir':
    message => $_deploy_dir
  }

  notify { 'jdk version set':
    name    =>  'Setting JDK version',
    message =>  $_tomcat_jdk_version
  }

  if $tomcat::tomcat_shutdown_port == undef {
    $_tomcat_shutdown_port = $tomcat::params::tomcat_shutdown_port
  } else {
    $_tomcat_shutdown_port = $tomcat::tomcat_shutdown_port
  }

  if $tomcat::tomcat_http_port == undef {
    $_tomcat_http_port = $tomcat::params::tomcat_http_port
  } else {
    $_tomcat_http_port = $tomcat::tomcat_http_port
  }

  if $tomcat::tomcat_https_port == undef {
    $_tomcat_https_port = $tomcat::params::tomcat_https_port
  } else {
    $_tomcat_https_port = $tomcat::tomcat_https_port
  }

  if $tomcat::tomcat_ajp_port == undef {
    $_tomcat_ajp_port = $tomcat::params::tomcat_ajp_port
  } else {
    $_tomcat_ajp_port = $tomcat::tomcat_ajp_port
  }

  $tomcat_conf = "${_deploy_dir}/conf"
  $tomcat_bin = "${_deploy_dir}/bin"

  class{'tomcat::install':
    require => Class['fr_global::params'],
  }

}
