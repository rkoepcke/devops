class
  tomcat::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $version_major = 7
  $version_minor = 0
  $version_patch = 47
  $tomcat_jdk_version = 'jdk1.6.0_30'

  $tomcat_shutdown_port = 8005
  $tomcat_http_port = 8080
  $tomcat_https_port = 8443
  $tomcat_ajp_port = 8009
}
