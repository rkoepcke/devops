class
  tomcat::install
inherits
  tomcat
{
  notify { 'starting-tomcat-fetch':
    name    =>  'STARTING Tomcat fetch',
    message =>  "${tomcat::tomcat_deploy_dir}/${tomcat::package}"
  }

  notify { 'finished-tomcat-fetch':
    name    =>  'FINISHED Tomcat fetch',
    message =>  "${tomcat::tomcat_deploy_dir}/${tomcat::package}"
  }

  file { 'fetch tomcat binary archive':
    ensure  => file,
    replace => no,
    path    => "${fr_global::params::dir_download}/${tomcat::package}",
    source  => "puppet:///modules/tomcat/${tomcat::package}",
    before  => Exec['extract-tomcat'],
  }

  $tomcat_source = "${fr_global::params::dir_download}/${tomcat::package}"
  $tomcat_target = $tomcat::tomcat_deploy_dir

  notify{$tomcat::tomcat_deploy_dir:}

  exec { 'extract-tomcat':
    command => "tar -xzf ${tomcat_source} --directory ${tomcat_target}",
    path    => '/bin',
    unless  => "/usr/bin/test -d ${tomcat::_deploy_dir}"
  }

  file { "${tomcat::tomcat_conf}/server.xml":
    ensure  => file,
    require => Exec['extract-tomcat'],
    content => template('tomcat/tc_server.xml.erb'),
  }

  file { "${tomcat::tomcat_bin}/setenv.sh":
    ensure  => file,
    require => Exec['extract-tomcat'],
    mode    => '0755',
    content => template('tomcat/tc_setenv.sh.erb'),
  }
}
