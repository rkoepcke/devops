class
  ds_docs::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_docs_jdk_version = "1.8.0_66"
  $ds_docs_user = "serviceprod"
  $ds_docs_group = "engops"
  $ds_docs_doc_solr_url = "http://sindex1-rl.ca.firstrain.net:8080/solr/doc"
}
