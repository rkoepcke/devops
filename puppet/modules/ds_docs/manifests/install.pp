class
  ds_docs::install
inherits
  ds_docs
{

  $ds_docs_required_paths = [
    $ds_docs::ds_docs_deploy_dir,
    "$ds_docs::ds_docs_deploy_dir/bin",
    "$ds_docs::ds_docs_deploy_dir/conf",
    "$ds_docs::ds_docs_deploy_dir/lib",
    "$ds_docs::ds_docs_deploy_dir/pllib",
    "$ds_docs::ds_docs_deploy_dir/plconf",
    $ds_docs::ds_docs_frlogdir,
    "${ds_docs::ds_docs_frlogdir}/logs",
    "${ds_docs::ds_docs_frlogdir}/dpflogs",
  ]

  file { $ds_docs_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_docs::_ds_docs_user,
    group  => $ds_docs::_ds_docs_group,
    before => File['fetch dbf library tar file'],
  }

  file {'fetch dbf library tar file':
     ensure  => 'file',
     require => File[$ds_docs_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_docs::_ds_docs_user,
     group   => $ds_docs::_ds_docs_group,
     source  => "puppet:///modules/ds_docs/dpf-lib.tar"
  }

   exec { 'extract-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_docs_deploy_dir}/lib",
     require => File[$ds_docs_required_paths],
     user    => $ds_docs::_ds_docs_user,
     group   => $ds_docs::_ds_docs_group,
     path    => '/bin',
  }

  File['fetch dbf library tar file'] -> Exec['extract-dpf-lib']

  file { "${ds_docs::ds_docs_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/log4j2.xml.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/config.properties.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0755',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/startup.sh.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0755',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/shutdown.sh.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/plconf/DSDocs_Pipeline.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/DSDocs_Pipeline.xml.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/plconf/DSDocsTagger_Pipeline.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/DSDocsTagger_Pipeline.xml.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/plconf/DSDocsUpdater_Pipeline.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    content => template('ds_docs/DSDocsUpdater_Pipeline.xml.erb'),
  }

  file { "${ds_docs::ds_docs_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/dpf-1.2.jar",
  }


  file { "${ds_docs::ds_docs_deploy_dir}/pllib/DSDocsPipeline.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/DSDocsPipeline-1.0-DS.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/basicutils-1.0.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/basicutils-1.0.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/dsdatacache.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/dsdatacache.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/DSDocsTaggerPipeline.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/DSDocsTaggerPipeline-1.0-DS.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/DSDocsUpdaterPipeline.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/DSDocsUpdaterPipeline-1.0-DS.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/nlputils-2.0.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/nlputils-2.0.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/pllib/fr-corenlp3.5.2-1.0.jar":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/fr-corenlp3.5.2-1.0.jar",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/spring-config.xml",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/castormapping.xml",
  }

  file { "${ds_docs::ds_docs_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_docs_required_paths],
    mode    => '0644',
    owner   => $ds_docs::_ds_docs_user,
    group   => $ds_docs::_ds_docs_group,
    source  =>  "puppet:///modules/ds_docs/castor.properties",
  }

  file { "/etc/init.d/frUpdateDSDocs":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_docs/frUpdateDSDocs.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frUpdateDSDocs.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frUpdateDSDocs'],
        owner   => root,
        group   => root,
        content => template('ds_docs/frUpdateDSDocs.service.erb'),
      }
  }

}
