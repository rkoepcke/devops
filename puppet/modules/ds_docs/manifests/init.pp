class
  ds_docs
  (
    $ds_docs_jdk_version = undef,
    $ds_docs_user = undef,
    $ds_docs_group = undef,
    $ds_docs_mi32_db = undef,
    $ds_docs_mi32_user = undef,
    $ds_docs_mi32_pw = undef,
    $ds_docs_eng_solr_url = undef,
    $ds_docs_entity_solr_url = undef,
    $ds_docs_target_solr_url = undef,
    $ds_docs_minute_id_start = undef,
    $ds_docs_site_doc_id_start = undef,
    $ds_docs_jmxport = undef,
    $ds_docs_app_name = "frUpdateDSDocs",
  )
{
  include ds_docs::params

  notify { 'ds_docs deploy dir':
    message => $_deploy_dir
  }

  if $ds_docs::ds_docs_jdk_version == undef {
    $_ds_docs_jdk_version = $ds_docs::params::ds_docs_jdk_version
  } else {
    $_ds_docs_jdk_version = $ds_docs::ds_docs_jdk_version
  }

  if $ds_docs::ds_docs_user == undef {
    $_ds_docs_user = $ds_docs::params::ds_docs_user
  } else {
    $_ds_docs_user = $ds_docs::ds_docs_user
  }

  if $ds_docs::ds_docs_group == undef {
    $_ds_docs_group = $ds_docs::params::ds_docs_group
  } else {
    $_ds_docs_group = $ds_docs::ds_docs_group
  }

  if $ds_docs::ds_docs_mi32_db == undef {
    $_ds_docs_mi32_db = $ds_docs::params::ds_docs_mi32_db
  } else {
    $_ds_docs_mi32_db = $ds_docs::ds_docs_mi32_db
  }

  if $ds_docs::ds_docs_mi32_user == undef {
    $_ds_docs_mi32_user = $ds_docs::params::ds_docs_mi32_user
  } else {
    $_ds_docs_mi32_user = $ds_docs::ds_docs_mi32_user
  }

  if $ds_docs::ds_docs_mi32_pw == undef {
    $_ds_docs_mi32_pw = $ds_docs::params::ds_docs_mi32_pw
  } else {
    $_ds_docs_mi32_pw = $ds_docs::ds_docs_mi32_pw
  }

  if $ds_docs::ds_docs_eng_solr_url == undef {
    $_ds_docs_eng_solr_url = $ds_docs::params::ds_docs_eng_solr_url
  } else {
    $_ds_docs_eng_solr_url = $ds_docs::ds_docs_eng_solr_url
  }

  if $ds_docs::ds_docs_entity_solr_url == undef {
    $_ds_docs_entity_solr_url = $ds_docs::params::ds_docs_entity_solr_url
  } else {
    $_ds_docs_entity_solr_url = $ds_docs::ds_docs_entity_solr_url
  }

  if $ds_docs::ds_docs_target_solr_url == undef {
    $_ds_docs_target_solr_url = $ds_docs::params::ds_docs_target_solr_url
  } else {
    $_ds_docs_target_solr_url = $ds_docs::ds_docs_target_solr_url
  }

  if $ds_docs::ds_docs_minute_id_start == undef {
    $_ds_docs_minute_id_start = $ds_docs::params::ds_docs_minute_id_start
  } else {
    $_ds_docs_minute_id_start = $ds_docs::ds_docs_minute_id_start
  }

  if $ds_docs::ds_docs_site_doc_id_start == undef {
    $_ds_docs_site_doc_id_start = $ds_docs::params::ds_docs_site_doc_id_start
  } else {
    $_ds_docs_site_doc_id_start = $ds_docs::ds_docs_site_doc_id_start
  }

  if $ds_docs::ds_docs_jmxport == undef {
    $_ds_docs_jmxport = $ds_docs::params::ds_docs_jmxport
  } else {
    $_ds_docs_jmxport = $ds_docs::ds_docs_jmxport
  }

  $ds_docs_deploy_dir = "/fr/deploy/${ds_docs_app_name}"
  $ds_docs_frlogdir = "/frlogdir/${ds_docs_app_name}"
  $ds_docs_jdk_dir = "/fr/third-party/${_ds_docs_jdk_version}"
  $ds_docs_pl_lib = "${ds_docs_deploy_dir}/pllib"
  $ds_docs_pl_conf = "${ds_docs_deploy_dir}/plconf"

  class{'ds_docs::install':
    require => Class['fr_global::params'],
  }
}
