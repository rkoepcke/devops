class
  ds_regiontag
  (
    $ds_regiontag_jdk_version = undef,
    $ds_regiontag_user = undef,
    $ds_regiontag_group = undef,
    $ds_regiontag_mi32_db = undef,
    $ds_regiontag_mi32_user = undef,
    $ds_regiontag_mi32_pw = undef,
    $ds_regiontag_neo_url = undef,
    $ds_regiontag_api_url = undef,
    $ds_regiontag_doc_solr_url = undef,
    $ds_regiontag_jmxport = undef,
    $ds_regiontag_app_name = "frregiontagging",
  )
{
  include ds_regiontag::params

  notify { 'ds_regiontag deploy dir':
    message => $_deploy_dir
  }

  if $ds_regiontag::ds_regiontag_jdk_version == undef {
    $_ds_regiontag_jdk_version = $ds_regiontag::params::ds_regiontag_jdk_version
  } else {
    $_ds_regiontag_jdk_version = $ds_regiontag::ds_regiontag_jdk_version
  }

  if $ds_regiontag::ds_regiontag_user == undef {
    $_ds_regiontag_user = $ds_regiontag::params::ds_regiontag_user
  } else {
    $_ds_regiontag_user = $ds_regiontag::ds_regiontag_user
  }

  if $ds_regiontag::ds_regiontag_group == undef {
    $_ds_regiontag_group = $ds_regiontag::params::ds_regiontag_group
  } else {
    $_ds_regiontag_group = $ds_regiontag::ds_regiontag_group
  }

  if $ds_regiontag::ds_regiontag_mi32_db == undef {
    $_ds_regiontag_mi32_db = $ds_regiontag::params::ds_regiontag_mi32_db
  } else {
    $_ds_regiontag_mi32_db = $ds_regiontag::ds_regiontag_mi32_db
  }

  if $ds_regiontag::ds_regiontag_mi32_user == undef {
    $_ds_regiontag_mi32_user = $ds_regiontag::params::ds_regiontag_mi32_user
  } else {
    $_ds_regiontag_mi32_user = $ds_regiontag::ds_regiontag_mi32_user
  }

  if $ds_regiontag::ds_regiontag_mi32_pw == undef {
    $_ds_regiontag_mi32_pw = $ds_regiontag::params::ds_regiontag_mi32_pw
  } else {
    $_ds_regiontag_mi32_pw = $ds_regiontag::ds_regiontag_mi32_pw
  }

  if $ds_regiontag::ds_regiontag_neo_url == undef {
    $_ds_regiontag_neo_url = $ds_regiontag::params::ds_regiontag_neo_url
  } else {
    $_ds_regiontag_neo_url = $ds_regiontag::ds_regiontag_neo_url
  }

  if $ds_regiontag::ds_regiontag_api_url == undef {
    $_ds_regiontag_api_url = $ds_regiontag::params::ds_regiontag_api_url
  } else {
    $_ds_regiontag_api_url = $ds_regiontag::ds_regiontag_api_url
  }

  if $ds_regiontag::ds_regiontag_doc_solr_url == undef {
    $_ds_regiontag_doc_solr_url = $ds_regiontag::params::ds_regiontag_doc_solr_url
  } else {
    $_ds_regiontag_doc_solr_url = $ds_regiontag::ds_regiontag_doc_solr_url
  }

  if $ds_regiontag::ds_regiontag_jmxport == undef {
    $_ds_regiontag_jmxport = $ds_regiontag::params::ds_regiontag_jmxport
  } else {
    $_ds_regiontag_jmxport = $ds_regiontag::ds_regiontag_jmxport
  }

  $ds_regiontag_deploy_dir = "/fr/deploy/${ds_regiontag_app_name}"
  $ds_regiontag_frlogdir = "/frlogdir/${ds_regiontag_app_name}"
  $ds_regiontag_jdk_dir = "/fr/third-party/${_ds_regiontag_jdk_version}"
  $ds_regiontag_pl_lib = "${ds_regiontag_deploy_dir}/pllib"
  $ds_regiontag_pl_conf = "${ds_regiontag_deploy_dir}/plconf"

  class{'ds_regiontag::install':
    require => Class['fr_global::params'],
  }
}
