class
  ds_regiontag::install
inherits
  ds_regiontag
{

  $ds_regiontag_required_paths = [
    $ds_regiontag::ds_regiontag_deploy_dir,
    "$ds_regiontag::ds_regiontag_deploy_dir/bin",
    "$ds_regiontag::ds_regiontag_deploy_dir/conf",
    "$ds_regiontag::ds_regiontag_deploy_dir/lib",
    "$ds_regiontag::ds_regiontag_deploy_dir/pllib",
    "$ds_regiontag::ds_regiontag_deploy_dir/plconf",
    $ds_regiontag::ds_regiontag_frlogdir,
    "${ds_regiontag::ds_regiontag_frlogdir}/logs",
    "${ds_regiontag::ds_regiontag_frlogdir}/dpflogs",
  ]

  file { $ds_regiontag_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_regiontag::_ds_regiontag_user,
    group  => $ds_regiontag::_ds_regiontag_group,
    before => File['fetch dbf rt library tar file'],
  }

  file {'fetch dbf rt library tar file':
     ensure  => 'file',
     require => File[$ds_regiontag_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-rt-lib.tar",
     owner   => $ds_regiontag::_ds_regiontag_user,
     group   => $ds_regiontag::_ds_regiontag_group,
     source  => "puppet:///modules/ds_regiontag/dpf-rt-lib.tar"
  }

  exec { 'extract-dpf-rt-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-rt-lib.tar --directory ${ds_regiontag_deploy_dir}/lib",
     require => File[$ds_regiontag_required_paths],
     user    => $ds_regiontag::_ds_regiontag_user,
     group   => $ds_regiontag::_ds_regiontag_group,
     path    => '/bin',
  }

  File['fetch dbf rt library tar file'] -> Exec['extract-dpf-rt-lib']

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    content => template('ds_regiontag/log4j2.xml.erb'),
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    content => template('ds_regiontag/config.properties.erb'),
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0755',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    content => template('ds_regiontag/startup.sh.erb'),
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0755',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    content => template('ds_regiontag/shutdown.sh.erb'),
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/plconf/RegionFiltersPipeline.xml":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    content => template('ds_regiontag/RegionFiltersPipeline.xml.erb'),
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/dpf-1.2.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/RegionTaggingFilters.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/RegionTaggingFilters-1.0.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/commons-codec-1.7.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/commons-codec-1.7.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/commons-httpclient-3.0.1.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/commons-httpclient-3.0.1.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/commons-io-2.1.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/commons-io-2.1.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/HtmlParser.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/HtmlParser.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/nlputils-1.0.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/nlputils-1.0.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/pllib/stanford-corenlp-3.5.2.jar":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/stanford-corenlp-3.5.2.jar",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/spring-config.xml",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/castormapping.xml",
  }

  file { "${ds_regiontag::ds_regiontag_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_regiontag_required_paths],
    mode    => '0644',
    owner   => $ds_regiontag::_ds_regiontag_user,
    group   => $ds_regiontag::_ds_regiontag_group,
    source  =>  "puppet:///modules/ds_regiontag/castor.properties",
  }

  file { "/etc/init.d/frregiontagging":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_regiontag/frregiontagging.erb'),
  }

   if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frregiontagging.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frregiontagging'],
        owner   => root,
        group   => root,
        content => template('ds_regiontag/frregiontagging.service.erb'),
      }
  }

}
