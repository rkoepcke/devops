# Install class for the SOLR 4.7.2 install
class
  solr472::install
inherits
  solr472
{

  notify { 'Solr-Data-Dir':
    message =>  $solr472::solr472_data_dir
  }

  notify { 'Solr-Log-Dir':
    message =>  $solr472::solr472_logs
  }

  $solr472_required_paths = [
    $solr472::_solr472_deploy_dir,
    $solr472::_solr472_services_dir,
    "${solr472::_solr472_services_dir}/bin",
    "${solr472::_solr472_services_dir}/logs",
    "${solr472::_solr472_services_dir}/conf",
    "${solr472::_solr472_services_dir}/conf/Catalina",
    "${solr472::_solr472_services_dir}/conf/Catalina/localhost",
    "${solr472::_solr472_services_dir}/lib",
    "${solr472::_solr472_services_dir}/temp",
    "${solr472::_solr472_services_dir}/work",
    $solr472::_solr472_frlogdir,
    "${solr472::_solr472_frlogdir}/logs",
    "${solr472::_solr472_frlogdir}/logs/accesslogs",
    $solr472::_solr472_data_dir,
  ]

  file { $solr472_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $solr472::_solr472_tomcat_user,
    group  => $solr472::_solr472_tomcat_group,
    before => Notify['finished-solr472-war-fetch']
  }

  notify { 'start-solr472-war-fetch':
    name    =>  'STARTING Solr War Fetch',
    message =>  "${fr_global::params::dir_download}/solr-4.7.2.war"
  }

  notify { 'finished-solr472-war-fetch':
    name    =>  'FINISHED Solr War Fetch',
    message =>  "${fr_global::params::dir_download}/solr-4.7.2.war}"
  }

  file { 'fetch solr472 war file':
    ensure  =>  'file',
    require =>  File[$solr472_required_paths],
    path    =>  "${solr472::_solr472_deploy_dir}/solr.war",
    owner   =>  $solr472::_solr472_tomcat_user,
    group   =>  $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/solr-4.7.2.war",
    before  =>  Notify['FINISHED Solr War Fetch']
  }

  exec { 'copy-tomcat-bin-files-for-solr':
    command => "cp ${solr472::_solr472_tomcat_home_dir}/bin/*.sh ${solr472::solr472_bin}",
    require => [File[$solr472_required_paths],Class['tomcat::install']],
    user    => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${solr472::solr472_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files-for-solr':
    command => "cp ${solr472::_solr472_tomcat_home_dir}/bin/*.jar ${solr472::solr472_bin}",
    require => [File[$solr472_required_paths],Class['tomcat::install']],
    user    => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${solr472::solr472_bin}/bootstrap.jar",
  }

  file { 'fetch solr472 catalina properties file':
    ensure  =>  'file',
    require =>  File[$solr472_required_paths],
    path    =>  "${solr472::solr472_conf}/catalina.properties",
    owner   =>  $solr472::_solr472_tomcat_user,
    group   =>  $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/catalina.properties",
  }

  file { "${solr472::solr472_conf}/server.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    content => template('solr472/server.xml.erb'),
  }

  file { "${solr472::solr472_conf}/log4j.properties":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    content => template('solr472/log4j.properties.erb'),
  }

  file { "${solr472::solr472_conf}/logging.properties":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    content => template('solr472/logging.properties.erb'),
  }

  file { "${solr472::solr472_conf}/catalina.policy":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/catalina.policy",
  }

  file { "${solr472::solr472_conf}/context.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/context.xml",
  }

  file { "${solr472::solr472_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/tomcat-users.xml",
  }

  file { "${solr472::solr472_conf}/web.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    source  =>  "puppet:///modules/solr472/web.xml",
  }

  file { "${solr472::solr472_conf}/Catalina/localhost/solr.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    content => template('solr472/solr.xml.erb'),
  }

  file { "${solr472::_solr472_data_dir}/solr.xml":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0644',
    owner  => $solr472::_solr472_tomcat_user,
    group  => $solr472::_solr472_tomcat_group,
    content => template('solr472/solr-home.xml.erb'),
  }

  file { "${solr472::solr472_bin}/setenv.sh":
    ensure  => file,
    require => File[$solr472_required_paths],
    mode    => '0755',
    owner   => $solr472::_solr472_tomcat_user,
    group   => $solr472::_solr472_tomcat_group,
    content => template('solr472/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/${solr472::_solr472_app_name}":
    ensure  => file,
    mode    => '0755',
    content => template('solr472/frsolrtomcat472.erb'),
  }

   if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frsolrtomcat472.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frsolrtomcat472'],
        owner   => root,
        group   => root,
        content => template('solr472/frsolrtomcat472.service.erb'),
      }
  }

}
