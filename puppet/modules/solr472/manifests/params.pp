# Parameter class for the SOLR module.
class
  solr472::params
{
  include fr_global::params

  # Needs to be a global param at some point
  $solr472_data_dir = '/frdata/repository'

  $deploy_dir_default = $fr_global::params::dir_deploy
  $services_dir_default = $fr_global::params::dir_services
  $frlogdir_default = $fr_global::params::dir_frlogdir
  $solr472_jdk_version_default = 'jdk1.8.0_66'
  $solr472_tp_dir_default = '/fr/third-party'
  $tomcat_name_default = 'apache-tomcat-7.0.47'
  $solr472_max_memory = '4096M'
  $solr472_min_memory = '1024M'

  $solr472_tomcat_home_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}"
  $solr472_tomcat_bin_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}/bin"
  $solr472_tomcat_lib_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}/lib"

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $solr472_shutdown_port = 8005
  $solr472_http_port = 8080
  $solr472_https_port = 8443
  $solr472_ajp_port = 8009
}
