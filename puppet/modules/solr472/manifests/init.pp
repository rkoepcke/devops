# init class for SOLR module.
class
  solr472
  (
    $solr472_jdk_version = undef,
    $solr472_tp_dir = undef,
    $solr472_deploy_dir  = undef,
    $solr472_services_dir = undef,
    $solr472_frlogdir    = undef,
    $solr472_tomcat_user = undef,
    $solr472_tomcat_group = undef,
    $solr472_tomcat_home_dir = undef,
    $solr472_app_name = undef,
    $solr472_data_dir = undef,
    $solr472_http_port = undef,
    $solr472_https_port = undef,
    $solr472_shutdown_port = undef,
    $solr472_ajp_port = undef,
    $solr472_max_memory = undef,
    $solr472_min_memory = undef,
  )
{
  include solr472::params

  if $solr472_app_name == undef {
    $_solr472_app_name = "frsolrtomcat472"
  } else {
    $_solr472_app_name = $solr472_app_name
  }

  if $solr472_deploy_dir == undef {
    $_solr472_deploy_dir = "${solr472::params::deploy_dir_default}/${_solr472_app_name}"
  }else {
    $_solr472_deploy_dir = "${solr472_deploy_dir}/${_solr472_app_name}"
  }

  if $solr472_services_dir == undef {
    $_solr472_services_dir = "${solr472::params::services_dir_default}/${_solr472_app_name}"
  } else {
    $_solr472_services_dir = "${solr472_services_dir}/${_solr472_app_name}"
  }

  if $solr472_frlogdir == undef {
    $_solr472_frlogdir = "${solr472::params::frlogdir_default}/${_solr472_app_name}"
  } else {
    $_solr472_frlogdir = "${solr472_frlogdir}/${_solr472_app_name}"
  }

  if $solr472_tp_dir == undef {
    $_solr472_tp_dir = $solr472::params::solr472_tp_dir_default
  } else {
    $_solr472_tp_dir = $solr472_tp_dir
  }

  if $solr472_jdk_version == undef {
    $_solr472_jdk_version = $solr472::params::solr472_jdk_version_default
  } else {
    $_solr472_jdk_version = $solr472_jdk_version
  }

  if $solr472_http_port==undef {
    $_solr472_http_port = $solr472::params::solr472_http_port
  } else {
    $_solr472_http_port = $solr472_http_port
  }

  if $solr472_https_port == undef {
    $_solr472_https_port = $solr472::params::solr472_https_port
  } else {
    $_solr472_https_port = $solr472_https_port
  }

  if $solr472_ajp_port==undef {
    $_solr472_ajp_port = $solr472::params::solr472_ajp_port
  } else {
    $_solr472_ajp_port = $solr472_ajp_port
  }

  if $solr472_shutdown_port==undef {
    $_solr472_shutdown_port = $solr472::params::solr472_shutdown_port
  } else {
    $_solr472_shutdown_port = $solr472_shutdown_port
  }

  if $solr472_tomcat_user == undef {
    $_solr472_tomcat_user = 'tomcat'
  } else {
    $_solr472_tomcat_user = $solr472_tomcat_user
  }

  if $solr472_tomcat_group == undef {
    $_solr472_tomcat_group = 'tomcat'
  } else {
    $_solr472_tomcat_group = $solr472_tomcat_group
  }

  if $solr472_max_memory == undef {
    $_solr472_max_memory = $solr472::params::solr472_jmx_port
  } else {
    $_solr472_max_memory = $solr472_max_memory
  }

  if $solr472_min_memory == undef {
    $_solr472_min_memory = $solr472::params::solr472_jmx_port
  } else {
    $_solr472_min_memory = $solr472_min_memory
  }

  if $solr472_data_dir == undef {
    $_solr472_data_dir = $solr472::params::solr472_data_dir
  } else {
    $_solr472_data_dir = $solr472_data_dir
  }

  if $solr472_tomcat_home_dir == undef {
    $_solr472_tomcat_home_dir = $solr472::params::solr472_tomcat_home_dir
  } else {
    $_solr472_tomcat_home_dir = $solr472_tomcat_home_dir
  }

  $solr472_jdk_dir = "/fr/third-party/${_solr472_jdk_version}"
  $solr472_conf = "${_solr472_services_dir}/conf"
  $solr472_bin = "${_solr472_services_dir}/bin"
  $solr472_lib = "${_solr472_services_dir}/lib"
  $solr472_logs = "${_solr472_frlogdir}/logs"
  $solr472_docroot = $_solr472_deploy_dir
  $solr472_url = "${::fqdn}:${_solr472_http_port}"

  class{'solr472::install':
    require => Class['fr_global::params'],
  }
}
