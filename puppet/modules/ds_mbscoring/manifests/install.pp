class
  ds_mbscoring::install
inherits
  ds_mbscoring
{

  $ds_mbscoring_required_paths = [
    $ds_mbscoring::ds_mbscoring_deploy_dir,
    "$ds_mbscoring::ds_mbscoring_deploy_dir/bin",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella/companyRelevancy",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella/creditRelevancy",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella/genx1",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella/publisherClassifier",
    "$ds_mbscoring::ds_mbscoring_deploy_dir/webApi/fantastic-umbrella/sourceDetector",
    $ds_mbscoring::ds_mbscoring_frlogdir,
    "${ds_mbscoring::ds_mbscoring_frlogdir}/logs",
  ]

  file { $ds_mbscoring_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_mbscoring::_ds_mbscoring_user,
    group  => $ds_mbscoring::_ds_mbscoring_group,
  }

  file { "${ds_mbscoring::ds_mbscoring_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_mbscoring_required_paths],
    mode    => '0755',
    owner   => $ds_mbscoring::_ds_mbscoring_user,
    group   => $ds_mbscoring::_ds_mbscoring_group,
    content => template('ds_mbscoring/startup.sh.erb'),
  }

  file { "${ds_mbscoring::ds_mbscoring_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_mbscoring_required_paths],
    mode    => '0755',
    owner   => $ds_mbscoring::_ds_mbscoring_user,
    group   => $ds_mbscoring::_ds_mbscoring_group,
    content => template('ds_mbscoring/shutdown.sh.erb'),
  }

  file { "${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/app.py":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/app.py"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/companyRelevancy/model":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/companyRelevancy/model"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/companyRelevancy/selected_vocab":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/companyRelevancy/selected_vocab"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/companyRelevancy/tfidf_transformer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/companyRelevancy/tfidf_transformer"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/creditRelevancy/model":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/creditRelevancy/model"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/creditRelevancy/count_vect":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/creditRelevancy/count_vect"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/creditRelevancy/tfidf_transformer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/creditRelevancy/tfidf_transformer"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/creditRelevancy/scorer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/creditRelevancy/scorer"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/genx1/model":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/genx1/model"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/genx1/count_vect":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/genx1/count_vect"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/genx1/tfidf_transformer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/genx1/tfidf_transformer"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/publisherClassifier/model":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/publisherClassifier/model"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/publisherClassifier/count_vect":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/publisherClassifier/count_vect"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/publisherClassifier/tfidf_transformer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/publisherClassifier/tfidf_transformer"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/sourceDetector/model":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/sourceDetector/model"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/sourceDetector/selected_vocab":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/sourceDetector/selected_vocab"
  }

  file {"${ds_mbscoring::ds_mbscoring_deploy_dir}/webApi/fantastic-umbrella/sourceDetector/tfidf_transformer":
     ensure  => 'file',
     require => File[$ds_mbscoring_required_paths],
     mode    => '0644',
     owner   => $ds_mbscoring::_ds_mbscoring_user,
     group   => $ds_mbscoring::_ds_mbscoring_group,
     source  => "puppet:///modules/ds_mbscoring/sourceDetector/tfidf_transformer"
  }

  file { "/etc/init.d/frmodelbasedscoring":
    ensure  => file,
    require => File[$ds_mbscoring_required_paths],
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_mbscoring/frmodelbasedscoring.erb'),
  }

  file { "/etc/systemd/system/frmodelbasedscoring.service":
    ensure  => file,
    require => File[$ds_mbscoring_required_paths],
    mode    => '0644',
    owner   => root,
    group   => root,
    content => template('ds_mbscoring/frmodelbasedscoring.service.erb'),
  }

}
