class
  ds_mbscoring::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_mbscoring_user = "serviceprod"
  $ds_mbscoring_group = "engops"
  $ds_mbscoring_anaconda_homedir = "/fr/third-party/anaconda2"
}
