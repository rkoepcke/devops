class
  ds_mbscoring
  (
    $ds_mbscoring_user = undef,
    $ds_mbscoring_group = undef,
    $ds_anaconda_homedir = undef,
    $ds_mbscoring_app_name = "frmodelbasedscoring",
  )
{
  include ds_mbscoring::params

  if $ds_mbscoring::ds_mbscoring_user == undef {
    $_ds_mbscoring_user = $ds_mbscoring::params::ds_mbscoring_user
  } else {
    $_ds_mbscoring_user = $ds_mbscoring::ds_mbscoring_user
  }

  if $ds_mbscoring::ds_mbscoring_group == undef {
    $_ds_mbscoring_group = $ds_mbscoring::params::ds_mbscoring_group
  } else {
    $_ds_mbscoring_group = $ds_mbscoring::ds_mbscoring_group
  }

  if $ds_mbscoring::ds_mbscoring_anaconda_homedir == undef {
    $_ds_mbscoring_anaconda_homedir = $ds_mbscoring::params::ds_mbscoring_anaconda_homedir
  } else {
    $_ds_mbscoring_anaconda_homedir = $ds_mbscoring::ds_mbscoring_anaconda_homedir
  }

  $ds_mbscoring_deploy_dir = "/fr/deploy/${ds_mbscoring_app_name}"
  $ds_mbscoring_frlogdir = "/frlogdir/${ds_mbscoring_app_name}"

  class{'ds_mbscoring::install':
    require => Class['fr_global::params'],
  }
}
