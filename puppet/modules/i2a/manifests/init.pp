class
  i2a
  (
    $i2a_jdk_version = undef,
    $i2a_user = undef, 
    $i2a_group = undef, 
    $i2a_plantopening_solr = undef,
    $i2a_customerwin_solr = undef,
    $i2a_mergerandacquistion_solr = undef,
    $i2a_managementturn_solr = undef,
    $i2a_entity_solr = undef, 
    $i2a_doc_solr = undef, 
    $i2a_dsdoc_solr = undef, 
    $i2a_target_emailaddr = undef,
    $i2a_mi32DB = undef,
  )
{
  include i2a::params

  if $i2a::i2a_jdk_version == undef {
    $_i2a_jdk_version = $i2a::params::i2a_jdk_version
  } else {
    $_i2a_jdk_version = $i2a::i2a_jdk_version
  }

  if $i2a::i2a_user == undef {
    $_i2a_user = $i2a::params::i2a_user
  } else {
    $_i2a_user = $i2a::i2a_user
  }

  if $i2a::i2a_group == undef {
    $_i2a_group = $i2a::params::i2a_group
  } else {
    $_i2a_group = $i2a::i2a_group
  }

  if $i2a::i2a_entity_solr == undef {
    $_i2a_entity_solr = $i2a::params::i2a_entity_solr
  } else {
    $_i2a_entity_solr = $i2a::i2a_entity_solr
  }

  if $i2a::i2a_doc_solr == undef {
    $_i2a_doc_solr = $i2a::params::i2a_doc_solr
  } else {
    $_i2a_doc_solr = $i2a::i2a_doc_solr
  }

  if $i2a::i2a_dsdoc_solr == undef {
    $_i2a_dsdoc_solr = $i2a::params::i2a_dsdoc_solr
  } else {
    $_i2a_dsdoc_solr = $i2a::i2a_dsdoc_solr
  }

  if $i2a::i2a_target_emailaddr == undef {
    $_i2a_target_emailaddr = $i2a::params::i2a_target_emailaddr
  } else {
    $_i2a_target_emailaddr = $i2a::i2a_target_emailaddr
  }

  if $i2a::i2a_plantopening_solr == undef {
    $_i2a_plantopening_solr = $i2a::params::i2a_plantopening_solr
  } else {
    $_i2a_plantopening_solr = $i2a::i2a_plantopening_solr
  }

  if $i2a::i2a_managmentturn_solr == undef {
    $_i2a_managementturn_solr = $i2a::params::i2a_managementturn_solr
  } else {
    $_i2a_managementturn_solr = $i2a::i2a_managementturn_solr
  }

  if $i2a::i2a_mergerandacquistion_solr == undef {
    $_i2a_mergerandacquistion_solr = $i2a::params::i2a_mergerandacquistion_solr
  } else {
    $_i2a_mergerandacquistion_solr = $i2a::i2a_mergerandacquistion_solr
  }

  if $i2a::i2a_customerwin_solr == undef {
    $_i2a_customerwin_solr = $i2a::params::i2a_customerwin_solr
  } else {
    $_i2a_customerwin_solr = $i2a::i2a_customerwin_solr
  }

  if $i2a::i2a_mi32DB == undef {
    $_i2a_mi32DB = "10.10.10.110:1433"
  } else {
    $_i2a_mi32DB = $i2a::i2a_mi32DB
  }

  $i2a_deploy_dir = "/fr/deploy"
  $i2a_frlogdir = "/frlogdir"
  $i2a_util_dir = "/fr/UtilityTools"
  $i2a_jdk_home = "${fr_global::params::dir_bin}/jdk${i2a_jdk_version}"

  class{'i2a::install':
    require => Class['fr_global::params'],
  }

}
