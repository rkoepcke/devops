class
  i2a::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $i2a_user = "serviceprod"
  $i2a_group = "serviceprod"
  $i2a_plantopening_solr = "local.plantopening.firstrain.com:9090"
  $i2a_customerwin_solr = "local.customerwin.firstrain.com:9090"
  $i2a_mergerandacquistion_solr = "local.mergerandacquistion.firstrain.com:9090"
  $i2a_managementturn_solr = "local.managmentturn.firstrain.com:9090"
  $i2a_doc_solr = "solr.froovy.firstrain.com"
  $i2a_target_emailaddr = "i2areports@firstrain.com"
}
