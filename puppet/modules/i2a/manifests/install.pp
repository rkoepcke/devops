class
  i2a::install
inherits
  i2a
{

  $i2a_required_paths = [
    "${i2a::i2a_frlogdir}/MT",
    "${i2a::i2a_frlogdir}/MT/logs",
    "${i2a::i2a_frlogdir}/mergerandacquistion", 
    "${i2a::i2a_frlogdir}/mergerandacquistion/logs", 
    "${i2a::i2a_frlogdir}/mergerandacquistion/cpflogs", 
    "${i2a::i2a_frlogdir}/plantopening", 
    "${i2a::i2a_frlogdir}/plantopening/logs", 
    "${i2a::i2a_frlogdir}/customerwin", 
    "${i2a::i2a_frlogdir}/customerwin/logs", 
    "${fr_global::params::dir_root}/UtilityTools",
    "$i2a::i2a_deploy_dir/I2ASCW",
    "$i2a::i2a_deploy_dir/I2ASMA",
    "$i2a::i2a_deploy_dir/I2ASMT",
    "$i2a::i2a_deploy_dir/I2ASMT/conf",
    "$i2a::i2a_deploy_dir/I2ASMT/FRMTAuto_lib",
    "$i2a::i2a_deploy_dir/I2ASMT/output",
    "$i2a::i2a_deploy_dir/I2ASPO",
    "$i2a::i2a_deploy_dir/I2ASUtil",
    "$i2a::i2a_deploy_dir/I2ASlibs",
    "$i2a::i2a_deploy_dir/I2ASlibs/Extractor_lib",
    "$i2a::i2a_deploy_dir/I2ASlibs/Extractor_lib_old",
    "$i2a::i2a_deploy_dir/I2Abin",
    "$i2a::i2a_deploy_dir/I2AEXR",
    "$i2a::i2a_deploy_dir/I2AEXR/conf",
    "$i2a::i2a_deploy_dir/I2AEXR/lib",
    "$i2a::i2a_deploy_dir/log4j",
  ]

  file { $i2a_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $i2a::i2a_user,
    group  => $i2a::i2a_group,
    before => Notify['UtilityTools Dir'],
  }

  notify { 'UtilityTools Dir':
     name => "Tools dir name",
     message => "${i2a::i2a_deploy_dir}/I2Abin/cw.sh"
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/cw.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/cw.sh.erb'),
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/po.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/po.sh.erb'),
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/ma.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/ma.sh.erb'),
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/mt.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/mt.sh.erb'),
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/runCW_MA_EXR_jobs.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/runCW_MA_EXR_jobs.sh.erb'),
  }

  file { 'fetch log4j properties file':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/log4j/log4j.properties",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/log4j.properties"
  }

  file { 'fetch log4j2 xml file':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/log4j/log4j2.xml",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/log4j2.xml.erb')
  }

  file { 'fetch CW NERNames':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/NERNames.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CW_NERNames.txt"
  }

  file { 'fetch CWL CoRules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/CWL_CoRules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CWL_CoRules.txt"
  }

  file { 'fetch CWL Money':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/CWL_Money.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CWL_Money.txt"
  }

  file { 'fetch CWL ContractScope':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/CWL_ContractScope.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CWL_ContractScope.txt"
  }

  file { 'fetch CWL Duration':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/CWL_Duration.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CWL_Duration.txt"
  }

  file { 'fetch CW Schema':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASCW/CW_Schema.xml",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/CW_schema.xml"
  }

  file { "${i2a::i2a_deploy_dir}/I2ASCW/config.properties":
    ensure  =>  file,
    require =>  File[$i2a_required_paths],
    mode    => '0644',
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/CW_config.properties.erb')
  }

  file { "${i2a::i2a_deploy_dir}/I2ASMA/config.properties":
    ensure  =>  file,
    require =>  File[$i2a_required_paths],
    mode    => '0644',
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/MA_config.properties.erb'),
  }

  file { "${i2a::i2a_deploy_dir}/I2ASMA/entityinfo.properties":
    ensure  =>  file,
    require =>  File[$i2a_required_paths],
    mode    => '0644',
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/MA_entityinfo.properties.erb'),
  }

  file { 'fetch MA illegalPhraseRules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/illegalPhraseRules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_illegalPhraseRules.txt"
  }

  file { 'fetch MA illegalPhraseWords':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/illegalPhraseWords.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_illegalPhraseWords.txt"
  }

  file { 'fetch MA BA_PhraseExtractionRules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/BA_PhraseExtractionRules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_BA_PhraseExtractionRules.txt"
  }

  file { 'fetch MA BA_Rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/BA_Rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_BA_Rules.txt"
  }

  file { 'fetch MA BA_FilterRules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/FilterRules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_FilterRules.txt"
  }

  file { 'fetch MA Money_Rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/Money_Rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_Money_Rules.txt"
  }

  file { 'fetch MA custom_Rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/custom_Rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_custom_Rules.txt"
  }

  file { 'fetch MA Negative_Rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/Negative_Rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_Negative_Rules.txt"
  }

  file { 'fetch MA commonWords':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/commonWords.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_commonWords.txt"
  }

  file { 'fetch MA companyWordSynonyms':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMA/companyWordSynonyms.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MA_companyWordSynonyms.txt"
  }

  file { "${i2a::i2a_deploy_dir}/I2ASPO/config.properties":
    ensure  =>  file,
    require =>  File[$i2a_required_paths],
    mode    => '0644',
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/PO_config.properties.erb')
  }

  file { 'fetch PO active-passive_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/active-passive_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_active-passive_rules.txt"
  }

  file { 'fetch PO capacity_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/capacity_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_capacity_rules.txt"
  }

  file { 'fetch PO date_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/date_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_date_rules.txt"
  }

  file { 'fetch PO job_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/job_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_job_rules.txt"
  }

  file { 'fetch PO money_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/money_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_money_rules.txt"
  }

  file { 'fetch PO quote_rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/quote_rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_quote_rules.txt"
  }

  file { 'fetch PO NERNames':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASPO/NERNames.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/PO_NERNames.txt"
  }

  file { "${i2a::i2a_deploy_dir}/I2ASMT/config.properties":
    ensure  =>  file,
    require =>  File[$i2a_required_paths],
    mode    => '0644',
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    content =>  template('i2a/MT_config.properties.erb')
  }

  file { 'fetch FRMTAuto Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRMTAuto.jar"
  }

  file { 'fetch MT NERNames':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/NERNames.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT_NERNames.txt"
  }

  file { 'fetch MT CXODesigMap properties':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/CXODesigMap.properties",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT_CXODesigMap.properties"
  }

  file { 'fetch MT DateRule':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/DateRule.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT_DateRule.txt"
  }

  file { 'fetch MT divRule':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/divRule.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT_divRule.txt"
  }

  file { 'fetch MT endings':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/endings.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT_endings.txt"
  }

  file { 'fetch MT MTDept.rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/MTDept.rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MTDept.rules.txt"
  }

  file { 'fetch MT MTRule.properties':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/MTRule.properties",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MTRule.properties"
  }

  file { 'fetch MT MT.rules':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/conf/MT.rules.txt",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/MT.rules.txt"
  }

  file { "${i2a::i2a_deploy_dir}/I2ASMT/runMTAutomation.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/runMTAutomation.sh.erb')
  }

  file { 'fetch fr-common-utils jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto_lib/fr-common-utils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/fr-common-utils.jar"
  }

  file { 'fetch FRSolrExtension jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto_lib/FRSolrExtension.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRSolrExtension.jar"
  }

  file { 'fetch FRoovy jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto_lib/FRoovy.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRoovy.jar"
  }

  file { 'fetch i2a-FRMTAuto_lib tarfile':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/FRMTAuto_lib.tar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRMTAuto_lib.tar"
  }

  exec { 'extract-i2a-FRMTAuto-lib':
    command => "tar -xvf ${fr_global::params::dir_root}/download/FRMTAuto_lib.tar --directory ${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto_lib",
    require =>  File[$i2a_required_paths],
    user    =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    path    =>  '/bin',
  }

  #exec { 'remove-i2a-FRMTAuto-tar':
  #  command => "rm -f ${i2a::i2a_deploy_dir}/I2ASMT/FRMTAuto_lib/FRMTAuto_lib.tar",
  #  path    => '/bin'
  #}

  File['fetch i2a-FRMTAuto_lib tarfile'] -> Exec['extract-i2a-FRMTAuto-lib'] 

  file { 'fetch UTIL endings.txt':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASUtil/endings.txt/",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/UTIL_endings.txt"
  }

  file { 'fetch extractor_lib tarfile':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/extractor_lib.tar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/extractor_lib.tar"
  }

  exec { 'extract-i2a-extractor-lib':
    command => "tar -xvf ${fr_global::params::dir_root}/download/extractor_lib.tar --directory ${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib",
    require =>  File[$i2a_required_paths],
    user    =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    path    =>  '/bin',
  }

  File['fetch extractor_lib tarfile'] -> Exec['extract-i2a-extractor-lib'] 

  file { 'fetch old extractor_lib tarfile':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/extractor_lib.old.tar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/extractor_lib.old.tar"
  }

  exec { 'extract-i2a-extractor-lib-old':
    command => "tar -xvf ${fr_global::params::dir_root}/download/extractor_lib.old.tar --directory ${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib_old",
    require =>  File[$i2a_required_paths],
    user    =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    path    =>  '/bin',
  }

  File['fetch old extractor_lib tarfile'] -> Exec['extract-i2a-extractor-lib-old'] 

  file { 'fetch fr-common-utils2 jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/fr-common-utils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/fr-common-utils.jar"
  }

  file { 'fetch FRSolrExtension2 jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/FRSolrExtension.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRSolrExtension.jar"
  }

  file { 'fetch FRoovy2 jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/FRoovy.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRoovy.jar"
  }

  file { 'fetch FRRuleBasedExtraction_CW_PO Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/FRRuleBasedExtraction_CW_PO.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRRuleBasedExtraction_CW_PO.jar"
  }

  file { 'fetch FRRuleBasedExtraction_MA Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/FRRuleBasedExtraction_MA.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/insights-to-actions-1.0-DS.jar"
  }

  file { 'fetch DSEntityInfoCache Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/DSEntityInfoCache.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/ds-entity-info-cache-1.0-DS.jar"
  }

  file { 'fetch categorization-utils Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/categorization-utils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/categorization-utils-1.0-DS.jar"
  }

  file { 'fetch BasicUtils Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/BasicUtils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/BasicUtils-1.0-DS.jar"
  }

  file { 'fetch FRCoreNLP-3.5.2-DS Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/FRCoreNLP-3.5.2-DS.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/FRCoreNLP-3.5.2-DS.jar"
  }

  file { 'fetch NLPUtils Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/NLPUtils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/NLPUtils-1.0-DS.jar"
  }

  file { 'fetch Solr47Utils Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2ASlibs/Extractor_lib/Solr47Utils.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/Solr47Utils-1.0-DS.jar"
  }

  file { 'fetch i2a-exceptionreports Jar':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${i2a::i2a_deploy_dir}/I2AEXR/i2a-exceptionreports.jar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/i2a.exceptionreports.jar"
  }

  file { "${i2a::i2a_deploy_dir}/I2Abin/i2Aexception.sh":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/i2Aexception.sh.erb')
  }

  file { "${i2a::i2a_deploy_dir}/I2AEXR/conf/log4j2.xml":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/EXR_log4j2.xml.erb')
  }

  file { "${i2a::i2a_deploy_dir}/I2AEXR/conf/reports.properties":
    ensure  => file,
    require => File[$i2a_required_paths],
    mode    => '0755',
    owner   => $i2a::i2a_user,
    group   => $i2a::i2a_group,
    content => template('i2a/reports.properties.erb')
  }

  file { 'fetch ExceptionReports tarfile':
    ensure  =>  'file',
    require =>  File[$i2a_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/exceptrpt.tar",
    owner   =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    source  =>  "puppet:///modules/i2a/exceptrpt-lib.tar"
  }

  exec { 'extract-i2a-exceptionReports-lib':
    command => "tar -xvf ${fr_global::params::dir_root}/download/exceptrpt.tar --directory ${i2a::i2a_deploy_dir}/I2AEXR/lib",
    require =>  File[$i2a_required_paths],
    user    =>  $i2a::_i2a_user,
    group   =>  $i2a::_i2a_group,
    path    =>  '/bin',
  }

  File['fetch ExceptionReports tarfile'] -> Exec['extract-i2a-exceptionReports-lib']
}
