class
  dswebapp::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $dswebapp_shutdown_port = 8005
  $dswebapp_http_port = 8080
  $dswebapp_https_port = 8443
  $dswebapp_ajp_port = 8009
  $dswebapp_jdk_version = "1.8.0_45"
  $dswebapp_tomcat_user = "serviceprod"
  $dswebapp_tomcat_group = "serviceprod"
  $dswebapp_index_url = "sdsmysql1-l.ca.firstrain.net:7080"
  $dswebapp_entity_url = "solr.froovy.firstrain.com"
}
