class
  dswebapp::install
inherits
  dswebapp
{

  $dswebapp_required_paths = [
    $dswebapp::dswebapp_deploy_dir,
    $dswebapp::dswebapp_services_dir,
    "${dswebapp::dswebapp_services_dir}/bin",
    "${dswebapp::dswebapp_services_dir}/logs",
    "${dswebapp::dswebapp_services_dir}/conf",
    "${dswebapp::dswebapp_services_dir}/conf/Catalina",
    "${dswebapp::dswebapp_services_dir}/conf/Catalina/localhost",
    "${dswebapp::dswebapp_services_dir}/lib",
    "${dswebapp::dswebapp_services_dir}/temp",
    "${dswebapp::dswebapp_services_dir}/work",
    $dswebapp::dswebapp_frlogdir,
    "${dswebapp::dswebapp_frlogdir}/logs",
    "${dswebapp::dswebapp_frlogdir}/cpflogs",
    "${dswebapp::dswebapp_frlogdir}/cpflogs/test",
    "${dswebapp::dswebapp_cache_rootdir}/entityInfoCache",
    "${dswebapp::dswebapp_cache_rootdir}/entityInfoCache/entityLocalIndex",
  ]

  file { $dswebapp_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $dswebapp::_dswebapp_tomcat_user,
    group  => $dswebapp::_dswebapp_tomcat_group,
    before => Notify['finished-dswebapp-tag-war-fetch']
  }

  notify { 'start-dswebapp-tag-war-fetch':
    name    =>  'STARTING DSWebApp TagLinkingServices.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingServices.war"
  }

  notify { 'finished-dswebapp-tag-war-fetch':
    name    =>  'FINISHED DSWebApp TagLinkingServices.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingServices.war}"
  }

  file { 'fetch TagLinkingServices War file':
    ensure  =>  'file',
    require =>  File[$dswebapp_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingServices.war",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    source  =>  "puppet:///modules/dswebapp/TagLinkingServices.war",
    before  =>  Notify['finished-dswebapp-tag-war-fetch']
  }

  exec { 'delete-taglinking-solr-dir':
    command => "rm -rf ${dswebapp::dswebapp_deploy_dir}/TagLinkingServices",
    require =>  File[$dswebapp_required_paths],
    path => '/usr/bin:/bin',
    user => $dswebapp::_dswebapp_tomcat_user,
    onlyif => "test -d ${dswebapp::dswebapp_deploy_dir}/TagLinkingServices"
  }
    
  notify { 'start-dswebapp-solr-tag-war-fetch':
    name    =>  'STARTING DSWebApp TagLinkingSolrServices.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingSolrServices.war"
  }

  notify { 'finished-dswebapp-solr-tag-war-fetch':
    name    =>  'FINISHED DSWebApp TagLinkingSolrServices.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingSolrServices.war}"
  }

  file { 'fetch TagLinkingSolrServices War file':
    ensure  =>  'file',
    require =>  File[$dswebapp_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/TagLinkingSolrServices.war",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    source  =>  "puppet:///modules/dswebapp/TagLinkingSolrServices.war",
    before  =>  Notify['finished-dswebapp-solr-tag-war-fetch']
  }

  exec { 'delete-taglinking-dir':
    command => "rm -rf ${dswebapp::dswebapp_deploy_dir}/TagLinkingSolrServices",
    require =>  File[$dswebapp_required_paths],
    path => '/usr/bin:/bin',
    user => $dswebapp::_dswebapp_tomcat_user,
    onlyif => "test -d ${dswebapp::dswebapp_deploy_dir}/TagLinkingSolrServices"
  }
    
  notify { 'start-dswebapp-newCoDis-war-fetch':
    name    =>  'STARTING DSWebApp FRNewCompanyDiscovery.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/FRNewCompanyDiscovery.war"
  }

  notify { 'finished-dswebapp-newCoDis-war-fetch':
    name    =>  'FINISHED DSWebApp FRNewCompanyDiscovery.war Fetch',
    message =>  "${dswebapp::dswebapp_deploy_dir}/FRNewCompanyDiscovery.war}"
  }

  file { 'fetch FRNewCompanyDiscovery.war file':
    ensure  =>  'file',
    require =>  File[$dswebapp_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/FRNewCompanyDiscovery.war",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    source  =>  "puppet:///modules/dswebapp/FRNewCompanyDiscovery.war",
    before  =>  Notify['finished-dswebapp-newCoDis-war-fetch']
  }

  exec { 'delete-frnewcompany-dir':
    command => "rm -rf ${dswebapp::dswebapp_deploy_dir}/FRNewCompanyDiscovery",
    require =>  File[$dswebapp_required_paths],
    path => '/usr/bin:/bin',
    user => $dswebapp::_dswebapp_tomcat_user,
    onlyif => "test -d ${dswebapp::dswebapp_deploy_dir}/FRNewCompanyDiscovery"
  }
    
  $dswebapp_tr_required_paths = [
    "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI",
    "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF",
    "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/lib",
    "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/classes",
    "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/classes/resource",
  ]

  file { $dswebapp_tr_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $dswebapp::_dswebapp_tomcat_user,
    group  => $dswebapp::_dswebapp_tomcat_group,
    before => File['fetch-FRThemesDiscoveryAPI.jar']
  }

  notify { 'start-FRThemesDiscoveryAPI.jar-fetch':
    name    =>  'STARTING DSWebApp FRThemesDiscoveryAPI Install',
    message =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/lib/FRThemesDiscoveryAPI.jar"
  }

  file { 'fetch-FRThemesDiscoveryAPI.jar':
    ensure  =>  'file',
    require =>  File[$dswebapp_tr_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/lib/FRThemesDiscoveryAPI.jar",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    mode    => '0644',
    source  =>  "puppet:///modules/dswebapp/FRThemesDiscoveryAPI.jar",
    before  =>  Notify['finished-FRThemesDiscoveryAPI.jar-fetch']
  }

  file { 'fetch frthemes-spring-dummy.xml file':
    ensure  =>  'file',
    require =>  File[$dswebapp_tr_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/spring-dummy.xml",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    mode    => '0644',
    source  =>  "puppet:///modules/dswebapp/frthemes-spring-dummy.xml",
    before  =>  Notify['finished-FRThemesDiscoveryAPI.jar-fetch']
  }

  file { 'fetch frthemes-spring-themes-servlet.xml file':
    ensure  =>  'file',
    require =>  File[$dswebapp_tr_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/spring-themes-servlet.xml",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    mode    => '0644',
    source  =>  "puppet:///modules/dswebapp/frthemes-spring-themes-servlet.xml",
    before  =>  Notify['finished-FRThemesDiscoveryAPI.jar-fetch']
  }

  file { 'fetch frthemes-web.xml file':
    ensure  =>  'file',
    require =>  File[$dswebapp_tr_required_paths],
    path    =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/web.xml",
    owner   =>  $dswebapp::_dswebapp_tomcat_user,
    group   =>  $dswebapp::_dswebapp_tomcat_group,
    mode    => '0644',
    source  =>  "puppet:///modules/dswebapp/frthemes-web.xml",
    before  =>  Notify['finished-FRThemesDiscoveryAPI.jar-fetch']
  }

  file {'fetch FRThemesDiscovery library tar file':
    ensure  => 'file',
    require => File[$dswebapp_tr_required_paths],
    path    => "${fr_global::params::dir_root}/download/frthemes-lib.tar",
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    source  => "puppet:///modules/dswebapp/frthemes-lib.tar",
  }

  file {'fetch commonWords.txt conf file':
    ensure  => 'file',
    require => File[$dswebapp_tr_required_paths],
    path    => "${dswebapp::dswebapp_conf}/commonWords.txt",
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    source  => "puppet:///modules/dswebapp/commonWords.txt",
  }

  file {'fetch companyWordSynonyms.txt conf file':
    ensure  => 'file',
    require => File[$dswebapp_tr_required_paths],
    path    => "${dswebapp::dswebapp_conf}/companyWordSynonyms.txt",
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    source  => "puppet:///modules/dswebapp/companyWordSynonyms.txt",
  }

  exec { 'extract-frthemes-dis-lib':
    command => "tar -xvf ${fr_global::params::dir_root}/download/frthemes-lib.tar --directory ${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF",
    require => File[$dswebapp_tr_required_paths],
    user    => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    path    => '/bin',
  }

  File['fetch FRThemesDiscovery library tar file'] -> Exec['extract-frthemes-dis-lib']

  file { "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/classes/log4j.properties":
    ensure  => file,
    require => File[$dswebapp_tr_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/frthemes-log4j.properties.erb'),
  }

  file { "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/classes/resource/spring_config.properties":
    ensure  => file,
    require => File[$dswebapp_tr_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/frthemes-spring_config.properties.erb'),
  }

  notify { 'finished-FRThemesDiscoveryAPI.jar-fetch':
    name    =>  'Finished DSWebApp FRThemesDiscoveryAPI Install',
    message =>  "${dswebapp::dswebapp_deploy_dir}/FRThemesDiscoveryAPI/WEB-INF/lib/FRThemesDiscoveryAPI.jar"
  }

### End new FRThemeDiscovery code

  exec { 'copy-tomcat-bin-files':
    command => "cp ${dswebapp::_dswebapp_tomcat_home}/bin/*.sh ${dswebapp::dswebapp_bin}",
    require => [File[$dswebapp_required_paths],Class['tomcat::install']],
    user    => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${dswebapp::dswebapp_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files':
    command => "cp ${dswebapp::_dswebapp_tomcat_home}/bin/*.jar ${dswebapp::dswebapp_bin}",
    require => [File[$dswebapp_required_paths],Class['tomcat::install']],
    user    => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${dswebapp::dswebapp_bin}/bootstrap.jar",
  }

  notify {'Directory Check':
     name => "config directory is $dswebapp_conf"
  }

  file { "${dswebapp::dswebapp_conf}/server.xml":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/server.xml.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/categorizationUtils.properties":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/categorizationUtils.properties.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/logging.properties":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/logging.properties.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/catalina.policy":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/catalina.policy.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/context.xml":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/context.xml.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/tomcat-users.xml.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/web.xml":
    ensure  => file,
    require => File[$dswebapp_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/web.xml.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/taglinking.properties":
    ensure  => file,
    require => File[$dswebapp_tr_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/taglinking.properties.erb'),
  }

  file { "${dswebapp::dswebapp_conf}/dsentityinfocache.properties":
    ensure  => file,
    require => File[$dswebapp_tr_required_paths],
    mode    => '0644',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/dsentityinfocache.properties.erb'),
  }

  file { "${dswebapp::dswebapp_bin}/setenv.sh":
    ensure  => file,
    require => [File[$dswebapp_required_paths],Exec['copy-tomcat-bin-files']],
    mode    => '0755',
    owner   => $dswebapp::_dswebapp_tomcat_user,
    group   => $dswebapp::_dswebapp_tomcat_group,
    content => template('dswebapp/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/frdsapps":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    replace => false,
    content => template('dswebapp/frdsapps.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frdsapps.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frdsapps'],
        owner   => root,
        group   => root,
        content => template('dswebapp/frdsapps.service.erb'),
      }
  }

  if $::hostname =~ /sdsweb[0-9]-l/ {
      file { 'fetch NLPServices War file':
	ensure  =>  'file',
	require =>  File[$dswebapp_required_paths],
	path    =>  "${dswebapp::dswebapp_deploy_dir}/NLPServices.war",
	owner   =>  $dswebapp::_dswebapp_tomcat_user,
	group   =>  $dswebapp::_dswebapp_tomcat_group,
	source  =>  "puppet:///modules/dswebapp/NLPServices.war",
	before  =>  Notify['finished-dswebapp-tag-war-fetch']
      }

      exec { 'delete-nlpservices-solr-dir':
	command => "rm -rf ${dswebapp::dswebapp_deploy_dir}/NLPServices",
	require =>  File[$dswebapp_required_paths],
	path => '/usr/bin:/bin',
	user => $dswebapp::_dswebapp_tomcat_user,
	onlyif => "test -d ${dswebapp::dswebapp_deploy_dir}/NLPServices"
      }
  }
}
