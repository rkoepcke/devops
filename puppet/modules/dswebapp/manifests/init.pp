class
  dswebapp
  (
    $dswebapp_tomcat_home = undef,
    $dswebapp_http_port = undef,
    $dswebapp_https_port = undef,
    $dswebapp_shutdown_port = undef,
    $dswebapp_ajp_port = undef,
    $dswebapp_jdk_version = undef,
    $dswebapp_tomcat_user = undef,
    $dswebapp_tomcat_group = undef,
    $dswebapp_neo_url = undef,
    $dswebapp_index_url = undef,
    $dswebapp_entity_url = undef,
    $dswebapp_trendingthemes_url = undef,
    $dswebapp_app_name = "frdsapps",
  )
{
  include dswebapp::params

  notify { 'dswebapp deploy dir':
    message => $_deploy_dir
  }

  if $dswebapp::dswebapp_jdk_version == undef {
    $_dswebapp_jdk_version = $dswebapp::params::dswebapp_jdk_version
  } else {
    $_dswebapp_jdk_version = $dswebapp::dswebapp_jdk_version
  }

  if $dswebapp::dswebapp_shutdown_port == undef {
    $_dswebapp_shutdown_port = $dswebapp::params::dswebapp_shutdown_port
  } else {
    $_dswebapp_shutdown_port = $dswebapp::dswebapp_shutdown_port
  }

  if $dswebapp::dswebapp_http_port == undef {
    $_dswebapp_http_port = $dswebapp::params::dswebapp_http_port
  } else {
    $_dswebapp_http_port = $dswebapp::dswebapp_http_port
  }

  if $dswebapp::dswebapp_https_port == undef {
    $_dswebapp_https_port = $dswebapp::params::dswebapp_https_port
  } else {
    $_dswebapp_https_port = $dswebapp::dswebapp_https_port
  }

  if $dswebapp::dswebapp_ajp_port == undef {
    $_dswebapp_ajp_port = $dswebapp::params::dswebapp_ajp_port
  } else {
    $_dswebapp_ajp_port = $dswebapp::dswebapp_ajp_port
  }

  if $dswebapp::dswebapp_tomcat_home == undef {
    $_dswebapp_tomcat_home = $dswebapp::params::dswebapp_tomcat_home
  } else {
    $_dswebapp_tomcat_home = $dswebapp::dswebapp_tomcat_home
  }

  if $dswebapp::dswebapp_tomcat_user == undef {
    $_dswebapp_tomcat_user = $dswebapp::params::dswebapp_tomcat_user
  } else {
    $_dswebapp_tomcat_user = $dswebapp::dswebapp_tomcat_user
  }

  if $dswebapp::dswebapp_tomcat_group == undef {
    $_dswebapp_tomcat_group = $dswebapp::params::dswebapp_tomcat_group
  } else {
    $_dswebapp_tomcat_group = $dswebapp::dswebapp_tomcat_group
  }

  if $dswebapp::dswebapp_index_url == undef {
    $_dswebapp_index_url = $dswebapp::params::dswebapp_index_url
  } else {
    $_dswebapp_index_url = $dswebapp::dswebapp_index_url
  }

  if $dswebapp::dswebapp_neo_url == undef {
    $_dswebapp_neo_url = $dswebapp::params::dswebapp_neo_url
  } else {
    $_dswebapp_neo_url = $dswebapp::dswebapp_neo_url
  }

  if $dswebapp::dswebapp_entity_url == undef {
    $_dswebapp_entity_url = $dswebapp::params::dswebapp_entity_url
  } else {
    $_dswebapp_entity_url = $dswebapp::dswebapp_entity_url
  }

  if $dswebapp::dswebapp_trendingthemes_url == undef {
    $_dswebapp_trendingthemes_url = $dswebapp::params::dswebapp_trendingthemes_url
  } else {
    $_dswebapp_trendingthemes_url = $dswebapp::dswebapp_trendingthemes_url
  }

  $dswebapp_deploy_dir = "/fr/deploy/${dswebapp_app_name}"
  $dswebapp_services_dir = "/fr/services/${dswebapp_app_name}"
  $dswebapp_frlogdir = "/frlogdir/${dswebapp_app_name}"
  $dswebapp_conf = "/fr/services/${dswebapp_app_name}/conf"
  $dswebapp_bin = "/fr/services/${dswebapp_app_name}/bin"
  $dswebapp_jdk_dir = "/fr/third-party/${_dswebapp_jdk_version}"
  $dswebapp_cache_rootdir = "/frdata"

  class{'dswebapp::install':
    require => Class['fr_global::params'],
  }

}
