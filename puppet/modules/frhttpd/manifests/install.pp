class
  frhttpd::install
inherits
  frhttpd
{

  $frhttpd_required_paths = [
    "${frhttpd::frhttpd_frlogdir}",
    "$frhttpd::frhttpd_deploy_dir",
    "$frhttpd::frhttpd_deploy_dir/conf",
    "$frhttpd::frhttpd_deploy_dir/conf.d",
    "$frhttpd::frhttpd_rundir",
  ]

  exec { 'install-httpd-server':
     command => "yum install -y httpd",
     user    => "root",
     group   => "root",
     path    => ["/usr/bin", "/bin", "/usr/sbin"],
     creates => "/usr/sbin/httpd",
     before  => Exec['create-frhttpd-binary'],
     #onlyif  => "yum list installed | egrep -c httpd.x86_64",
  }

  file { $frhttpd_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $frhttpd::frhttpd_user,
    group  => $frhttpd::frhttpd_group,
    before => Exec['install-httpd-modules-link'],
  }

  exec { 'install-httpd-modules-link':
     command => "ln -s /usr/lib64/httpd/modules modules",
     require => File[$frhttpd_required_paths],
     user    => "root",
     group   => "root",
     path    => ["/usr/bin", "/bin", "/usr/sbin"],
     cwd     => "${frhttpd::frhttpd_deploy_dir}",
     unless  => "test -h modules",
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file {'HTTP conf.d tar file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 path    => "${fr_global::params::dir_root}/download/confd_os7.tar",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/confd_os7.tar",
      }

      exec { 'extract-httpd-confd-files':
	 command => "tar -xvf ${fr_global::params::dir_root}/download/confd_os7.tar --directory ${frhttpd_deploy_dir}",
	 require => File[$frhttpd_required_paths],
	 user    => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 path    => ["/bin", "/usr/bin"],
	 creates => "${frhttpd_deploy_dir}/conf.d/README",
      }

      File['HTTP conf.d tar file'] -> Exec['extract-httpd-confd-files']

      file {'HTTP conf.modules.d tar file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 path    => "${fr_global::params::dir_root}/download/confModules_os7.tar",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/confModules_os7.tar",
      }

      exec { 'extract-httpd-conf-modulesd-files':
	 command => "tar -xvf ${fr_global::params::dir_root}/download/confModules_os7.tar --directory ${frhttpd_deploy_dir}",
	 require => File[$frhttpd_required_paths],
	 user    => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 path    => ["/bin", "/usr/bin"],
	 creates => "${frhttpd_deploy_dir}/conf.modules.d/00-proxy.conf"
      }

      File['HTTP conf.modules.d tar file'] -> Exec['extract-httpd-conf-modulesd-files']

      file { "${frhttpd::frhttpd_deploy_dir}/conf/httpd.conf":
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 content => template('frhttpd/httpd_os7.conf.erb'),
         replace => false,
      }

      file {'fetch magic_os7 file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 path    => "${frhttpd::frhttpd_deploy_dir}/conf/magic",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/magic_os7",
      }

      file {'frhttpd service file':
	 ensure  => 'file',
	 mode    => '0644',
	 path    => "/usr/lib/systemd/system/frhttpd.service",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/frhttpd.service",
      }
  }
  else {
      file {'fetch mod_dnssd.conf file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 path    => "${frhttpd::frhttpd_deploy_dir}/conf.d/mod_dnssd.conf",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/mod_dnssd.conf",
      }

      file {'fetch welcome.conf file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 path    => "${frhttpd::frhttpd_deploy_dir}/conf.d/welcome.conf",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/welcome.conf",
      }

      file {'fetch README file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 path    => "${frhttpd::frhttpd_deploy_dir}/conf.d/README",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/README",
      }

      file { "${frhttpd::frhttpd_deploy_dir}/conf/httpd.conf":
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 content => template('frhttpd/httpd.conf.erb'),
      }

      file {'fetch magic file':
	 ensure  => 'file',
	 require => File[$frhttpd_required_paths],
	 mode    => '0644',
	 path    => "${frhttpd::frhttpd_deploy_dir}/conf/magic",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/magic",
       }

      file {'fetch etc httpd file':
	 ensure  => 'file',
	 mode    => '0755',
	 path    => "/etc/init.d/frhttpd",
	 owner   => $frhttpd::_frhttpd_user,
	 group   => $frhttpd::_frhttpd_group,
	 source  => "puppet:///modules/frhttpd/frhttpd",
      }
  }
   
  file { "/etc/sysconfig/frhttpd":
     ensure  => 'file',
     mode    => '0644',
     owner   => $frhttpd::_frhttpd_user,
     group   => $frhttpd::_frhttpd_group,
     content => template('frhttpd/frhttpd.sys.erb'),
  }

  exec { 'create-frhttpd-binary':
     command => "cp /usr/sbin/httpd /usr/sbin/frhttpd",
     require => File[$frhttpd_required_paths],
     user    => "root",
     group   => "root",
     path    => ["/usr/bin", "/bin", "/usr/sbin"],
     cwd     => "/usr/sbin",
     creates => "/usr/sbin/frhttpd",
  }

  exec { 'install-httpd-run-link':
     command => "ln -s ${frhttpd::frhttpd_rundir} run",
     require => File[$frhttpd_required_paths],
     user    => "root",
     group   => "root",
     path    => ["/usr/bin", "/bin", "/usr/sbin"],
     cwd     => "${frhttpd::frhttpd_deploy_dir}",
     unless  => "test -h run",
  }

  exec { 'install-httpd-log-link':
     command => "ln -s ${frhttpd::frhttpd_frlogdir} logs",
     require => File[$frhttpd_required_paths],
     user    => "root",
     group   => "root",
     path    => ["/usr/bin", "/bin", "/usr/sbin"],
     cwd     => "${frhttpd::frhttpd_deploy_dir}",
     unless  => "test -h logs",
  }

  tidy { 'Cleanup puppet client bucket':
     path    => '/var/lib/puppet/clientbucket',  
     age     => '4w',
     recurse => true,
     rmdirs  => true,
     backup  => false,
     type    => 'ctime',
  }
  
}
