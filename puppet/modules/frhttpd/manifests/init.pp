class
  frhttpd
  (
    $frhttpd_user = undef, 
    $frhttpd_group = undef, 
    $frhttpd_alias = undef,
    $frhttpd_port = undef,
  )
{
  include frhttpd::params

  if $frhttpd::frhttpd_port == undef {
    $_frhttpd_port = $frhttpd::params::frhttpd_port
  } else {
    $_frhttpd_port = $frhttpd::frhttpd_port
  }

  if $frhttpd::frhttpd_user == undef {
    $_frhttpd_user = $frhttpd::params::frhttpd_user
  } else {
    $_frhttpd_user = $frhttpd::frhttpd_user
  }

  if $frhttpd::frhttpd_group == undef {
    $_frhttpd_group = $frhttpd::params::frhttpd_group
  } else {
    $_frhttpd_group = $frhttpd::frhttpd_group
  }

  if $frhttpd::frhttpd_alias == undef {
    $_frhttpd_alias = $frhttpd::params::frhttpd_alias
  } else {
    $_frhttpd_alias = $frhttpd::frhttpd_alias
  }

  $frhttpd_deploy_dir = "${fr_global::params::dir_bin}/frhttpd"
  $frhttpd_frlogdir = "/frlogdir/frhttpd"
  $frhttpd_rundir = "/var/run/frhttpd"

  class{'frhttpd::install':
    require => Class['fr_global::params'],
  }

}
