class
  frhttpd::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $frhttpd_user = "root"
  $frhttpd_group = "root"
  $frhttpd_port = 55555
  $frhttpd_alias = "logs"
}
