class
  ds_trendthemes::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  $trendThemes_user = "serviceprod"
  $trendThemes_group = "serviceprod"
  $trendThemes_jdk_version = "jdk1.8.0_45"
  $trendThemes_entitySolr = "dsolr1-rl.ca.firstrain.net:38117/solr/entity"
  $trendThemes_phrasesSolr = "sdsmysql1-rl.ca.firstrain.net:8080/solr/ExtractedPhrases"
  $trendThemes_Solr = "sdsmysql1-rl.ca.firstrain.net:8080/solr/TrendingThemes"
  $trendThemes_loggerID = "info.prod.logger"
  $trendThemes_firstday = "5438"
  $trendThemes_appname = "TrendingThemes"
}
