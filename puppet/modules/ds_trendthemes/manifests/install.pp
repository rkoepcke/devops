class
  ds_trendthemes::install
inherits
  ds_trendthemes
{

  $trendThemes_required_paths = [
    "$ds_trendthemes::trendThemes_frlogdir",
    "${ds_trendthemes::trendThemes_frlogdir}/logs",
    "${ds_trendthemes::trendThemes_frlogdir}/logs/testLogs",
    "${ds_trendthemes::trendThemes_frlogdir}/cpflogs",
    "$ds_trendthemes::trendThemes_deploy_dir",
    "$ds_trendthemes::trendThemes_deploy_dir/bin",
    "$ds_trendthemes::trendThemes_deploy_dir/lib",
    "$ds_trendthemes::trendThemes_deploy_dir/conf",
  ]

  file { $trendThemes_required_paths:
    ensure => directory,
    mode   => '0644',
    owner  => $ds_trendthemes::trendThemes_user,
    group  => $ds_trendthemes::trendThemes_group,
    before => Notify['Install Trend Theme Library Jars'],
  }

  notify { 'Install Trend Theme Library Jars':
     name => "Theme Discovery Library",
     message => "${ds_trendthemes::trendThemes_deploy_dir}/lib jar files"
  }

  file {'fetch trend theme library tar file':
     ensure  => 'file',
     require => File[$trendThemes_required_paths],
     path    => "${fr_global::params::dir_root}/download/ttJarFiles.tar",
     owner   => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     source  => "puppet:///modules/ds_trendthemes/ttJarFiles.tar"
  }

  exec { 'extract-trend-themes-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/ttJarFiles.tar --directory ${trendThemes_deploy_dir}/lib",
     require => File[$trendThemes_required_paths],
     user    => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     path    => '/bin',
  }

  File['fetch trend theme library tar file'] -> Exec['extract-trend-themes-lib']

  file {'fetch trendingthemes Jar':
     ensure  => 'file',
     mode    => '0644',
     require => File[$trendThemes_required_paths],
     path    => "${ds_trendthemes::trendThemes_deploy_dir}/trendingthemes-2.0.jar",
     owner   => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     source  => "puppet:///modules/ds_trendthemes/trendingthemes-2.0.jar"
  }
  
  file { "${ds_trendthemes::trendThemes_deploy_dir}/conf/log4j2.xml":
     ensure  => 'file',
     require => File[$trendThemes_required_paths],
     mode    => '0644',
     owner   => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     content => template('ds_trendthemes/log4j2.xml.erb'),
  }
   
  file { "${ds_trendthemes::trendThemes_deploy_dir}/conf/config.properties":
     ensure  => 'file',
     require => File[$trendThemes_required_paths],
     mode    => '0644',
     owner   => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     content => template('ds_trendthemes/config.properties.erb'),
  }

  file { "${ds_trendthemes::trendThemes_deploy_dir}/bin/UpdateTrendingThemes.sh":
     ensure  => 'file',
     require => File[$trendThemes_required_paths],
     mode    => '0755',
     owner   => $ds_trendthemes::_trendThemes_user,
     group   => $ds_trendthemes::_trendThemes_group,
     content => template('ds_trendthemes/UpdateTrendingThemes.sh.erb'),
  }
}
