class
  ds_trendthemes
  (
    $trendThemes_jdk_version = undef,
    $trendThemes_user = undef, 
    $trendThemes_group = undef, 
    $trendThemes_entitySolr = undef,
    $trendThemes_phraseSolr = undef,
    $trendThemes_Solr = undef,
    $trendThemes_loggerID = undef,
    $trendThemes_firstday = undef,
    $trendThemes_appname = undef,
  )
{
  include ds_trendthemes::params

  if $ds_trendthemes::trendThemes_jdk_version == undef {
    $_trendThemes_jdk_version = $ds_trendthemes::params::trendThemes_jdk_version
  } else {
    $_trendThemes_jdk_version = $ds_trendthemes::trendThemes_jdk_version
  }

  if $ds_trendthemes::trendThemes_user == undef {
    $_trendThemes_user = $ds_trendthemes::params::trendThemes_user
  } else {
    $_trendThemes_user = $ds_trendthemes::trendThemes_user
  }

  if $ds_trendthemes::trendThemes_group == undef {
    $_trendThemes_group = $ds_trendthemes::params::trendThemes_group
  } else {
    $_trendThemes_group = $ds_trendthemes::trendThemes_group
  }

  if $ds_trendthemes::trendThemes_entitySolr == undef {
    $_trendThemes_entitySolr = $ds_trendthemes::params::trendThemes_entitySolr
  } else {
    $_trendThemes_entitySolr = $ds_trendthemes::trendThemes_entitySolr
  }

  if $ds_trendthemes::trendThemes_phraseSolr == undef {
    $_trendThemes_phraseSolr = $ds_trendthemes::params::trendThemes_phraseSolr
  } else {
    $_trendThemes_phraseSolr = $ds_trendthemes::trendThemes_phraseSolr
  }

  if $ds_trendthemes::trendThemes_Solr == undef {
    $_trendThemes_Solr = $ds_trendthemes::params::trendThemes_Solr
  } else {
    $_trendThemes_Solr = $ds_trendthemes::trendThemes_Solr
  }

  if $ds_trendthemes::trendThemes_appname == undef {
    $_trendThemes_appname = $ds_trendthemes::params::trendThemes_appname
  } else {
    $_trendThemes_appname = $ds_trendthemes::trendThemes_appname
  }

  if $ds_trendthemes::trendThemes_loggerID == undef {
    $_trendThemes_loggerID = $ds_trendthemes::params::trendThemes_loggerID
  } else {
    $_trendThemes_loggerID = $ds_trendthemes::trendThemes_loggerID
  }

  if $ds_trendthemes::trendThemes_firstday == undef {
    $_trendThemes_firstday = $ds_trendthemes::params::trendThemes_firstday
  } else {
    $_trendThemes_firstday = $ds_trendthemes::trendThemes_firstday
  }

  $trendThemes_deploy_dir = "/fr/deploy/${trendThemes_appname}"
  $trendThemes_frlogdir = "/frlogdir/${trendThemes_appname}"
  $trendThemes_jdk_home = "${fr_global::params::dir_bin}/${trendThemes_jdk_version}"

  class{'ds_trendthemes::install':
    require => Class['fr_global::params'],
  }

}
