class
  ds_moodydata::install
inherits
  ds_moodydata
{

  $ds_moodydata_required_paths = [
    $ds_moodydata::ds_moodydata_deploy_dir,
    "$ds_moodydata::ds_moodydata_deploy_dir/bin",
    "$ds_moodydata::ds_moodydata_deploy_dir/conf",
    "$ds_moodydata::ds_moodydata_deploy_dir/lib",
    "$ds_moodydata::ds_moodydata_deploy_dir/dslib",
    $ds_moodydata::ds_moodydata_frlogdir,
    "${ds_moodydata::ds_moodydata_frlogdir}/logs",
    $ds_moodydata::_ds_moodydata_output_rootdir,
    "${ds_moodydata::_ds_moodydata_output_rootdir}/data",
  ]

  file { $ds_moodydata_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_moodydata::_ds_moodydata_user,
    group  => $ds_moodydata::_ds_moodydata_group,
    before => File['fetch moodydata library tar file'],
  }

  file {'fetch moodydata library tar file':
     ensure  => 'file',
     require => File[$ds_moodydata_required_paths],
     path    => "${fr_global::params::dir_root}/download/moody-lib.tar",
     owner   => $ds_moodydata::_ds_moodydata_user,
     group   => $ds_moodydata::_ds_moodydata_group,
     source  => "puppet:///modules/ds_moodydata/moody-lib.tar"
  }

   exec { 'extract-moody-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/moody-lib.tar --directory ${ds_moodydata_deploy_dir}/lib",
     require => File[$ds_moodydata_required_paths],
     user    => $ds_moodydata::_ds_moodydata_user,
     group   => $ds_moodydata::_ds_moodydata_group,
     path    => '/bin',
  }

  File['fetch moodydata library tar file'] -> Exec['extract-moody-lib']

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    content => template('ds_moodydata/log4j2.xml.erb'),
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    content => template('ds_moodydata/config.properties.erb'),
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0755',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    content => template('ds_moodydata/startup.sh.erb'),
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0755',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    content => template('ds_moodydata/shutdown.sh.erb'),
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/MoodysDataGeneration.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/MoodysDataGeneration-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/RegionRelevancyScorer.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/RegionRelevancyScorer-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/CreditRelevancyScorer.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/CreditRelevancyScorer-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/BasicUtils.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/BasicUtils-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/Solr47Utils.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/Solr47Utils-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/SQLUtils.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/SQLUtils-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/DocUtils.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/DocUtils-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/NLPUtils.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/NLPUtils-1.0-DS.jar",
  }

  file { "${ds_moodydata::ds_moodydata_deploy_dir}/dslib/FRCoreNLP.jar":
    ensure  => file,
    require => File[$ds_moodydata_required_paths],
    mode    => '0644',
    owner   => $ds_moodydata::_ds_moodydata_user,
    group   => $ds_moodydata::_ds_moodydata_group,
    source  =>  "puppet:///modules/ds_moodydata/FRCoreNLP-3.5.2-DS.jar",
  }
}
