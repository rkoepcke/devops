class
  ds_moodydata::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_moodydata_jdk_version = "1.8.0_66"
  $ds_moodydata_user = "serviceprod"
  $ds_moodydata_group = "engops"
  $ds_moodydata_doc_solr_url = "http://sfrontend-rl.ca.firstrain.net:8080/solr/doc"
  $ds_moodydata_creditrel_api_url = "http://sdsweb2-l.ca.firstrain.net:5000/api/creditRelevancy/v1.0"
  $ds_moodydata_storage_service_url = "http://stage.frstorageservice.firstrain.com/frstorageservice/extendmetav1.json"
  $ds_moodydata_output_dir = "/frdata/moodys/data"
}
