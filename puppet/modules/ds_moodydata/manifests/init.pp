class
  ds_moodydata
  (
    $ds_moodydata_jdk_version = undef,
    $ds_moodydata_user = undef,
    $ds_moodydata_group = undef,
    $ds_moodydata_doc_solr_url = undef,
    $ds_moodydata_dsdoc_solr_url = undef,
    $ds_moodydata_storage_service_url = undef,
    $ds_moodydata_creditrel_api_url = undef,
    $ds_moodydata_rest_service_url = undef,
    $ds_moodydata_output_rootdir = undef,
    $ds_moodydata_loggername = undef,
    $ds_moodydata_dsdb = undef,
    $ds_moodydata_dsdbname = undef,
    $ds_moodydata_dsdb_user = undef,
    $ds_moodydata_dsdb_passwd = undef,
    $ds_moodydata_searchdb = undef,
    $ds_moodydata_searchdbname = undef,
    $ds_moodydata_searchdb_user = undef,
    $ds_moodydata_searchdb_passwd = undef,
    $ds_moodydata_app_name = "frmoodysDataGeneration",
  )
{
  include ds_moodydata::params

  notify { 'ds_moodydata deploy dir':
    message => $_deploy_dir
  }

  if $ds_moodydata::ds_moodydata_jdk_version == undef {
    $_ds_moodydata_jdk_version = $ds_moodydata::params::ds_moodydata_jdk_version
  } else {
    $_ds_moodydata_jdk_version = $ds_moodydata::ds_moodydata_jdk_version
  }

  if $ds_moodydata::ds_moodydata_user == undef {
    $_ds_moodydata_user = $ds_moodydata::params::ds_moodydata_user
  } else {
    $_ds_moodydata_user = $ds_moodydata::ds_moodydata_user
  }

  if $ds_moodydata::ds_moodydata_group == undef {
    $_ds_moodydata_group = $ds_moodydata::params::ds_moodydata_group
  } else {
    $_ds_moodydata_group = $ds_moodydata::ds_moodydata_group
  }

  if $ds_moodydata::ds_moodydata_update_api_url == undef {
    $_ds_moodydata_update_api_url = $ds_moodydata::params::ds_moodydata_update_api_url
  } else {
    $_ds_moodydata_update_api_url = $ds_moodydata::ds_moodydata_update_api_url
  }

  if $ds_moodydata::ds_moodydata_storage_service_url == undef {
    $_ds_moodydata_storage_service_url = $ds_moodydata::params::ds_moodydata_storage_service_url
  } else {
    $_ds_moodydata_storage_service_url = $ds_moodydata::ds_moodydata_storage_service_url
  }

  if $ds_moodydata::ds_moodydata_creditrel_api_url == undef {
    $_ds_moodydata_creditrel_api_url = $ds_moodydata::params::ds_moodydata_creditrel_api_url
  } else {
    $_ds_moodydata_creditrel_api_url = $ds_moodydata::ds_moodydata_creditrel_api_url
  }

  if $ds_moodydata::ds_moodydata_rest_service_url == undef {
    $_ds_moodydata_rest_service_url = $ds_moodydata::params::ds_moodydata_rest_service_url
  } else {
    $_ds_moodydata_rest_service_url = $ds_moodydata::ds_moodydata_rest_service_url
  }

  if $ds_moodydata::ds_moodydata_dsdoc_solr_url == undef {
    $_ds_moodydata_dsdoc_solr_url = $ds_moodydata::params::ds_moodydata_dsdoc_solr_url
  } else {
    $_ds_moodydata_dsdoc_solr_url = $ds_moodydata::ds_moodydata_dsdoc_solr_url
  }

  if $ds_moodydata::ds_moodydata_doc_solr_url == undef {
    $_ds_moodydata_doc_solr_url = $ds_moodydata::params::ds_moodydata_doc_solr_url
  } else {
    $_ds_moodydata_doc_solr_url = $ds_moodydata::ds_moodydata_doc_solr_url
  }

  if $ds_moodydata::ds_moodydata_output_rootdir == undef {
    $_ds_moodydata_output_rootdir = $ds_moodydata::params::ds_moodydata_output_rootdir
  } else {
    $_ds_moodydata_output_rootdir = $ds_moodydata::ds_moodydata_output_rootdir
  }

  if $ds_moodydata::ds_moodydata_loggername == undef {
    $_ds_moodydata_loggername = $ds_moodydata::params::ds_moodydata_loggername
  } else {
    $_ds_moodydata_loggername = $ds_moodydata::ds_moodydata_loggername
  }

  if $ds_moodydata::ds_moodydata_dsdb == undef {
    $_ds_moodydata_dsdb = $ds_moodydata::params::ds_moodydata_dsdb
  } else {
    $_ds_moodydata_dsdb = $ds_moodydata::ds_moodydata_dsdb
  }

  if $ds_moodydata::ds_moodydata_dsdbname == undef {
    $_ds_moodydata_dsdbname = $ds_moodydata::params::ds_moodydata_dsdbname
  } else {
    $_ds_moodydata_dsdbname = $ds_moodydata::ds_moodydata_dsdbname
  }

  if $ds_moodydata::ds_moodydata_dsdb_user == undef {
    $_ds_moodydata_dsdb_user = $ds_moodydata::params::ds_moodydata_dsdb_user
  } else {
    $_ds_moodydata_dsdb_user = $ds_moodydata::ds_moodydata_dsdb_user
  }

  if $ds_moodydata::ds_moodydata_dsdb_passwd == undef {
    $_ds_moodydata_dsdb_passwd = $ds_moodydata::params::ds_moodydata_dsdb_passwd
  } else {
    $_ds_moodydata_dsdb_passwd = $ds_moodydata::ds_moodydata_dsdb_passwd
  }

  if $ds_moodydata::ds_moodydata_searchdb == undef {
    $_ds_moodydata_searchdb = $ds_moodydata::params::ds_moodydata_searchdb
  } else {
    $_ds_moodydata_searchdb = $ds_moodydata::ds_moodydata_searchdb
  }

  if $ds_moodydata::ds_moodydata_searchdbname == undef {
    $_ds_moodydata_searchdbname = $ds_moodydata::params::ds_moodydata_searchdbname
  } else {
    $_ds_moodydata_searchdbname = $ds_moodydata::ds_moodydata_searchdbname
  }

  if $ds_moodydata::ds_moodydata_searchdb_user == undef {
    $_ds_moodydata_searchdb_user = $ds_moodydata::params::ds_moodydata_searchdb_user
  } else {
    $_ds_moodydata_searchdb_user = $ds_moodydata::ds_moodydata_searchdb_user
  }

  if $ds_moodydata::ds_moodydata_searchdb_passwd == undef {
    $_ds_moodydata_searchdb_passwd = $ds_moodydata::params::ds_moodydata_searchdb_passwd
  } else {
    $_ds_moodydata_searchdb_passwd = $ds_moodydata::ds_moodydata_searchdb_passwd
  }

  $ds_moodydata_deploy_dir = "/fr/deploy/${ds_moodydata_app_name}"
  $ds_moodydata_frlogdir = "/frlogdir/${ds_moodydata_app_name}"
  $ds_moodydata_jdk_dir = "/fr/third-party/${_ds_moodydata_jdk_version}"
  $ds_moodydata_output_dir = "${_ds_moodydata_output_rootdir}/data"

  class{'ds_moodydata::install':
    require => Class['fr_global::params'],
  }
}
