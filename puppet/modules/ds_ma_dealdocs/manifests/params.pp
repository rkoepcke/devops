class
  ds_ma_dealdocs::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_ma_dealdocs_jdk_version = "1.8.0_66"
  $ds_ma_dealdocs_user = "serviceprod"
  $ds_ma_dealdocs_group = "engops"
  $ds_ma_dealdocs_doc_solr_url = "http://stage.doc.solr.firstrain.com/solr/doc"
  $ds_ma_dealdocs_doc_api_url = "http://updateApiUrl=http://sew4-l.ca.firstrain.net:8090"
}
