class
  ds_ma_dealdocs::install
inherits
  ds_ma_dealdocs
{

  $ds_ma_dealdocs_required_paths = [
    $ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir,
    "$ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir/bin",
    "$ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir/conf",
    "$ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir/lib",
    "$ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir/pllib",
    "$ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir/plconf",
    $ds_ma_dealdocs::ds_ma_dealdocs_frlogdir,
    "${ds_ma_dealdocs::ds_ma_dealdocs_frlogdir}/logs",
    "${ds_ma_dealdocs::ds_ma_dealdocs_frlogdir}/dpflogs",
  ]

  file { $ds_ma_dealdocs_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group  => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    before => File['fetch ep dbf library tar file'],
  }

  file {'fetch ep dbf library tar file':
     ensure  => 'file',
     require => File[$ds_ma_dealdocs_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
     group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
     source  => "puppet:///modules/ds_ma_dealdocs/dpf-lib.tar"
  }

   exec { 'extract-ep-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_ma_dealdocs_deploy_dir}/lib",
     require => File[$ds_ma_dealdocs_required_paths],
     user    => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
     group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
     path    => '/bin',
  }

  File['fetch ep dbf library tar file'] -> Exec['extract-ep-dpf-lib']

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    content => template('ds_ma_dealdocs/log4j2.xml.erb'),
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    content => template('ds_ma_dealdocs/config.properties.erb'),
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0755',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    content => template('ds_ma_dealdocs/startup.sh.erb'),
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0755',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    content => template('ds_ma_dealdocs/shutdown.sh.erb'),
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/plconf/MADealDocsPipeline.xml":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    content => template('ds_ma_dealdocs/MADealDocsPipeline.xml.erb'),
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/dpf-1.2.jar",
  }


  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/bsf-2.4.0.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/bsf-2.4.0.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/groovy-2.3.10-sources.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/groovy-2.3.10-sources.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/groovy-all-2.3.10.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/groovy-all-2.3.10.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/ivy-2.3.0.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/ivy-2.3.0.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/ivy-2.3.0-sources.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/ivy-2.3.0-sources.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/servlet-api-2.4.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/servlet-api-2.4.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/pllib/MADealDocs.jar":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/MADealDocs.jar",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/spring-config.xml",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/castormapping.xml",
  }

  file { "${ds_ma_dealdocs::ds_ma_dealdocs_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_ma_dealdocs_required_paths],
    mode    => '0644',
    owner   => $ds_ma_dealdocs::_ds_ma_dealdocs_user,
    group   => $ds_ma_dealdocs::_ds_ma_dealdocs_group,
    source  =>  "puppet:///modules/ds_ma_dealdocs/castor.properties",
  }

  file { "/etc/init.d/frmadealdocs":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_ma_dealdocs/frmadealdocs.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frmadealdocs.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frmadealdocs'],
        owner   => root,
        group   => root,
        content => template('ds_ma_dealdocs/frmadealdocs.service.erb'),
      }
  }

}
