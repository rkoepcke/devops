class
  ds_ma_dealdocs
  (
    $ds_ma_dealdocs_jdk_version = undef,
    $ds_ma_dealdocs_user = undef,
    $ds_ma_dealdocs_group = undef,
    $ds_ma_dealdocs_doc_solr_url = undef,
    $ds_ma_dealdocs_api_url = undef,
    $ds_ma_dealdocs_jmxport = undef,
    $ds_ma_dealdocs_app_name = "frmadealdocs",
  )
{
  include ds_ma_dealdocs::params

  notify { 'ds_ma_dealdocs deploy dir':
    message => $_deploy_dir
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_jdk_version == undef {
    $_ds_ma_dealdocs_jdk_version = $ds_ma_dealdocs::params::ds_ma_dealdocs_jdk_version
  } else {
    $_ds_ma_dealdocs_jdk_version = $ds_ma_dealdocs::ds_ma_dealdocs_jdk_version
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_user == undef {
    $_ds_ma_dealdocs_user = $ds_ma_dealdocs::params::ds_ma_dealdocs_user
  } else {
    $_ds_ma_dealdocs_user = $ds_ma_dealdocs::ds_ma_dealdocs_user
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_group == undef {
    $_ds_ma_dealdocs_group = $ds_ma_dealdocs::params::ds_ma_dealdocs_group
  } else {
    $_ds_ma_dealdocs_group = $ds_ma_dealdocs::ds_ma_dealdocs_group
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_doc_solr_url == undef {
    $_ds_ma_dealdocs_doc_solr_url = $ds_ma_dealdocs::params::ds_ma_dealdocs_doc_solr_url
  } else {
    $_ds_ma_dealdocs_doc_solr_url = $ds_ma_dealdocs::ds_ma_dealdocs_doc_solr_url
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_api_url == undef {
    $_ds_ma_dealdocs_api_url = $ds_ma_dealdocs::params::ds_ma_dealdocs_api_url
  } else {
    $_ds_ma_dealdocs_api_url = $ds_ma_dealdocs::ds_ma_dealdocs_api_url
  }

  if $ds_ma_dealdocs::ds_ma_dealdocs_jmxport == undef {
    $_ds_ma_dealdocs_jmxport = $ds_ma_dealdocs::params::ds_ma_dealdocs_jmxport
  } else {
    $_ds_ma_dealdocs_jmxport = $ds_ma_dealdocs::ds_ma_dealdocs_jmxport
  }

  $ds_ma_dealdocs_deploy_dir = "/fr/deploy/${ds_ma_dealdocs_app_name}"
  $ds_ma_dealdocs_frlogdir = "/frlogdir/${ds_ma_dealdocs_app_name}"
  $ds_ma_dealdocs_jdk_dir = "/fr/third-party/${_ds_ma_dealdocs_jdk_version}"
  $ds_ma_dealdocs_pl_lib = "${ds_ma_dealdocs_deploy_dir}/pllib"
  $ds_ma_dealdocs_pl_conf = "${ds_ma_dealdocs_deploy_dir}/plconf"

  class{'ds_ma_dealdocs::install':
    require => Class['fr_global::params'],
  }
}
