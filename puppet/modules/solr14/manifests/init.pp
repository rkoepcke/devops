# init class for SOLR module.
class
  solr14
  (
    $solr14_jdk_version = undef,
    $solr14_tp_dir = undef,
    $solr14_deploy_dir  = undef,
    $solr14_services_dir = undef,
    $solr14_frlogdir    = undef,
    $solr14_tomcat_user = undef,
    $solr14_tomcat_group = undef,
    $solr14_app_name = undef,
    $solr14_data_dir = undef,
    $solr14_http_port = undef,
    $solr14_https_port = undef,
    $solr14_shutdown_port = undef,
    $solr14_ajp_port = undef,
    $solr14_jmxremote_port = undef,
    $solr14_tomcat_version = undef,
  )
{
  include solr14::params

  if $solr14_app_name == undef {
    $_solr14_app_name = $solr14::params::solr14_app_name
  } else {
    $_solr14_app_name = $solr14_app_name
  }

  if $solr14_deploy_dir == undef {
    $_solr14_deploy_dir = $solr14::params::solr14_deploy_dir
  }else {
    $_solr14_deploy_dir = $solr14_deploy_dir
  }

  if $solr14_services_dir == undef {
    $_solr14_services_dir = $solr14::params::solr14_services_dir
  } else {
    $_solr14_services_dir = $solr14_services_dir
  }

  if $solr14_frlogdir == undef {
    $_solr14_frlogdir = "${solr14::params::solr14_frlogdir}/${_solr14_app_name}"
  } else {
    $_solr14_frlogdir = "${solr14_frlogdir}/${_solr14_app_name}"
  }

  if $solr14_tp_dir == undef {
    $_solr14_tp_dir = $solr14::params::solr14_tp_dir
  } else {
    $_solr14_tp_dir = $solr14_tp_dir
  }

  if $solr14_jdk_version == undef {
    $_solr14_jdk_version = $solr14::params::solr14_jdk_version
  } else {
    $_solr14_jdk_version = $solr14_jdk_version
  }

  if $solr14_http_port==undef {
    $_solr14_http_port = $solr14::params::solr14_http_port
  } else {
    $_solr14_http_port = $solr14_http_port
  }

  if $solr14_https_port == undef {
    $_solr14_https_port = $solr14::params::solr14_https_port
  } else {
    $_solr14_https_port = $solr14_https_port
  }

  if $solr14_ajp_port==undef {
    $_solr14_ajp_port = $solr14::params::solr14_ajp_port
  } else {
    $_solr14_ajp_port = $solr14_ajp_port
  }

  if $solr14_shutdown_port==undef {
    $_solr14_shutdown_port = $solr14::params::solr14_shutdown_port
  } else {
    $_solr14_shutdown_port = $solr14_shutdown_port
  }

  if $solr14_tomcat_user == undef {
    $_solr14_tomcat_user = $solr14::params::solr14_tomcat_user
  } else {
    $_solr14_tomcat_user = $solr14_tomcat_user
  }

  if $solr14_tomcat_group == undef {
    $_solr14_tomcat_group = $solr14::params::solr14_tomcat_group
  } else {
    $_solr14_tomcat_group = $solr14_tomcat_group
  }

  if $solr14_jmxremote_port == undef {
    $_solr14_jmxremote_port = $solr14::params::solr14_jmx_port
  } else {
    $_solr14_jmxremote_port = $solr14_jmxremote_port
  }

  if $solr14_data_dir == undef {
    $_solr14_data_dir = $solr14::params::solr14_data_dir
  } else {
    $_solr14_data_dir = $solr14_data_dir
  }

  if $solr14_tomcat_version == undef {
    $_solr14_tomcat_version = "${solr14::params::solr14_tomcat_version}"
  } else {
    $_solr14_tomcat_version = $solr14_tomcat_version
  }

  $solr14_tomcat_home_dir = "${fr_global::params::dir_bin}/apache-tomcat-${_solr14_tomcat_version}"
  $solr14_jdk_dir = "${_solr14_tp_dir}/${_solr14_jdk_version}"
  $solr14_conf = "${_solr14_services_dir}/conf"
  $solr14_bin = "${_solr14_services_dir}/bin"
  $solr14_lib = "${_solr14_services_dir}/lib"
  $solr14_logs = "${_solr14_frlogdir}/logs"
  $solr14_docroot = $_solr14_deploy_dir

  class{'solr14::install':

  }
}
