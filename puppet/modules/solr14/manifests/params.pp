# Parameter class for the SOLR module.
class
  solr14::params
{
  include fr_global::params

  # Needs to be a global param at some point
  $solr14_deploy_dir = "${fr_global::params::dir_deploy}/solrwebapp14"
  $solr14_services_dir = "${fr_global::params::dir_services}/frsolrtomcat14"
  $solr14_frlogdir = $fr_global::params::dir_frlogdir
  $solr14_jdk_version = 'jdk1.6.0_30'
  $solr14_tp_dir = '/fr/third-party'

  $solr14_tomcat_version = '7.0.47'
  $solr14_tomcat_home_dir = "${fr_global::params::dir_bin}/apache-tomcat-7.0.47"
  $solr14_data_dir = '/frdata/repository14'

  $solr14_shutdown_port = 8005
  $solr14_http_port = 8080
  $solr14_https_port = 8443
  $solr14_ajp_port = 8009
  $solr14_jmx_port = 4444
  $solr14_tomcat_user = "serviceprod"
  $solr14_tomcat_group = "serviceprod"
  $solr14_app_name = "frsolrtomcat14"
}
