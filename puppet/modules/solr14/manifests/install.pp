# Install class for the SOLR 4.7.2 install
class
  solr14::install
inherits
  solr14
{

  $solr14_required_paths = [
    $solr14::_solr14_deploy_dir,
    $solr14::_solr14_services_dir,
    "${solr14::_solr14_services_dir}/bin",
    "${solr14::_solr14_services_dir}/logs",
    "${solr14::_solr14_services_dir}/conf",
    "${solr14::_solr14_services_dir}/conf/Catalina",
    "${solr14::_solr14_services_dir}/conf/Catalina/localhost",
    "${solr14::_solr14_services_dir}/lib",
    "${solr14::_solr14_services_dir}/temp",
    "${solr14::_solr14_services_dir}/work",
    $solr14::_solr14_frlogdir,
    "${solr14::_solr14_frlogdir}/logs",
    "${solr14::_solr14_frlogdir}/logs/accesslogs",
    $solr14::_solr14_data_dir,
    "${solr14::_solr14_data_dir}/multicore",
  ]

  $solr14_deploy_req_paths = [
    "${solr14::_solr14_deploy_dir}/admin",
    "${solr14::_solr14_deploy_dir}/css",
    "${solr14::_solr14_deploy_dir}/img",
    "${solr14::_solr14_deploy_dir}/js",
    "${solr14::_solr14_deploy_dir}/META-INF",
    "${solr14::_solr14_deploy_dir}/WEB-INF",
    "${solr14::_solr14_deploy_dir}/WEB-INF/classes",
    "${solr14::_solr14_deploy_dir}/WEB-INF/lib",
    "${solr14::_solr14_deploy_dir}/WEB-INF/tags",
  ]

  file { $solr14_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $solr14::_solr14_tomcat_user,
    group  => $solr14::_solr14_tomcat_group,
    before => File['fetch solr14 conf web.xml file'],
  }

  file { $solr14_deploy_req_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $solr14::_solr14_tomcat_user,
    group  => $solr14::_solr14_tomcat_group,
    before => File['fetch solr14 application web.xml file'],
  }

  file { 'fetch solr14 application web.xml file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/WEB-INF/web.xml",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/web.xml",
  }

  file { 'fetch solr14 conf web.xml file':
    ensure  =>  'file',
    require =>  File[$solr14_required_paths],
    path    =>  "${solr14::solr14_conf}/web.xml",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/web.conf.xml",
  }

  file { 'fetch solr14 catalina.policy file':
    ensure  =>  'file',
    require =>  File[$solr14_required_paths],
    path    =>  "${solr14::solr14_conf}/catalina.policy",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/catalina.policy",
  }

  file { 'fetch solr14 catalina.properties file':
    ensure  =>  'file',
    require =>  File[$solr14_required_paths],
    path    =>  "${solr14::solr14_conf}/catalina.properties",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/catalina.properties",
  }

  file { 'fetch solr14 context.xml file':
    ensure  =>  'file',
    require =>  File[$solr14_required_paths],
    path    =>  "${solr14::solr14_conf}/context.xml",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/context.xml",
  }

  file { 'fetch solr14admin.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14admin.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14admin.tar",
    before  =>  File['fetch solr14css.tar file'],
  }

  exec { 'extract-solr14admin-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14admin.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14admin-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14admin.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14admin.tar file'] -> Exec['extract-solr14admin-tar'] -> Exec['delete-solr14admin-tar']

  file { 'fetch solr14css.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14css.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14css.tar",
    before  =>  File['fetch solr14Files.tar file'],
  }

  exec { 'extract-solr14css-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14css.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14css-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14css.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14css.tar file'] -> Exec['extract-solr14css-tar'] -> Exec['delete-solr14css-tar']

  file { 'fetch solr14Files.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14Files.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14Files.tar",
    before  =>  File['fetch solr14img.tar file'],
  }

  exec { 'extract-solr14Files-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14Files.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14Files-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14Files.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14Files.tar file'] -> Exec['extract-solr14Files-tar'] -> Exec['delete-solr14Files-tar']

  file { 'fetch solr14img.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14img.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14img.tar",
    before  =>  File['fetch solr14JavaScript.tar file'],
  }

  exec { 'extract-solr14img-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14img.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14img-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14img.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14img.tar file'] -> Exec['extract-solr14img-tar'] -> Exec['delete-solr14img-tar']

  file { 'fetch solr14JavaScript.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14JavaScript.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14JavaScript.tar",
    before  =>  File['fetch solr14meta.tar file'],
  }

  exec { 'extract-solr14JavaScript-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14JavaScript.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14JavaScript-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14JavaScript.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14JavaScript.tar file'] -> Exec['extract-solr14JavaScript-tar'] -> Exec['delete-solr14JavaScript-tar']

  file { 'fetch solr14meta.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14meta.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14meta.tar",
    before  =>  File['fetch solr14tag.tar file'],
  }

  exec { 'extract-solr14meta-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14meta.tar --directory ${solr14::_solr14_deploy_dir}",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14meta-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14meta.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14meta.tar file'] -> Exec['extract-solr14meta-tar'] -> Exec['delete-solr14meta-tar']

  file { 'fetch solr14tag.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14tag.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14tag.tar",
    before  =>  File['fetch solr14Lib.tar file'],
  }

  exec { 'extract-solr14tag-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14tag.tar --directory ${solr14::_solr14_deploy_dir}/WEB-INF/tags",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14tag-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14tag.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14tag.tar file'] -> Exec['extract-solr14tag-tar'] -> Exec['delete-solr14tag-tar']

  file { 'fetch solr14Lib.tar file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/solr14Lib.tar",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/solr14Lib.tar",
  }

  exec { 'extract-solr14Lib-tar':
    command => "tar -xvf ${solr14::_solr14_deploy_dir}/solr14Lib.tar --directory ${solr14::_solr14_deploy_dir}/WEB-INF/lib",
    require => File[$solr14_deploy_req_paths],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    path    => '/bin',
  }

  exec { 'delete-solr14Lib-tar':
    command => "rm -f ${solr14::_solr14_deploy_dir}/solr14Lib.tar",
    require =>  File[$solr14_deploy_req_paths],
    path => '/usr/bin:/bin',
    user => $solr14::_solr14_tomcat_user,
  }

  File['fetch solr14Lib.tar file'] -> Exec['extract-solr14Lib-tar'] -> Exec['delete-solr14Lib-tar']

  exec { 'copy-tomcat-bin-files for solr14':
    command => "cp ${solr14::solr14_tomcat_home_dir}/bin/*.sh ${solr14::solr14_bin}",
    require => [File[$solr14_required_paths],Class['tomcat::install']],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    cwd     => $solr14::solr14_bin,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${solr14::solr14_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files for solr14':
    command => "cp ${solr14::solr14_tomcat_home_dir}/bin/*.jar ${solr14::solr14_bin}",
    require => [File[$solr14_required_paths],Class['tomcat::install']],
    user    => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    cwd     => $solr14::solr14_bin,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${solr14::solr14_bin}/bootstrap.jar",
  }

  file { "${solr14::solr14_conf}/server.xml":
    ensure  => file,
    require => File[$solr14_required_paths],
    mode    => '0644',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/server.xml.erb'),
  }

  file { "${solr14::solr14_docroot}/WEB-INF/classes/log4j.properties":
    ensure  => file,
    require => File[$solr14_required_paths],
    mode    => '0644',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/log4j.properties.erb'),
  }

  file { 'fetch log4j template file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/WEB-INF/classes/log4j.properties.template",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/log4j.properties.template",
  }

  file { 'fetch log4j temp file':
    ensure  =>  'file',
    require =>  File[$solr14_deploy_req_paths],
    path    =>  "${solr14::_solr14_deploy_dir}/WEB-INF/classes/log4j.properties_temp",
    owner   =>  $solr14::_solr14_tomcat_user,
    group   =>  $solr14::_solr14_tomcat_group,
    mode    =>  '0644',
    source  =>  "puppet:///modules/solr14/log4j.properties_temp",
  }

  file { "${solr14::solr14_conf}/logging.properties":
    ensure  => file,
    require => File[$solr14_required_paths],
    mode    => '0644',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/logging.properties.erb'),
  }

  file { "${solr14::solr14_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$solr14_required_paths],
    mode    => '0644',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/tomcat-users.xml.erb'),
  }

  file { "${solr14::solr14_conf}/Catalina/localhost/solr.xml":
    ensure  => file,
    require => File[$solr14_required_paths],
    mode    => '0644',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/Catalina/localhost/solr.xml.erb'),
  }

  file { "${solr14::solr14_bin}/setenv.sh":
    ensure  => file,
    require => [File[$solr14_required_paths],Exec['copy-tomcat-bin-files for solr14']],
    mode    => '0755',
    owner   => $solr14::_solr14_tomcat_user,
    group   => $solr14::_solr14_tomcat_group,
    content => template('solr14/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/${solr14::_solr14_app_name}":
    ensure  => file,
    mode    => '0755',
    content => template('solr14/frsolrtomcat14.erb'),
  }

}
