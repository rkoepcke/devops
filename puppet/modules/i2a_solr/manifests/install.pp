# Install class for the SOLR 4.7.2 install
class
  i2a_solr::install
inherits
  i2a_solr
{

  notify { 'Solr-Data-Dir':
    message =>  $i2a_solr::fr_data_dir
  }

  notify { 'Solr-Log-Dir':
    message =>  $i2a_solr::i2a_solr_logs
  }
  $i2a_solr_required_paths = [
    $i2a_solr::_i2a_solr_deploy_dir,
    $i2a_solr::_i2a_solr_services_dir,
    "${i2a_solr::_i2a_solr_services_dir}/appconf",
    "${i2a_solr::_i2a_solr_services_dir}/bin",
    "${i2a_solr::_i2a_solr_services_dir}/logs",
    "${i2a_solr::_i2a_solr_services_dir}/conf",
    "${i2a_solr::_i2a_solr_services_dir}/conf/Catalina",
    "${i2a_solr::_i2a_solr_services_dir}/conf/Catalina/localhost",
    "${i2a_solr::_i2a_solr_services_dir}/lib",
    "${i2a_solr::_i2a_solr_services_dir}/temp",
    "${i2a_solr::_i2a_solr_services_dir}/work",
    $i2a_solr::_i2a_solr_frlogdir,
    "${i2a_solr::_i2a_solr_frlogdir}/logs",
    "${i2a_solr::_i2a_solr_frlogdir}/logs/accesslogs",
    $i2a_solr::_fr_data_dir,
  ]

  file { $i2a_solr_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $i2a_solr::_i2a_solr_tomcat_user,
    group  => $i2a_solr::_i2a_solr_tomcat_group,
    before => Notify['finished-i2a_solr-war-fetch']
  }

  notify { 'start-i2a_solr-war-fetch':
    name    =>  'STARTING Solr War Fetch',
    message =>  "${fr_global::params::dir_download}/${i2a_solr::warFile}"
  }

  notify { 'finished-i2a_solr-war-fetch':
    name    =>  'FINISHED Solr War Fetch',
    message =>  "${fr_global::params::dir_download}/${i2a_solr::warFile}"
  }

  file { 'fetch i2a_solr war file':
    ensure  =>  'file',
    require =>  File[$i2a_solr_required_paths],
    path    =>  "${i2a_solr::_i2a_solr_deploy_dir}/solr.war",
    owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
    group   =>  $i2a_solr::_i2a_solr_tomcat_group,
    source  =>  "puppet:///modules/i2a_solr/${i2a_solr::warFile}",
    before  =>  Notify['FINISHED Solr War Fetch']
  }

  notify { 'start-i2a_solr-jar-fetch':
    name    =>  'STARTING Solr Jar fetch',
    message =>  "${fr_global::params::dir_download}/${i2a_solr::solrJar}}"
  }

  notify { 'finished-i2a_solr-jar-fetch':
    name    =>  'FINISHED Solr Jar fetch',
    message =>  "${fr_global::params::dir_download}/${i2a_solr::solrJar}"
  }

  if ( $::hostname == "PI2ASOLR1-L" ) or ( $::hostname == "PI2ASOLR2-L" ) {
      file { 'fetch production Pivot war file':
	ensure  =>  'file',
	require =>  File[$i2a_solr_required_paths],
	path    =>  "${i2a_solr::_i2a_solr_deploy_dir}/Pivot.war",
	owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
	group   =>  $i2a_solr::_i2a_solr_tomcat_group,
	source  =>  "puppet:///modules/i2a_solr/Pivot.war.prod",
	before  =>  File['fetch i2a_solr jar files'],
      }

      exec { 'delete-pivot-prod-dir':
	command => "rm -rf ${i2a_solr::_i2a_solr_deploy_dir}/Pivot",
	require =>  File[$i2a_solr_required_paths],
	path => '/usr/bin:/bin',
	user => root,
	onlyif => "test -d ${i2a_solr::_i2a_solr_deploy_dir}/Pivot"
      }

      File['fetch production Pivot war file'] -> Exec['delete-pivot-prod-dir']
  }
  else {
      file { 'fetch staging Pivot war file':
	ensure  =>  'file',
	require =>  File[$i2a_solr_required_paths],
	path    =>  "${i2a_solr::_i2a_solr_deploy_dir}/Pivot.war",
	owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
	group   =>  $i2a_solr::_i2a_solr_tomcat_group,
	source  =>  "puppet:///modules/i2a_solr/Pivot.war.stage",
	before  =>  File['fetch i2a_solr jar files'],
      }

      exec { 'delete-pivot-stage-dir':
	command => "rm -rf ${i2a_solr::_i2a_solr_deploy_dir}/Pivot",
	require =>  File[$i2a_solr_required_paths],
	path => '/usr/bin:/bin',
	user => root,
	onlyif => "test -d ${i2a_solr::_i2a_solr_deploy_dir}/Pivot"
      }

      File['fetch staging Pivot war file'] -> Exec['delete-pivot-stage-dir']
  }

  file { 'fetch I2AServices war file':
    ensure  =>  'file',
    require =>  File[$i2a_solr_required_paths],
    path    =>  "${i2a_solr::_i2a_solr_deploy_dir}/I2AServices.war",
    owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
    group   =>  $i2a_solr::_i2a_solr_tomcat_group,
    source  =>  "puppet:///modules/i2a_solr/I2AServices.war",
    before  =>  File['fetch i2a_solr jar files'],
  }

  exec { 'delete-i2aservice-dir':
    command => "rm -rf ${i2a_solr::_i2a_solr_deploy_dir}/I2AServices",
    require =>  File[$i2a_solr_required_paths],
    path => '/usr/bin:/bin',
    user => root,
    onlyif => "test -d ${i2a_solr::_i2a_solr_deploy_dir}/I2AServices"
  }

  File['fetch I2AServices war file'] -> Exec['delete-i2aservice-dir']

  file { 'fetch i2a_solr jar files':
    ensure  =>  'file',
    require =>  File[$i2a_solr_required_paths],
    path    =>  "${fr_global::params::dir_download}/${i2a_solr::solrJar}",
    source  =>  "puppet:///modules/i2a_solr/${i2a_solr::solrJar}",
    before  =>  Notify['FINISHED Solr Jar fetch'],
  }

  exec { 'copy-tomcat-bin-files':
    command => "cp ${i2a_solr::i2a_tomcat_home_dir}/bin/*.sh ${i2a_solr::i2a_solr_bin}",
    require => [File[$i2a_solr_required_paths],Class['tomcat::install']],
    user    => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${i2a_solr::i2a_solr_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files':
    command => "cp ${i2a_solr::i2a_tomcat_home_dir}/bin/*.jar ${i2a_solr::i2a_solr_bin}",
    require => [File[$i2a_solr_required_paths],Class['tomcat::install']],
    user    => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${i2a_solr::i2a_solr_bin}/bootstrap.jar",
  }

  $i2a_solrj_source = "${fr_global::params::dir_download}/${i2a_solr::solrJar}"
  $i2a_solrj_target = "${i2a_solr::_i2a_solr_services_dir}/lib"
  $sjf = "i2a_solr_i2a_solrj-${i2a_solr::version_major}.${i2a_solr::version_minor}.${i2a_solr::version_patch}.jar"

  exec { 'extract-i2a_solr-jars':
    command => "tar -xvf ${i2a_solrj_source} --directory ${i2a_solrj_target}",
    require => File[$i2a_solr_required_paths],
    user    => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${i2a_solr::_i2a_solr_services_dir}/lib/solr-solrj-4.7.2.jar",
  }

  exec { 'remove-i2a-solr-tarfile':
    command => "rm -f ${i2a_solrj_source}",
    path    => '/bin'
  }

  File['fetch i2a_solr jar files'] -> Exec['extract-i2a_solr-jars'] -> Exec['remove-i2a-solr-tarfile']

  file { 'fetch i2asolr catalina properties file':
    ensure  =>  'file',
    require =>  File[$i2a_solr_required_paths],
    path    =>  "${i2a_solr::i2a_solr_conf}/catalina.properties",
    owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
    group   =>  $i2a_solr::_i2a_solr_tomcat_group,
    source  =>  "puppet:///modules/i2a_solr/catalina.properties",
  }

  file { 'fetch i2asolr spring-config.xml file':
    ensure  =>  'file',
    require =>  File[$i2a_solr_required_paths],
    path    =>  "${i2a_solr::_i2a_solr_services_dir}/appconf/spring-config.xml",
    owner   =>  $i2a_solr::_i2a_solr_tomcat_user,
    group   =>  $i2a_solr::_i2a_solr_tomcat_group,
    source  =>  "puppet:///modules/i2a_solr/spring-config.xml",
  }

  file { "${i2a_solr::i2a_solr_conf}/server.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/server.xml.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/log4j.properties":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/log4j.properties.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/logging.properties":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/logging.properties.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/catalina.policy":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/catalina.policy.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/context.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/context.xml.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/tomcat-users.xml.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/web.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/web.xml.erb'),
  }

  file { "${i2a_solr::i2a_solr_conf}/Catalina/localhost/solr.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/solr.xml.erb'),
  }

  file { "${i2a_solr::_fr_data_dir}/solr.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner  => $i2a_solr::_i2a_solr_tomcat_user,
    group  => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/solr-home.xml.erb'),
  }

  file { "${i2a_solr::i2a_solr_bin}/setenv.sh":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0755',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/setenv.sh.erb'),
  }
  
  file { "${i2a_solr::_i2a_solr_services_dir}/appconf/config.properties":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/app.config.properties.erb'),
  }
  
  file { "${i2a_solr::_i2a_solr_services_dir}/appconf/marketingConfig.properties":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/app.marketingConfig.properties.erb'),
  }

  file { "${i2a_solr::_i2a_solr_services_dir}/appconf/log4j2.xml":
    ensure  => file,
    require => File[$i2a_solr_required_paths],
    mode    => '0644',
    owner   => $i2a_solr::_i2a_solr_tomcat_user,
    group   => $i2a_solr::_i2a_solr_tomcat_group,
    content => template('i2a_solr/app.log4j2.xml.erb'),
  }

  file { "/etc/init.d/${i2a_solr::_i2a_solr_app_name}":
    ensure  => file,
    mode    => '0755',
    content => template('i2a_solr/frsolrtomcat472.erb'),
  }

   if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frsolrtomcat472.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frsolrtomcat472'],
        owner   => root,
        group   => root,
        content => template('i2a_solr/frsolrtomcat472.service.erb'),
      }
  }

}
