# init class for SOLR module.
class
  i2a_solr
  (
    $i2a_solr_jdk_version = undef,
    $i2a_solr_tp_dir = undef,
    $i2a_solr_deploy_dir  = undef,
    $i2a_solr_services_dir = undef,
    $i2a_solr_frlogdir    = undef,
    $i2a_solr_tomcat_user = undef,
    $i2a_solr_tomcat_group = undef,
    $i2a_solr_app_name = undef,
    $fr_data_dir = undef,
    $i2a_solr_http_port = undef,
    $i2a_solr_https_port = undef,
    $i2a_solr_shutdown_port = undef,
    $i2a_solr_ajp_port = undef,
    $i2a_solr_jmxremote_port = undef,
    $i2a_version_patch = undef,
    $i2a_version_minor = undef, 
    $i2a_version_major = undef,
    $i2a_solr_max_memory = undef,
    $i2a_tomcat_version = undef,
  )
{
  include i2a_solr::params

  $_i2a_version_major  = $i2a_solr::params::i2a_version_major
  $_i2a_version_minor  = $i2a_solr::params::i2a_version_minor
  $_i2a_version_patch  = $i2a_solr::params::i2a_version_patch
  $version = "${_i2a_version_major}.${_i2a_version_minor}.${_i2a_version_patch}"
  $ident_ver = "${_i2a_version_major}${_i2a_version_minor}${_i2a_version_patch}"
  $warFile = "solr-${version}.war"
  $solrJar = "solr${ident_ver}JarFiles.tar"

  if $i2a_tomcat_version == undef {
    $i2a_tomcat_home_dir = "${fr_global::params::dir_bin}/apache-tomcat-7.0.47"
  }  else {
    $i2a_tomcat_home_dir = "${fr_global::params::dir_bin}/apache-tomcat-${i2a_tomcat_version}"
  }

  if $i2a_solr_app_name == undef {
    $_i2a_solr_app_name = "frsolrtomcat${ident_ver}"
  } else {
    $_i2a_solr_app_name = $i2a_solr_app_name
  }

  if $i2a_solr_deploy_dir == undef {
    $_i2a_solr_deploy_dir = "${i2a_solr::params::deploy_dir_default}/${_i2a_solr_app_name}"
  }else {
    $_i2a_solr_deploy_dir = "${i2a_solr_deploy_dir}/${_i2a_solr_app_name}"
  }

  if $i2a_solr_services_dir == undef {
    $_i2a_solr_services_dir = "${i2a_solr::params::services_dir_default}/${_i2a_solr_app_name}"
  } else {
    $_i2a_solr_services_dir = "${i2a_solr_services_dir}/${_i2a_solr_app_name}"
  }

  if $i2a_solr_frlogdir == undef {
    $_i2a_solr_frlogdir = "${i2a_solr::params::frlogdir_default}/${_i2a_solr_app_name}"
  } else {
    $_i2a_solr_frlogdir = "${i2a_solr_frlogdir}/${_i2a_solr_app_name}"
  }

  if $i2a_solr_tp_dir == undef {
    $_i2a_solr_tp_dir = $i2a_solr::params::i2a_solr_tp_dir_default
  } else {
    $_i2a_solr_tp_dir = $i2a_solr_tp_dir
  }

  if $i2a_solr_jdk_version == undef {
    $_i2a_solr_jdk_version = $i2a_solr::params::i2a_solr_jdk_version_default
  } else {
    $_i2a_solr_jdk_version = $i2a_solr_jdk_version
  }

  if $i2a_solr_http_port==undef {
    $_i2a_solr_http_port = $i2a_solr::params::i2a_solr_http_port
  } else {
    $_i2a_solr_http_port = $i2a_solr_http_port
  }

  if $i2a_solr_https_port == undef {
    $_i2a_solr_https_port = $i2a_solr::params::i2a_solr_https_port
  } else {
    $_i2a_solr_https_port = $i2a_solr_https_port
  }

  if $i2a_solr_ajp_port==undef {
    $_i2a_solr_ajp_port = $i2a_solr::params::i2a_solr_ajp_port
  } else {
    $_i2a_solr_ajp_port = $i2a_solr_ajp_port
  }

  if $i2a_solr_shutdown_port==undef {
    $_i2a_solr_shutdown_port = $i2a_solr::params::i2a_solr_shutdown_port
  } else {
    $_i2a_solr_shutdown_port = $i2a_solr_shutdown_port
  }

  if $i2a_solr_tomcat_user == undef {
    $_i2a_solr_tomcat_user = 'tomcat'
  } else {
    $_i2a_solr_tomcat_user = $i2a_solr_tomcat_user
  }

  if $i2a_solr_tomcat_group == undef {
    $_i2a_solr_tomcat_group = 'tomcat'
  } else {
    $_i2a_solr_tomcat_group = $i2a_solr_tomcat_group
  }

  if $i2a_solr_jmxremote_port == undef {
    $_i2a_solr_jmxremote_port = $i2a_solr::params::i2a_solr_jmx_port
  } else {
    $_i2a_solr_jmxremote_port = $i2a_solr_jmxremote_port
  }

  if $i2a_solr_max_memory == undef {
    $_i2a_solr_max_memory = $i2a_solr::params::i2a_solr_jmx_port
  } else {
    $_i2a_solr_max_memory = $i2a_solr_max_memory
  }

  if $fr_data_dir == undef {
    $_fr_data_dir = $i2a_solr::params::fr_data_dir
  } else {
    $_fr_data_dir = $fr_data_dir
  }

  $_tomcat_home_dir = $params::tomcat_home_dir
  $_tomcat_bin_dir = $params::tomcat_bin_dir
  $_tomcat_lib_dir = $params::tomcat_lib_dir
  $_i2a_solr_jdk_dir = "${_i2a_solr_tp_dir}/${_i2a_solr_jdk_version}"
  $i2a_solr_conf = "${_i2a_solr_services_dir}/conf"
  $i2a_solr_bin = "${_i2a_solr_services_dir}/bin"
  $i2a_solr_lib = "${_i2a_solr_services_dir}/lib"
  $i2a_solr_logs = "${_i2a_solr_frlogdir}/logs"
  $i2a_solr_docroot = $_i2a_solr_deploy_dir
  $i2a_solr_url = "${::fqdn}:${_i2a_solr_http_port}"

  class{'i2a_solr::install':
    require => Class['fr_global::params'],
  }
}
