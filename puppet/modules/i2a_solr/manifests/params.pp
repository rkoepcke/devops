# Parameter class for the SOLR module.
class
  i2a_solr::params
{
  include fr_global::params

  # Needs to be a global param at some point
  $fr_data_dir = '/frdata/repository'

  $deploy_dir_default = $fr_global::params::dir_deploy
  $services_dir_default = $fr_global::params::dir_services
  $frlogdir_default = $fr_global::params::dir_frlogdir
  $i2a_solr_jdk_version_default = 'jdk1.6.0_30'
  $i2a_solr_tp_dir_default = '/fr/third-party'
  $tomcat_name_default = 'apache-tomcat-7.0.47'
  $i2a_solr_max_memory = '4096M'

  $tomcat_home_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}"
  $tomcat_bin_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}/bin"
  $tomcat_lib_dir = "${fr_global::params::dir_bin}/${tomcat_name_default}/lib"

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $i2a_version_major = 4
  $i2a_version_minor = 7
  $i2a_version_patch = 2

  $i2a_solr_shutdown_port = 8005
  $i2a_solr_http_port = 8080
  $i2a_solr_https_port = 8443
  $i2a_solr_ajp_port = 8009
  $i2a_solr_jmx_port = 4444
}
