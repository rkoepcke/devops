class
  ds_creditrel
  (
    $ds_creditrel_jdk_version = undef,
    $ds_creditrel_user = undef,
    $ds_creditrel_group = undef,
    $ds_creditrel_doc_solr_url = undef,
    $ds_creditrel_id_start = undef,
    $ds_creditrel_model_api_url = undef,
    $ds_creditrel_storage_service_url = undef,
    $ds_creditrel_app_name = "frcreditrelscore",
  )
{
  include ds_creditrel::params

  notify { 'ds_creditrel deploy dir':
    message => $_deploy_dir
  }

  if $ds_creditrel::ds_creditrel_jdk_version == undef {
    $_ds_creditrel_jdk_version = $ds_creditrel::params::ds_creditrel_jdk_version
  } else {
    $_ds_creditrel_jdk_version = $ds_creditrel::ds_creditrel_jdk_version
  }

  if $ds_creditrel::ds_creditrel_user == undef {
    $_ds_creditrel_user = $ds_creditrel::params::ds_creditrel_user
  } else {
    $_ds_creditrel_user = $ds_creditrel::ds_creditrel_user
  }

  if $ds_creditrel::ds_creditrel_group == undef {
    $_ds_creditrel_group = $ds_creditrel::params::ds_creditrel_group
  } else {
    $_ds_creditrel_group = $ds_creditrel::ds_creditrel_group
  }

  if $ds_creditrel::ds_creditrel_model_api_url == undef {
    $_ds_creditrel_model_api_url = $ds_creditrel::params::ds_creditrel_model_api_url
  } else {
    $_ds_creditrel_model_api_url = $ds_creditrel::ds_creditrel_model_api_url
  }

  if $ds_creditrel::ds_creditrel_storage_service_url == undef {
    $_ds_creditrel_storage_service_url = $ds_creditrel::params::ds_creditrel_storage_service_url
  } else {
    $_ds_creditrel_storage_service_url = $ds_creditrel::ds_creditrel_storage_service_url
  }

  if $ds_creditrel::ds_creditrel_doc_solr_url == undef {
    $_ds_creditrel_doc_solr_url = $ds_creditrel::params::ds_creditrel_doc_solr_url
  } else {
    $_ds_creditrel_doc_solr_url = $ds_creditrel::ds_creditrel_doc_solr_url
  }

  if $ds_creditrel::ds_creditrel_id_start == undef {
    $_ds_creditrel_id_start = $ds_creditrel::params::ds_creditrel_id_start
  } else {
    $_ds_creditrel_id_start = $ds_creditrel::ds_creditrel_id_start
  }

  $ds_creditrel_deploy_dir = "/fr/deploy/${ds_creditrel_app_name}"
  $ds_creditrel_frlogdir = "/frlogdir/${ds_creditrel_app_name}"
  $ds_creditrel_jdk_dir = "/fr/third-party/${_ds_creditrel_jdk_version}"
  $ds_creditrel_pl_lib = "${ds_creditrel_deploy_dir}/pllib"
  $ds_creditrel_pl_conf = "${ds_creditrel_deploy_dir}/plconf"

  class{'ds_creditrel::install':
    require => Class['fr_global::params'],
  }
}
