class
  ds_creditrel::install
inherits
  ds_creditrel
{

  $ds_creditrel_required_paths = [
    $ds_creditrel::ds_creditrel_deploy_dir,
    "$ds_creditrel::ds_creditrel_deploy_dir/bin",
    "$ds_creditrel::ds_creditrel_deploy_dir/conf",
    "$ds_creditrel::ds_creditrel_deploy_dir/lib",
    "$ds_creditrel::ds_creditrel_deploy_dir/pllib",
    "$ds_creditrel::ds_creditrel_deploy_dir/plconf",
    $ds_creditrel::ds_creditrel_frlogdir,
    "${ds_creditrel::ds_creditrel_frlogdir}/logs",
    "${ds_creditrel::ds_creditrel_frlogdir}/dpflogs",
  ]

  file { $ds_creditrel_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_creditrel::_ds_creditrel_user,
    group  => $ds_creditrel::_ds_creditrel_group,
    before => File['fetch creditrel dbf library tar file'],
  }

  file {'fetch creditrel dbf library tar file':
     ensure  => 'file',
     require => File[$ds_creditrel_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_creditrel::_ds_creditrel_user,
     group   => $ds_creditrel::_ds_creditrel_group,
     source  => "puppet:///modules/ds_creditrel/dpf-lib.tar"
  }

   exec { 'extract-cr-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_creditrel_deploy_dir}/lib",
     require => File[$ds_creditrel_required_paths],
     user    => $ds_creditrel::_ds_creditrel_user,
     group   => $ds_creditrel::_ds_creditrel_group,
     path    => '/bin',
  }

  File['fetch creditrel dbf library tar file'] -> Exec['extract-cr-dpf-lib']

  file {'fetch creditrel library tar file':
     ensure  => 'file',
     require => File[$ds_creditrel_required_paths],
     path    => "${fr_global::params::dir_root}/download/crs-lib.tar",
     owner   => $ds_creditrel::_ds_creditrel_user,
     group   => $ds_creditrel::_ds_creditrel_group,
     source  => "puppet:///modules/ds_creditrel/crs-lib.tar"
  }

   exec { 'extract-crs-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/crs-lib.tar --directory ${ds_creditrel_deploy_dir}/pllib",
     require => File[$ds_creditrel_required_paths],
     user    => $ds_creditrel::_ds_creditrel_user,
     group   => $ds_creditrel::_ds_creditrel_group,
     path    => '/bin',
  }

  File['fetch creditrel library tar file'] -> Exec['extract-crs-lib']

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    content => template('ds_creditrel/log4j2.xml.erb'),
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    content => template('ds_creditrel/config.properties.erb'),
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0755',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    content => template('ds_creditrel/startup.sh.erb'),
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0755',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    content => template('ds_creditrel/shutdown.sh.erb'),
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/plconf/CreditRelevancyScoringPipeline.xml":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    content => template('ds_creditrel/CreditRelevancyScoringPipeline.xml.erb'),
  }


  file { "${ds_creditrel::ds_creditrel_deploy_dir}/DataProcessingFramework.jar":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/DataProcessingFramework-1.0-DS.jar",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/pllib/CreditRelevancyScoringPipeline.jar":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/CreditRelevancyScoringPipeline-1.0-DS.jar",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/pllib/CreditRelevancyScorer.jar":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/CreditRelevancyScorer-1.0-DS.jar",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/lib/Solr47Utils.jar":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/Solr47Utils-1.0-DS.jar",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/spring-config.xml",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/castormapping.xml",
  }

  file { "${ds_creditrel::ds_creditrel_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_creditrel_required_paths],
    mode    => '0644',
    owner   => $ds_creditrel::_ds_creditrel_user,
    group   => $ds_creditrel::_ds_creditrel_group,
    source  =>  "puppet:///modules/ds_creditrel/castor.properties",
  }

  file { "/etc/init.d/frcreditrelscore":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_creditrel/frcreditrelscore.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frcreditrelscore.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frcreditrelscore'],
        owner   => root,
        group   => root,
        content => template('ds_creditrel/frcreditrelscore.service.erb'),
      }
  }

}
