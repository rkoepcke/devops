class
  ds_creditrel::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_creditrel_jdk_version = "1.8.0_66"
  $ds_creditrel_user = "serviceprod"
  $ds_creditrel_group = "engops"
  $ds_creditrel_doc_solr_url = "http://sfrontend-rl.ca.firstrain.net:8080/solr/doc"
  $ds_creditrel_model_api_url = "http://sdsweb2-l.ca.firstrain.net:5000/api/creditRelevancy/v1.0"
  $ds_creditrel_storage_service_url = "http://stage.frstorageservice.firstrain.com/frstorageservice/extendmetav1.json"
}
