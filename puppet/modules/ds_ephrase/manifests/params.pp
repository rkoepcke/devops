class
  ds_ephrase::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_ephrase_jdk_version = "1.8.0_66"
  $ds_ephrase_user = "serviceprod"
  $ds_ephrase_group = "engops"
  $ds_ephrase_doc_solr_url = "http://sdsmysql1-l.ca.firstrain.net:8080"
  $ds_ephrase_sitedocid_start = 748428502
}
