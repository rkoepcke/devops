class
  ds_ephrase
  (
    $ds_ephrase_jdk_version = undef,
    $ds_ephrase_user = undef,
    $ds_ephrase_group = undef,
    $ds_ephrase_solr_url = undef,
    $ds_ephrase_sitedocid_start = undef,
    $ds_ephrase_jmxport = undef,
    $ds_ephrase_app_name = "frextractedphrases",
  )
{
  include ds_ephrase::params

  notify { 'ds_ephrase deploy dir':
    message => $_deploy_dir
  }

  if $ds_ephrase::ds_ephrase_jdk_version == undef {
    $_ds_ephrase_jdk_version = $ds_ephrase::params::ds_ephrase_jdk_version
  } else {
    $_ds_ephrase_jdk_version = $ds_ephrase::ds_ephrase_jdk_version
  }

  if $ds_ephrase::ds_ephrase_user == undef {
    $_ds_ephrase_user = $ds_ephrase::params::ds_ephrase_user
  } else {
    $_ds_ephrase_user = $ds_ephrase::ds_ephrase_user
  }

  if $ds_ephrase::ds_ephrase_group == undef {
    $_ds_ephrase_group = $ds_ephrase::params::ds_ephrase_group
  } else {
    $_ds_ephrase_group = $ds_ephrase::ds_ephrase_group
  }

  if $ds_ephrase::ds_ephrase_solr_url == undef {
    $_ds_ephrase_solr_url = $ds_ephrase::params::ds_ephrase_solr_url
  } else {
    $_ds_ephrase_solr_url = $ds_ephrase::ds_ephrase_solr_url
  }

  if $ds_ephrase::ds_ephrase_jmxport == undef {
    $_ds_ephrase_jmxport = $ds_ephrase::params::ds_ephrase_jmxport
  } else {
    $_ds_ephrase_jmxport = $ds_ephrase::ds_ephrase_jmxport
  }

  if $ds_ephrase::ds_ephrase_sitedocid_start == undef {
    $_ds_ephrase_sitedocid_start = $ds_ephrase::params::ds_ephrase_sitedocid_start
  } else {
    $_ds_ephrase_sitedocid_start = $ds_ephrase::ds_ephrase_sitedocid_start
  }

  $ds_ephrase_deploy_dir = "/fr/deploy/${ds_ephrase_app_name}"
  $ds_ephrase_frlogdir = "/frlogdir/${ds_ephrase_app_name}"
  $ds_ephrase_jdk_dir = "/fr/third-party/${_ds_ephrase_jdk_version}"
  $ds_ephrase_pl_lib = "${ds_ephrase_deploy_dir}/pllib"
  $ds_ephrase_pl_conf = "${ds_ephrase_deploy_dir}/plconf"

  class{'ds_ephrase::install':
    require => Class['fr_global::params'],
  }
}
