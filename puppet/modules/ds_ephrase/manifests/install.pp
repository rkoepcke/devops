class
  ds_ephrase::install
inherits
  ds_ephrase
{

  $ds_ephrase_required_paths = [
    $ds_ephrase::ds_ephrase_deploy_dir,
    "$ds_ephrase::ds_ephrase_deploy_dir/bin",
    "$ds_ephrase::ds_ephrase_deploy_dir/conf",
    "$ds_ephrase::ds_ephrase_deploy_dir/lib",
    "$ds_ephrase::ds_ephrase_deploy_dir/pllib",
    "$ds_ephrase::ds_ephrase_deploy_dir/plconf",
    $ds_ephrase::ds_ephrase_frlogdir,
    "${ds_ephrase::ds_ephrase_frlogdir}/logs",
    "${ds_ephrase::ds_ephrase_frlogdir}/dpflogs",
  ]

  file { $ds_ephrase_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_ephrase::_ds_ephrase_user,
    group  => $ds_ephrase::_ds_ephrase_group,
    before => File['fetch ep dbf library tar file'],
  }

  file {'fetch ep dbf library tar file':
     ensure  => 'file',
     require => File[$ds_ephrase_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_ephrase::_ds_ephrase_user,
     group   => $ds_ephrase::_ds_ephrase_group,
     source  => "puppet:///modules/ds_ephrase/dpf-lib.tar"
  }

   exec { 'extract-ep-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_ephrase_deploy_dir}/lib",
     require => File[$ds_ephrase_required_paths],
     user    => $ds_ephrase::_ds_ephrase_user,
     group   => $ds_ephrase::_ds_ephrase_group,
     path    => '/bin',
  }

  File['fetch ep dbf library tar file'] -> Exec['extract-ep-dpf-lib']

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/log4j2.xml.erb'),
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/config.properties.erb'),
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0755',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/startup.sh.erb'),
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0755',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/shutdown.sh.erb'),
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/plconf/ExtractedPhrases_Pipeline.xml":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/ExtractedPhrases_Pipeline.xml.erb'),
  }

  #file { "${ds_ephrase::ds_ephrase_deploy_dir}/plconf/ExtractedPhrasesTagger_Pipeline.xml":
    #ensure  => file,
    #require => File[$ds_ephrase_required_paths],
    #mode    => '0644',
    #owner   => $ds_ephrase::_ds_ephrase_user,
    #group   => $ds_ephrase::_ds_ephrase_group,
    #content => template('ds_ephrase/ExtractedPhrasesTagger_Pipeline.xml.erb'),
  #}

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/plconf/ExtractedPhrasesUpdater_Pipeline.xml":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    content => template('ds_ephrase/ExtractedPhrasesUpdater_Pipeline.xml.erb'),
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/DataProcessingFramework-1.0-DS.jar",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/lib/Solr47Utils.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/Solr47Utils-1.0-DS.jar",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/extractedphrases.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/ExtractedPhrasesPipeline-1.0-DS.jar",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/BasicUtils.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/BasicUtils-1.0-DS.jar",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/FRCoreNLP-3.5.2-DS.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/FRCoreNLP-3.5.2-DS.jar",
  }

  #file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/extractedphrasestagger.jar":
    #ensure  => file,
    #require => File[$ds_ephrase_required_paths],
    #mode    => '0644',
    #owner   => $ds_ephrase::_ds_ephrase_user,
    #group   => $ds_ephrase::_ds_ephrase_group,
    #source  =>  "puppet:///modules/ds_ephrase/extractedphrasestagger.jar",
  #}

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/extractedphrasesupdater.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/ExtractedPhrasesUpdaterPipeline-1.0-DS.jar",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/NLPUtils.jar":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/NLPUtils-1.0-DS.jar",
  }

  #file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/opennlp-maxent-3.0.3.jar":
    #ensure  => file,
    #require => File[$ds_ephrase_required_paths],
    #mode    => '0644',
    #owner   => $ds_ephrase::_ds_ephrase_user,
    #group   => $ds_ephrase::_ds_ephrase_group,
    #source  =>  "puppet:///modules/ds_ephrase/opennlp-maxent-3.0.3.jar",
  #}

  #file { "${ds_ephrase::ds_ephrase_deploy_dir}/pllib/opennlp-tools-1.5.3.jar":
    #ensure  => file,
    #require => File[$ds_ephrase_required_paths],
    #mode    => '0644',
    #owner   => $ds_ephrase::_ds_ephrase_user,
    #group   => $ds_ephrase::_ds_ephrase_group,
    #source  =>  "puppet:///modules/ds_ephrase/opennlp-tools-1.5.3.jar",
  #}

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/spring-config.xml",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/castormapping.xml",
  }

  file { "${ds_ephrase::ds_ephrase_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_ephrase_required_paths],
    mode    => '0644',
    owner   => $ds_ephrase::_ds_ephrase_user,
    group   => $ds_ephrase::_ds_ephrase_group,
    source  =>  "puppet:///modules/ds_ephrase/castor.properties",
  }

  file { "/etc/init.d/frextractedphrases":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_ephrase/frextractedphrases.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frextractedphrases.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frextractedphrases'],
        owner   => root,
        group   => root,
        content => template('ds_ephrase/frextractedphrases.service.erb'),
      }
  }

}
