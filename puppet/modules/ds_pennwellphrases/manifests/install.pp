class
  ds_pennwellphrases::install
inherits
  ds_pennwellphrases
{

  $ds_pennwellphrases_required_paths = [
    $ds_pennwellphrases::ds_pennwellphrases_deploy_dir,
    "$ds_pennwellphrases::ds_pennwellphrases_deploy_dir/bin",
    "$ds_pennwellphrases::ds_pennwellphrases_deploy_dir/conf",
    "$ds_pennwellphrases::ds_pennwellphrases_deploy_dir/lib",
    "$ds_pennwellphrases::ds_pennwellphrases_deploy_dir/pllib",
    "$ds_pennwellphrases::ds_pennwellphrases_deploy_dir/plconf",
    $ds_pennwellphrases::ds_pennwellphrases_frlogdir,
    "${ds_pennwellphrases::ds_pennwellphrases_frlogdir}/logs",
    "${ds_pennwellphrases::ds_pennwellphrases_frlogdir}/dpflogs",
  ]

  file { $ds_pennwellphrases_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group  => $ds_pennwellphrases::_ds_pennwellphrases_group,
    before => File['fetch pennwell dbf library tar file'],
  }

  file {'fetch pennwell dbf library tar file':
     ensure  => 'file',
     require => File[$ds_pennwellphrases_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
     group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
     source  => "puppet:///modules/ds_pennwellphrases/dpf-lib.tar"
  }

   exec { 'extract-pennwell-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_pennwellphrases_deploy_dir}/lib",
     require => File[$ds_pennwellphrases_required_paths],
     user    => $ds_pennwellphrases::_ds_pennwellphrases_user,
     group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
     path    => '/bin',
  }

  File['fetch pennwell dbf library tar file'] -> Exec['extract-pennwell-dpf-lib']

  file {'fetch pennwell library tar file':
     ensure  => 'file',
     require => File[$ds_pennwellphrases_required_paths],
     path    => "${fr_global::params::dir_root}/download/pennwell-jars.tar",
     owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
     group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
     source  => "puppet:///modules/ds_pennwellphrases/pennwell-jars.tar"
  }

   exec { 'extract-pennwell-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/pennwell-jars.tar --directory ${ds_pennwellphrases_deploy_dir}/pllib",
     require => File[$ds_pennwellphrases_required_paths],
     user    => $ds_pennwellphrases::_ds_pennwellphrases_user,
     group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
     path    => '/bin',
  }

  File['fetch pennwell library tar file'] -> Exec['extract-pennwell-lib']

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/log4j2.xml.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/config.properties.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0755',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/startup.sh.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0755',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/shutdown.sh.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/plconf/PennwellPhrases_Pipeline.xml":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/PennwellPhrases_Pipeline.xml.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/plconf/PennwellPhrases_Historical_Pipeline.xml":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    content => template('ds_pennwellphrases/PennwellPhrases_Historical_Pipeline.xml.erb'),
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/dpf-1.2.jar",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/pllib/basicutils-1.0.jar":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/basicutils-1.0.jar",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/pllib/pennwellphrases-1.0.jar":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/pennwellphrases-1.0.jar",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/pllib/nlputils-2.0.jar":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/nlputils-2.0.jar",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/spring-config.xml",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/castormapping.xml",
  }

  file { "${ds_pennwellphrases::ds_pennwellphrases_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_pennwellphrases_required_paths],
    mode    => '0644',
    owner   => $ds_pennwellphrases::_ds_pennwellphrases_user,
    group   => $ds_pennwellphrases::_ds_pennwellphrases_group,
    source  =>  "puppet:///modules/ds_pennwellphrases/castor.properties",
  }

  file { "/etc/init.d/frpennwellphrases":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_pennwellphrases/frpennwellphrases.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frpennwellphrases.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frpennwellphrases'],
        owner   => root,
        group   => root,
        content => template('ds_pennwellphrases/frpennwellphrases.service.erb'),
      }
  }

}
