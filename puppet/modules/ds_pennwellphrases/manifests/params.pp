class
  ds_pennwellphrases::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_pennwellphrases_jdk_version = "1.8.0_66"
  $ds_pennwellphrases_user = "serviceprod"
  $ds_pennwellphrases_group = "engops"
}
