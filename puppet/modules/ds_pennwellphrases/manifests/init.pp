class
  ds_pennwellphrases
  (
    $ds_pennwellphrases_jdk_version = undef,
    $ds_pennwellphrases_user = undef,
    $ds_pennwellphrases_group = undef,
    $ds_pennwellphrases_meta_url = undef,
    $ds_pennwellphrases_mi32 = undef,
    $ds_pennwellphrases_mi32_user = undef,
    $ds_pennwellphrases_mi32_passwd = undef,
    $ds_pennwellphrases_app_name = "frpennwellphrases",
  )
{
  include ds_pennwellphrases::params

  notify { 'ds_pennwellphrases deploy dir':
    message => $_deploy_dir
  }

  if $ds_pennwellphrases::ds_pennwellphrases_jdk_version == undef {
    $_ds_pennwellphrases_jdk_version = $ds_pennwellphrases::params::ds_pennwellphrases_jdk_version
  } else {
    $_ds_pennwellphrases_jdk_version = $ds_pennwellphrases::ds_pennwellphrases_jdk_version
  }

  if $ds_pennwellphrases::ds_pennwellphrases_user == undef {
    $_ds_pennwellphrases_user = $ds_pennwellphrases::params::ds_pennwellphrases_user
  } else {
    $_ds_pennwellphrases_user = $ds_pennwellphrases::ds_pennwellphrases_user
  }

  if $ds_pennwellphrases::ds_pennwellphrases_group == undef {
    $_ds_pennwellphrases_group = $ds_pennwellphrases::params::ds_pennwellphrases_group
  } else {
    $_ds_pennwellphrases_group = $ds_pennwellphrases::ds_pennwellphrases_group
  }

  if $ds_pennwellphrases::ds_pennwellphrases_meta_url == undef {
    $_ds_pennwellphrases_meta_url = $ds_pennwellphrases::params::ds_pennwellphrases_meta_url
  } else {
    $_ds_pennwellphrases_meta_url = $ds_pennwellphrases::ds_pennwellphrases_meta_url
  }

  if $ds_pennwellphrases::ds_pennwellphrases_mi32 == undef {
    $_ds_pennwellphrases_mi32 = $ds_pennwellphrases::params::ds_pennwellphrases_mi32
  } else {
    $_ds_pennwellphrases_mi32 = $ds_pennwellphrases::ds_pennwellphrases_mi32
  }

  if $ds_pennwellphrases::ds_pennwellphrases_mi32_user == undef {
    $_ds_pennwellphrases_mi32_user = $ds_pennwellphrases::params::ds_pennwellphrases_mi32_user
  } else {
    $_ds_pennwellphrases_mi32_user = $ds_pennwellphrases::ds_pennwellphrases_mi32_user
  }

  if $ds_pennwellphrases::ds_pennwellphrases_mi32_passwd == undef {
    $_ds_pennwellphrases_mi32_passwd = $ds_pennwellphrases::params::ds_pennwellphrases_mi32_passwd
  } else {
    $_ds_pennwellphrases_mi32_passwd = $ds_pennwellphrases::ds_pennwellphrases_mi32_passwd
  }

  $ds_pennwellphrases_deploy_dir = "/fr/deploy/${ds_pennwellphrases_app_name}"
  $ds_pennwellphrases_frlogdir = "/frlogdir/${ds_pennwellphrases_app_name}"
  $ds_pennwellphrases_jdk_dir = "/fr/third-party/${_ds_pennwellphrases_jdk_version}"
  $ds_pennwellphrases_pl_lib = "${ds_pennwellphrases_deploy_dir}/pllib"
  $ds_pennwellphrases_pl_conf = "${ds_pennwellphrases_deploy_dir}/plconf"

  class{'ds_pennwellphrases::install':
    require => Class['fr_global::params'],
  }
}
