class
  frsso::install
inherits
  frsso
{

  $frsso_required_paths = [
    $frsso::frsso_deploy_dir,
    $frsso::frsso_services_dir,
    "${frsso::frsso_services_dir}/bin",
    "${frsso::frsso_services_dir}/logs",
    "${frsso::frsso_services_dir}/conf",
    "${frsso::frsso_services_dir}/conf/Catalina",
    "${frsso::frsso_services_dir}/conf/Catalina/localhost",
    "${frsso::frsso_services_dir}/lib",
    "${frsso::frsso_services_dir}/temp",
    "${frsso::frsso_services_dir}/work",
    "${frsso::frsso_services_dir}/appconf",
    $frsso::frsso_frlogdir,
    "${frsso::frsso_frlogdir}/logs",
    "${frsso::frsso_frlogdir}/logs/accesslogs",
    "${frsso::frsso_frlogdir}/cpflogs",
  ]

  file { $frsso_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $frsso::_frsso_user,
    group  => $frsso::_frsso_group,
    before => Notify['finished-frsso-war-fetch']
  }

  notify { 'start-frsso-war-fetch':
    name    =>  'STARTING I2AWebApp firstrain_sso.war Fetch',
    message =>  "${frsso::frsso_deploy_dir}/firstrain_sso.war"
  }

  notify { 'finished-frsso-war-fetch':
    name    =>  'FINISHED I2AWebApp firstrain_sso.war Fetch',
    message =>  "${frsso::frsso_deploy_dir}/firstrain_sso.war}"
  }

  file { 'fetch firstrain_sso War file':
    ensure  =>  'file',
    require =>  File[$frsso_required_paths],
    path    =>  "${frsso::frsso_deploy_dir}/firstrain_sso.war",
    owner   =>  $frsso::_frsso_user,
    group   =>  $frsso::_frsso_group,
    source  =>  "puppet:///modules/frsso/FirstRainSSO-Trunk-1.0.war",
    before  =>  Notify['finished-frsso-war-fetch']
  }

  exec { 'copy-tomcat-bin-frsso-files':
    command => "cp ${frsso::_frsso_tomcat_home}/bin/*.sh ${frsso::frsso_bin}",
    require => [File[$frsso_required_paths],Class['tomcat::install']],
    user    => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${frsso::frsso_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-i2-afiles':
    command => "cp ${frsso::_frsso_tomcat_home}/bin/*.jar ${frsso::frsso_bin}",
    require => [File[$frsso_required_paths],Class['tomcat::install']],
    user    => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${frsso::frsso_bin}/bootstrap.jar",
  }

  file { "${frsso::frsso_conf}/Catalina/localhost/sso.xml":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/sso.xml.erb'),
  }

  file { "${frsso::frsso_conf}/server.xml":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/server.xml.erb'),
  }

  file { "${frsso::frsso_conf}/catalina.properties":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/catalina.properties.erb'),
  }

  file { "${frsso::frsso_conf}/log4j.properties":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/log4j.properties.erb'),
  }

  file { "${frsso::frsso_services_dir}/appconf/application.properties":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/application.properties.erb'),
  }

  file { "${frsso::frsso_conf}/logging.properties":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/logging.properties.erb'),
  }

  file { "${frsso::frsso_conf}/catalina.policy":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/catalina.policy.erb'),
  }

  file { "${frsso::frsso_conf}/context.xml":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/context.xml.erb'),
  }

  file { "${frsso::frsso_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/tomcat-users.xml.erb'),
  }

  file { "${frsso::frsso_conf}/web.xml":
    ensure  => file,
    require => File[$frsso_required_paths],
    mode    => '0644',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/web.xml.erb'),
  }

  file { "${frsso::frsso_bin}/setenv.sh":
    ensure  => file,
    require => [File[$frsso_required_paths],Exec['copy-tomcat-bin-frsso-files']],
    mode    => '0755',
    owner   => $frsso::_frsso_user,
    group   => $frsso::_frsso_group,
    content => template('frsso/setenv.sh.erb'),
  }
  
  file { 'fetch arris-metadata.xml file':
    ensure  =>  'file',
    require =>  File[$frsso_required_paths],
    path    =>  "${frsso::frsso_services_dir}/appconf/arris-metadata.xml",
    owner   =>  $frsso::_frsso_user,
    group   =>  $frsso::_frsso_group,
    source  =>  "puppet:///modules/frsso/arris-metadata.xml",
  }

  if ( $::ipaddress =~ /^10.10.10./ ) {
      file { 'fetch prod authConfig.json file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/authConfig.json",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/authConfig.json.prod",
      }

      file { 'fetch hilton-metadata.xml file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/hilton-metadata.xml",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/hilton-metadata.xml.prod",
      }

      file { 'fetch okta-metadata.xml file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/okta-metadata.xml",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/okta-metadata.xml.prod",
      }

      notify { 'Production specific files':
        message =>  "uploaded production files : $::ipaddress"
      }
  }
  else {
      file { 'fetch Stage authConfig.json file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/authConfig.json",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/authConfig.json.stage",
      }

      file { 'fetch hilton-metadata.xml file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/hilton-metadata.xml",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/hilton-metadata.xml.stage",
      }

      file { 'fetch okta-metadata.xml file':
        ensure  =>  'file',
        require =>  File[$frsso_required_paths],
        path    =>  "${frsso::frsso_services_dir}/appconf/okta-metadata.xml",
        owner   =>  $frsso::_frsso_user,
        group   =>  $frsso::_frsso_group,
        mode    => '0644',
        source  =>  "puppet:///modules/frsso/okta-metadata.xml.stage",
      }

      notify { 'Stage specific files':
        message =>  "uploaded stage files : $::ipaddress"
      }
  }

  file { 'fetch ibm-metadata.xml file':
    ensure  =>  'file',
    require =>  File[$frsso_required_paths],
    path    =>  "${frsso::frsso_services_dir}/appconf/ibm-metadata.xml",
    owner   =>  $frsso::_frsso_user,
    group   =>  $frsso::_frsso_group,
    mode    => '0644',
    source  =>  "puppet:///modules/frsso/ibm-metadata.xml",
  }

  file { 'fetch firstrain.jks file':
    ensure  =>  'file',
    require =>  File[$frsso_required_paths],
    path    =>  "${frsso::frsso_services_dir}/appconf/firstrain.jks",
    owner   =>  $frsso::_frsso_user,
    group   =>  $frsso::_frsso_group,
    mode    => '0644',
    source  =>  "puppet:///modules/frsso/firstrain.jks",
  }

  file { "/etc/init.d/frsso":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    replace => true,
    content => template('frsso/frsso.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frsso.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frsso'],
        owner   => root,
        group   => root,
        content => template('frsso/frsso.service.erb'),
      }
  }
}
