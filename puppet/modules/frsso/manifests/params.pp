class
  frsso::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $frsso_shutdown_port = 8005
  $frsso_http_port = 8080
  $frsso_https_port = 8443
  $frsso_ajp_port = 8009
  $frsso_jdk_version = "1.8.0_66"
  $frsso_user = "serviceprod"
  $frsso_group = "opseng"
  $frsso_proxy_url = "https://changeThis.firstrain.com/sso"
  $frsso_orion_url = "https://changeThis.firstrain.com"
  $frsso_mobile_api_url = "http://changeThis:38218/frbusinessdataapi"
  $frsso_user_activity_url = "http://local.fruseractivity.firstrain.com:38219/fruseractivity"

}
