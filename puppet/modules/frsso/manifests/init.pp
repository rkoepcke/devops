class
  frsso
  (
    $frsso_tomcat_home = undef,
    $frsso_http_port = undef,
    $frsso_https_port = undef,
    $frsso_shutdown_port = undef,
    $frsso_ajp_port = undef,
    $frsso_jdk_version = undef,
    $frsso_min_mem_size = undef,
    $frsso_max_mem_size = undef,
    $frsso_user = undef,
    $frsso_group = undef,
    $frsso_proxy_url = undef,
    $frsso_orion_url = undef,
    $frsso_mobile_api_url = undef,
    $frsso_user_activity_url = undef,
    $frsso_app_name = "frsso",
  )
{
  include frsso::params

  notify { 'frsso deploy dir':
    message => $_deploy_dir
  }

  if $frsso::frsso_jdk_version == undef {
    $_frsso_jdk_version = $frsso::params::frsso_jdk_version
  } else {
    $_frsso_jdk_version = $frsso::frsso_jdk_version
  }

  if $frsso::frsso_shutdown_port == undef {
    $_frsso_shutdown_port = $frsso::params::frsso_shutdown_port
  } else {
    $_frsso_shutdown_port = $frsso::frsso_shutdown_port
  }

  if $frsso::frsso_http_port == undef {
    $_frsso_http_port = $frsso::params::frsso_http_port
  } else {
    $_frsso_http_port = $frsso::frsso_http_port
  }

  if $frsso::frsso_https_port == undef {
    $_frsso_https_port = $frsso::params::frsso_https_port
  } else {
    $_frsso_https_port = $frsso::frsso_https_port
  }

  if $frsso::frsso_ajp_port == undef {
    $_frsso_ajp_port = $frsso::params::frsso_ajp_port
  } else {
    $_frsso_ajp_port = $frsso::frsso_ajp_port
  }

  if $frsso::frsso_tomcat_home == undef {
    $_frsso_tomcat_home = $frsso::params::frsso_tomcat_home
  } else {
    $_frsso_tomcat_home = $frsso::frsso_tomcat_home
  }

  if $frsso::frsso_user == undef {
    $_frsso_user = $frsso::params::frsso_user
  } else {
    $_frsso_user = $frsso::frsso_user
  }

  if $frsso::frsso_group == undef {
    $_frsso_group = $frsso::params::frsso_group
  } else {
    $_frsso_group = $frsso::frsso_group
  }

  if $frsso::frsso_min_mem_size == undef {
    $_frsso_min_mem_size = $frsso::params::frsso_min_mem_size
  } else {
    $_frsso_min_mem_size = $frsso::frsso_min_mem_size
  }

  if $frsso::frsso_max_mem_size == undef {
    $_frsso_max_mem_size = $frsso::params::frsso_max_mem_size
  } else {
    $_frsso_max_mem_size = $frsso::frsso_max_mem_size
  }

  if $frsso::frsso_proxy_url == undef {
    $_frsso_proxy_url = $frsso::params::frsso_proxy_url
  } else {
    $_frsso_proxy_url = $frsso::frsso_proxy_url
  }

  if $frsso::frsso_orion_url == undef {
    $_frsso_orion_url = $frsso::params::frsso_orion_url
  } else {
    $_frsso_orion_url = $frsso::frsso_orion_url
  }

  if $frsso::frsso_mobile_api_url == undef {
    $_frsso_mobile_api_url = $frsso::params::frsso_mobile_api_url
  } else {
    $_frsso_mobile_api_url = $frsso::frsso_mobile_api_url
  }

  if $frsso::frsso_user_activity_url == undef {
    $_frsso_user_activity_url = $frsso::params::frsso_user_activity_url
  } else {
    $_frsso_user_activity_url = $frsso::frsso_user_activity_url
  }

  $frsso_deploy_dir = "/fr/deploy/${frsso_app_name}"
  $frsso_services_dir = "/fr/services/${frsso_app_name}"
  $frsso_frlogdir = "/frlogdir/${frsso_app_name}"
  $frsso_conf = "/fr/services/${frsso_app_name}/conf"
  $frsso_bin = "/fr/services/${frsso_app_name}/bin"
  $frsso_jdk_dir = "/fr/third-party/${_frsso_jdk_version}"
  $frsso_cache_rootdir = "/frdata"

  class{'frsso::install':
    require => Class['fr_global::params'],
  }

}
