class
  i2a_webapp::install
inherits
  i2a_webapp
{

  $i2a_webapp_required_paths = [
    $i2a_webapp::i2a_webapp_deploy_dir,
    $i2a_webapp::i2a_webapp_services_dir,
    "${i2a_webapp::i2a_webapp_services_dir}/bin",
    "${i2a_webapp::i2a_webapp_services_dir}/logs",
    "${i2a_webapp::i2a_webapp_services_dir}/conf",
    "${i2a_webapp::i2a_webapp_services_dir}/conf/Catalina",
    "${i2a_webapp::i2a_webapp_services_dir}/conf/Catalina/localhost",
    "${i2a_webapp::i2a_webapp_services_dir}/lib",
    "${i2a_webapp::i2a_webapp_services_dir}/temp",
    "${i2a_webapp::i2a_webapp_services_dir}/work",
    $i2a_webapp::i2a_webapp_frlogdir,
    "${i2a_webapp::i2a_webapp_frlogdir}/logs",
    "${i2a_webapp::i2a_webapp_frlogdir}/cpflogs",
    "${i2a_webapp::i2a_webapp_cache_loc}",
  ]

  file { $i2a_webapp_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $i2a_webapp::_i2a_webapp_user,
    group  => $i2a_webapp::_i2a_webapp_group,
    before => Notify['finished-i2a_webapp-war-fetch']
  }

  notify { 'start-i2a_webapp-war-fetch':
    name    =>  'STARTING I2AWebApp I2ARecordManager.war Fetch',
    message =>  "${i2a_webapp::i2a_webapp_deploy_dir}/I2ARecordManager.war"
  }

  notify { 'finished-i2a_webapp-war-fetch':
    name    =>  'FINISHED I2AWebApp I2ARecordManager.war Fetch',
    message =>  "${i2a_webapp::i2a_webapp_deploy_dir}/I2ARecordManager.war}"
  }

  file { 'fetch I2ARecordManager War file':
    ensure  =>  'file',
    require =>  File[$i2a_webapp_required_paths],
    path    =>  "${i2a_webapp::i2a_webapp_deploy_dir}/I2ARecordManager.war",
    owner   =>  $i2a_webapp::_i2a_webapp_user,
    group   =>  $i2a_webapp::_i2a_webapp_group,
    source  =>  "puppet:///modules/i2a_webapp/I2ARecordManager.war",
    before  =>  Notify['finished-i2a_webapp-war-fetch']
  }

  exec { 'delete-recordmgr-dir':
    command => "rm -rf ${i2a_webapp::i2a_webapp_deploy_dir}/I2ARecordManager",
    require =>  File[$i2a_webapp_required_paths],
    path => '/usr/bin:/bin',
    user => $i2a_webapp::_i2a_webapp_user,
    onlyif => "test -d ${i2a_webapp::i2a_webapp_deploy_dir}/I2ARecordManager"
  }
    
  exec { 'copy-tomcat-bin-i2a-files':
    command => "cp ${i2a_webapp::_i2a_webapp_tomcat_home}/bin/*.sh ${i2a_webapp::i2a_webapp_bin}",
    require => [File[$i2a_webapp_required_paths],Class['tomcat::install']],
    user    => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${i2a_webapp::i2a_webapp_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-i2-afiles':
    command => "cp ${i2a_webapp::_i2a_webapp_tomcat_home}/bin/*.jar ${i2a_webapp::i2a_webapp_bin}",
    require => [File[$i2a_webapp_required_paths],Class['tomcat::install']],
    user    => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${i2a_webapp::i2a_webapp_bin}/bootstrap.jar",
  }

  notify {'Directory Check':
     name => "config directory is $i2a_webapp_conf"
  }

  file { "${i2a_webapp::i2a_webapp_conf}/server.xml":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/server.xml.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/log4j2.xml":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/log4j2.xml.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/manager.properties":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/manager.properties.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/logging.properties":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/logging.properties.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/catalina.policy":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/catalina.policy.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/context.xml":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/context.xml.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/tomcat-users.xml.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/web.xml":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/web.xml.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_conf}/entityInfoCache.properties":
    ensure  => file,
    require => File[$i2a_webapp_required_paths],
    mode    => '0644',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/entityInfoCache.properties.erb'),
  }

  file { "${i2a_webapp::i2a_webapp_bin}/setenv.sh":
    ensure  => file,
    require => [File[$i2a_webapp_required_paths],Exec['copy-tomcat-bin-i2a-files']],
    mode    => '0755',
    owner   => $i2a_webapp::_i2a_webapp_user,
    group   => $i2a_webapp::_i2a_webapp_group,
    content => template('i2a_webapp/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/frrecordmgr":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    replace => false,
    content => template('i2a_webapp/frrecordmgr.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frrecordmgr.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frrecordmgr'],
        owner   => root,
        group   => root,
        content => template('i2a_webapp/frrecordmgr.service.erb'),
      }
  }
}
