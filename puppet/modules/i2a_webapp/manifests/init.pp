class
  i2a_webapp
  (
    $i2a_webapp_tomcat_home = undef,
    $i2a_webapp_http_port = undef,
    $i2a_webapp_https_port = undef,
    $i2a_webapp_shutdown_port = undef,
    $i2a_webapp_ajp_port = undef,
    $i2a_webapp_jdk_version = undef,
    $i2a_webapp_user = undef,
    $i2a_webapp_group = undef,
    $i2a_webapp_solr_entity = undef,
    $i2a_webapp_solr_url = undef,
    $i2a_webapp_cache_loc = undef,
    $i2a_webapp_dev_mode = undef,
    $i2a_webapp_app_name = "frrecordmgr",
  )
{
  include i2a_webapp::params

  notify { 'i2a_webapp deploy dir':
    message => $_deploy_dir
  }

  if $i2a_webapp::i2a_webapp_jdk_version == undef {
    $_i2a_webapp_jdk_version = $i2a_webapp::params::i2a_webapp_jdk_version
  } else {
    $_i2a_webapp_jdk_version = $i2a_webapp::i2a_webapp_jdk_version
  }

  if $i2a_webapp::i2a_webapp_shutdown_port == undef {
    $_i2a_webapp_shutdown_port = $i2a_webapp::params::i2a_webapp_shutdown_port
  } else {
    $_i2a_webapp_shutdown_port = $i2a_webapp::i2a_webapp_shutdown_port
  }

  if $i2a_webapp::i2a_webapp_http_port == undef {
    $_i2a_webapp_http_port = $i2a_webapp::params::i2a_webapp_http_port
  } else {
    $_i2a_webapp_http_port = $i2a_webapp::i2a_webapp_http_port
  }

  if $i2a_webapp::i2a_webapp_https_port == undef {
    $_i2a_webapp_https_port = $i2a_webapp::params::i2a_webapp_https_port
  } else {
    $_i2a_webapp_https_port = $i2a_webapp::i2a_webapp_https_port
  }

  if $i2a_webapp::i2a_webapp_ajp_port == undef {
    $_i2a_webapp_ajp_port = $i2a_webapp::params::i2a_webapp_ajp_port
  } else {
    $_i2a_webapp_ajp_port = $i2a_webapp::i2a_webapp_ajp_port
  }

  if $i2a_webapp::i2a_webapp_tomcat_home == undef {
    $_i2a_webapp_tomcat_home = $i2a_webapp::params::i2a_webapp_tomcat_home
  } else {
    $_i2a_webapp_tomcat_home = $i2a_webapp::i2a_webapp_tomcat_home
  }

  if $i2a_webapp::i2a_webapp_user == undef {
    $_i2a_webapp_user = $i2a_webapp::params::i2a_webapp_user
  } else {
    $_i2a_webapp_user = $i2a_webapp::i2a_webapp_user
  }

  if $i2a_webapp::i2a_webapp_group == undef {
    $_i2a_webapp_group = $i2a_webapp::params::i2a_webapp_group
  } else {
    $_i2a_webapp_group = $i2a_webapp::i2a_webapp_group
  }

  if $i2a_webapp::i2a_webapp_cache_loc == undef {
    $_i2a_webapp_cache_loc = $i2a_webapp::params::i2a_webapp_cache_loc
  } else {
    $_i2a_webapp_cache_loc = $i2a_webapp::i2a_webapp_cache_loc
  }

  if $i2a_webapp::i2a_webapp_solr_entity == undef {
    $_i2a_webapp_solr_entity = $i2a_webapp::params::i2a_webapp_solr_entity
  } else {
    $_i2a_webapp_solr_entity = $i2a_webapp::i2a_webapp_solr_entity
  }

  if $i2a_webapp::i2a_webapp_solr_url == undef {
    $_i2a_webapp_solr_url = $i2a_webapp::params::i2a_webapp_solr_url
  } else {
    $_i2a_webapp_solr_url = $i2a_webapp::i2a_webapp_solr_url
  }

  if $i2a_webapp::i2a_webapp_dev_mode == undef {
    $_i2a_webapp_dev_mode = $i2a_webapp::params::i2a_webapp_dev_mode
  } else {
    $_i2a_webapp_dev_mode = $i2a_webapp::i2a_webapp_dev_mode
  }

  $i2a_webapp_deploy_dir = "/fr/deploy/${i2a_webapp_app_name}"
  $i2a_webapp_services_dir = "/fr/services/${i2a_webapp_app_name}"
  $i2a_webapp_frlogdir = "/frlogdir/${i2a_webapp_app_name}"
  $i2a_webapp_conf = "/fr/services/${i2a_webapp_app_name}/conf"
  $i2a_webapp_bin = "/fr/services/${i2a_webapp_app_name}/bin"
  $i2a_webapp_jdk_dir = "/fr/third-party/${_i2a_webapp_jdk_version}"
  $i2a_webapp_cache_rootdir = "/frdata"

  class{'i2a_webapp::install':
    require => Class['fr_global::params'],
  }

}
