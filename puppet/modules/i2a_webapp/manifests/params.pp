class
  i2a_webapp::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $i2a_webapp_shutdown_port = 8005
  $i2a_webapp_http_port = 8080
  $i2a_webapp_https_port = 8443
  $i2a_webapp_ajp_port = 8009
  $i2a_webapp_jdk_version = "1.8.0_66"
  $i2a_webapp_tomcat_user = "serviceprod"
  $i2a_webapp_tomcat_group = "serviceprod"
  $i2a_webapp_entity_url = "solr.froovy.firstrain.com"
}
