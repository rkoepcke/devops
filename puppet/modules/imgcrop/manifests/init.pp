class
  imgcrop
  (
    $imgcrop_tomcat_home = undef,
    $imgcrop_http_port = undef,
    $imgcrop_https_port = undef,
    $imgcrop_shutdown_port = undef,
    $imgcrop_ajp_port = undef,
    $imgcrop_jdk_version = undef,
    $imgcrop_tomcat_user = undef,
    $imgcrop_tomcat_group = undef,
    $imgcrop_repository = undef,
    $imgcrop_display_url = undef,
    $imgcrop_app_name = "frimgcropservice",
  )
{
  include imgcrop::params

  notify { 'imgcrop deploy dir':
    message => $_deploy_dir
  }

  if $imgcrop::imgcrop_jdk_version == undef {
    $_imgcrop_jdk_version = $imgcrop::params::imgcrop_jdk_version
  } else {
    $_imgcrop_jdk_version = $imgcrop::imgcrop_jdk_version
  }

  if $imgcrop::imgcrop_shutdown_port == undef {
    $_imgcrop_shutdown_port = $imgcrop::params::imgcrop_shutdown_port
  } else {
    $_imgcrop_shutdown_port = $imgcrop::imgcrop_shutdown_port
  }

  if $imgcrop::imgcrop_http_port == undef {
    $_imgcrop_http_port = $imgcrop::params::imgcrop_http_port
  } else {
    $_imgcrop_http_port = $imgcrop::imgcrop_http_port
  }

  if $imgcrop::imgcrop_https_port == undef {
    $_imgcrop_https_port = $imgcrop::params::imgcrop_https_port
  } else {
    $_imgcrop_https_port = $imgcrop::imgcrop_https_port
  }

  if $imgcrop::imgcrop_ajp_port == undef {
    $_imgcrop_ajp_port = $imgcrop::params::imgcrop_ajp_port
  } else {
    $_imgcrop_ajp_port = $imgcrop::imgcrop_ajp_port
  }

  if $imgcrop::imgcrop_tomcat_home == undef {
    $_imgcrop_tomcat_home = $imgcrop::params::imgcrop_tomcat_home
  } else {
    $_imgcrop_tomcat_home = $imgcrop::imgcrop_tomcat_home
  }

  if $imgcrop::imgcrop_tomcat_user == undef {
    $_imgcrop_tomcat_user = $imgcrop::params::imgcrop_tomcat_user
  } else {
    $_imgcrop_tomcat_user = $imgcrop::imgcrop_tomcat_user
  }

  if $imgcrop::imgcrop_tomcat_group == undef {
    $_imgcrop_tomcat_group = $imgcrop::params::imgcrop_tomcat_group
  } else {
    $_imgcrop_tomcat_group = $imgcrop::imgcrop_tomcat_group
  }

  if $imgcrop::imgcrop_repository == undef {
    $_imgcrop_repository = $imgcrop::params::imgcrop_repository
  } else {
    $_imgcrop_repository = $imgcrop::imgcrop_repository
  }

  if $imgcrop::imgcrop_display_url == undef {
    $_imgcrop_display_url = $imgcrop::params::imgcrop_display_url
  } else {
    $_imgcrop_display_url = $imgcrop::imgcrop_display_url
  }

  $imgcrop_deploy_dir = "/fr/deploy/${imgcrop_app_name}"
  $imgcrop_services_dir = "/fr/services/${imgcrop_app_name}"
  $imgcrop_frlogdir = "/frlogdir/${imgcrop_app_name}"
  $imgcrop_conf = "/fr/services/${imgcrop_app_name}/conf"
  $imgcrop_bin = "/fr/services/${imgcrop_app_name}/bin"
  $imgcrop_jdk_dir = "/fr/third-party/${_imgcrop_jdk_version}"

  class{'imgcrop::install':
    require => Class['fr_global::params'],
  }

}
