class
  imgcrop::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $imgcrop_shutdown_port = 8005
  $imgcrop_http_port = 8080
  $imgcrop_https_port = 8443
  $imgcrop_ajp_port = 8009
  $imgcrop_jdk_version = "1.8.0_45"
  $imgcrop_tomcat_user = "serviceprod"
  $imgcrop_tomcat_group = "serviceprod"
  $imgcrop_repository = "/frdata/imgerepository"
  $imgcrop_display_url = "https://croppingservice.firstrain.com/images"
}
