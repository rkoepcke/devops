class
  imgcrop::install
inherits
  imgcrop
{

  $imgcrop_required_paths = [
    $imgcrop::imgcrop_deploy_dir,
    $imgcrop::imgcrop_services_dir,
    "${imgcrop::imgcrop_services_dir}/bin",
    "${imgcrop::imgcrop_services_dir}/logs",
    "${imgcrop::imgcrop_services_dir}/conf",
    "${imgcrop::imgcrop_services_dir}/conf/Catalina",
    "${imgcrop::imgcrop_services_dir}/conf/Catalina/localhost",
    "${imgcrop::imgcrop_services_dir}/lib",
    "${imgcrop::imgcrop_services_dir}/temp",
    "${imgcrop::imgcrop_services_dir}/work",
    "${imgcrop::imgcrop_deploy_dir}/WEB-INF",
    "${imgcrop::imgcrop_deploy_dir}/WEB-INF/classes",
    "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib",
    $imgcrop::imgcrop_frlogdir,
    "${imgcrop::imgcrop_frlogdir}/logs",
    "${imgcrop::imgcrop_frlogdir}/cpflogs",
  ]

  file { $imgcrop_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $imgcrop::imgcrop_tomcat_user,
    group  => $imgcrop::imgcrop_tomcat_group,
    before  =>  Notify['start thirdpartyJars.tar fetch']
  }

  notify { 'start thirdpartyJars.tar fetch':
    name    =>  "Start ImgCrop thirdpartyJars.tar fetch",
    message =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/thirdpartyJars.tar"
  }

  file { 'fetch thirdpartyJars.tar file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/thirdpartyJars.tar",
    owner   =>  $imgcrop::_imgcrop_tomcat_user,
    group   =>  $imgcrop::_imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/thirdpartyJars.tar"
  }

  exec { 'unpack-thirdparty-jar-files':
    command => "tar -xf ${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/thirdpartyJars.tar --directory ${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib",
    require => [File[$imgcrop_required_paths],File['fetch thirdpartyJars.tar file']],
    #user    => $imgcrop::imgcrop_tomcat_user,
    #group   => $imgcrop::imgcrop_tomcat_group,
    path    => '/bin',
    user    => $imgcrop::_imgcrop_tomcat_user,
    unless  => "/usr/bin/test -f ${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/javax.inject-1.jar"
  }

  exec { 'remove-thirdparty-tar-file':
    command => "rm -f ${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/thirdpartyJars.tar",
    path    => '/bin' 
  }

  File['fetch thirdpartyJars.tar file'] -> Exec['unpack-thirdparty-jar-files'] -> Exec['remove-thirdparty-tar-file']

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { 'fetch ulocalAdditions-os7.tar.gz file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${fr_global::params::dir_download}/ulocalAdditions-os7.tar.gz",
	owner   =>  $imgcrop::_imgcrop_tomcat_user,
	group   =>  $imgcrop::_imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/ulocalAdditions-os7.tar.gz"
      }

      exec { 'unpack-ulocaladditions-os7-files':
	command => "tar -xzf ${fr_global::params::dir_download}/ulocalAdditions-os7.tar.gz --directory /usr/local",
	require => [File[$imgcrop_required_paths],File['fetch ulocalAdditions-os7.tar.gz file']],
	path    => '/bin',
	user    => "root",
	unless  => "/usr/bin/test -f /usr/local/bin/ffmpeg"
      }

      exec { 'remove-ulocaladditions-os7-tar-file':
	command => "rm -f ${fr_global::params::dir_download}/WEB-INF/lib/ulocalAdditions-os7.tar.gz",
	path    => '/bin' 
      }

      File['fetch ulocalAdditions-os7.tar.gz file'] -> Exec['unpack-ulocaladditions-os7-files'] -> Exec['remove-ulocaladditions-os7-tar-file']

      file { 'fetch opencv-300-os7.jar file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/opencv-300.jar",
	owner   =>  $imgcrop::imgcrop_tomcat_user,
	group   =>  $imgcrop::imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/opencv-300-os7.jar"
      }

      file { 'fetch libopencv_java300-os7.so file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/libopencv_java300.so",
	owner   =>  $imgcrop::imgcrop_tomcat_user,
	group   =>  $imgcrop::imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/libopencv_java300-os7.so"
      }
  }
  else {
 
      file { 'fetch ulocalAdditions.tar.gz file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${fr_global::params::dir_download}/ulocalAdditions.tar.gz",
	owner   =>  $imgcrop::_imgcrop_tomcat_user,
	group   =>  $imgcrop::_imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/ulocalAdditions.tar.gz"
      }

      exec { 'unpack-ulocaladditions-files':
	command => "tar -xzf ${fr_global::params::dir_download}/ulocalAdditions.tar.gz --directory /usr/local",
	require => [File[$imgcrop_required_paths],File['fetch ulocalAdditions.tar.gz file']],
	path    => '/bin',
	user    => "root",
	unless  => "/usr/bin/test -f /usr/local/bin/ffmpeg"
      }

      exec { 'remove-ulocaladditions-tar-file':
	command => "rm -f ${fr_global::params::dir_download}/WEB-INF/lib/ulocalAdditions.tar.gz",
	path    => '/bin' 
      }

      File['fetch ulocalAdditions.tar.gz file'] -> Exec['unpack-ulocaladditions-files'] -> Exec['remove-ulocaladditions-tar-file']
   
      file { 'fetch opencv-300.jar file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/opencv-300.jar",
	owner   =>  $imgcrop::imgcrop_tomcat_user,
	group   =>  $imgcrop::imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/opencv-300.jar"
      }

      file { 'fetch libopencv_java300.so file':
	ensure  =>  'file',
	require =>  File[$imgcrop_required_paths],
	path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/libopencv_java300.so",
	owner   =>  $imgcrop::imgcrop_tomcat_user,
	group   =>  $imgcrop::imgcrop_tomcat_group,
	source  =>  "puppet:///modules/imgcrop/libopencv_java300.so"
      }
  }

  file { 'fetch frImageCroppingService.jar file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/frImageCroppingService.jar",
    owner   =>  $imgcrop::_imgcrop_tomcat_user,
    group   =>  $imgcrop::_imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/frImageCroppingService.jar"
  }

  file { 'fetch FRDomainModel.jar file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/lib/FRDomainModel.jar",
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/FRDomainModel.jar"
  }


  file { 'fetch web.xml file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/web.xml",
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/web.xml"
  }

  file { "${imgcrop::imgcrop_deploy_dir}/WEB-INF/applicationContext.xml":
    ensure  =>  'file',
    mode    => '0644',
    require =>  File[$imgcrop_required_paths],
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/applicationContext.xml.erb'),
  }

  file { "${imgcrop::imgcrop_deploy_dir}/WEB-INF/classes/log4j.properties":
    ensure  =>  'file',
    mode    => '0644',
    require =>  File[$imgcrop_required_paths],
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/log4j.properties.erb'),
  }

  file { 'fetch trained_classifierNM1.xml file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/classes/trained_classifierNM1.xml",
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/trained_classifierNM1.xml"
  }

  file { 'fetch trained_classifierNM2.xml file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/classes/trained_classifierNM2.xml",
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/trained_classifierNM2.xml"
  }

  file { 'fetch trained_classifier_erGrouping.xml file':
    ensure  =>  'file',
    require =>  File[$imgcrop_required_paths],
    path    =>  "${imgcrop::imgcrop_deploy_dir}/WEB-INF/classes/trained_classifier_erGrouping.xml",
    owner   =>  $imgcrop::imgcrop_tomcat_user,
    group   =>  $imgcrop::imgcrop_tomcat_group,
    source  =>  "puppet:///modules/imgcrop/trained_classifier_erGrouping.xml"
  }

  exec { 'copy-tomcat-bin-files':
    command => "cp ${imgcrop::_imgcrop_tomcat_home}/bin/*.sh ${imgcrop::imgcrop_bin}",
    require => [File[$imgcrop_required_paths],Class['tomcat::install']],
    user    => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${imgcrop::imgcrop_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files':
    command => "cp ${imgcrop::_imgcrop_tomcat_home}/bin/*.jar ${imgcrop::imgcrop_bin}",
    require => [File[$imgcrop_required_paths],Class['tomcat::install']],
    user    => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${imgcrop::imgcrop_bin}/bootstrap.jar",
  }

  # Remove this step once the code has been updated to remove the debug 
  # directory requirement.
  file { 'generate-debug-dir':
    ensure  => directory,
    require => File[$imgcrop_required_paths],
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    path    => "$imgcrop::imgcrop_services_dir/bin/debug",
    mode    => '0755',
  }
    
  file { "${imgcrop::imgcrop_conf}/server.xml":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/server.xml.erb'),
  }

  file { "${imgcrop::imgcrop_conf}/logging.properties":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/logging.properties.erb'),
  }

  file { "${imgcrop::imgcrop_conf}/catalina.policy":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/catalina.policy.erb'),
  }

  file { "${imgcrop::imgcrop_conf}/context.xml":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/context.xml.erb'),
  }

  file { "${imgcrop::imgcrop_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/tomcat-users.xml.erb'),
  }

  file { "${imgcrop::imgcrop_conf}/web.xml":
    ensure  => file,
    require => File[$imgcrop_required_paths],
    mode    => '0644',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/web.xml.erb'),
  }

  file { "${imgcrop::imgcrop_bin}/setenv.sh":
    ensure  => file,
    require => [File[$imgcrop_required_paths],Exec['copy-tomcat-bin-files']],
    mode    => '0755',
    owner   => $imgcrop::imgcrop_tomcat_user,
    group   => $imgcrop::imgcrop_tomcat_group,
    content => template('imgcrop/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/frimgcropservice":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    replace => false,
    content => template('imgcrop/frimgcropservice.erb'),
  }
}
