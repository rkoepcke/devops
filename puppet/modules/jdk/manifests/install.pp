class
  jdk::install
inherits
  jdk
{

  notify { "starting-fetch-${module_name}":
#   name    =>  "STARTING : fetch ${module_name",
    message =>  "${fr_global::params::dir_download}/${jdk::package}"
  }

  notify { "finished-fetch-${module_name}":
#   name    =>  "FINISHED : fetch ${module_name",
    message =>  "${fr_global::params::dir_download}/${jdk::package}"
  }

  file { "fetch ${module_name} binary archive":
    ensure  =>  file,
    replace =>  'no',
    require =>  File[$jdk::_deploy_dir],
    path    =>  "${fr_global::params::dir_download}/${jdk::package}",
    source  =>  "puppet:///modules/jdk/${jdk::package}",
    before  =>  [ Notify["finished-fetch-${module_name}"], Exec["extract-package-${module_name}"] ]
  }

    #TODO: checksum

    #TODO: use a off the shelf module
# file { 'jdk deploy dir':
# 	path	=> "${jdk::_deploy_dir}/jdk-${version}",
# 	ensure	=> "directory"
#   }

  $target = $jdk::_deploy_dir
  $source = "${fr_global::params::dir_download}/${jdk::package}"
  $command = "tar -xzf ${source} --directory ${target}"

  exec { "extract-package-${module_name}":
    command => "tar -xzf ${source} --directory ${target}",
    path    => "${fr_global::params::sys_path}",
    unless  => "/usr/bin/test -d ${jdk::jdk_dir_name}"
  }
  
  #TODO: validation
}
