class
  jdk
  (
    $deploy_dir     = undef,
    $version_major  = $jdk::params::version_major,
    $version_minor  = $jdk::params::version_minor,
    $version_patch  = $jdk::params::version_patch,
    $arch_pkg       = $jdk::params::arch_pkg,
  )
inherits
  jdk::params
{

  if $deploy_dir==undef {
    $_deploy_dir = $jdk::params::deploy_dir_default
  }else{
    $_deploy_dir = $deploy_dir
  }

  $version = "${version_major}${version_minor}${version_patch}"
  $package = "jdk-${version}-${jdk::params::arch_pkg}.tar.gz"
  $jdk_dir_name = "${_deploy_dir}/jdk1.${version_major}.0_${version_patch}"

  class{'jdk::install':
    require => Class['fr_global::params'],
  }

}
