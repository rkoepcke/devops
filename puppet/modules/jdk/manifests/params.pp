class
  jdk::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $version_major = '6'
  $version_minor = 'u'
  $version_patch = '30'

  $arch = $::architecture ? {
      /(?i-mx:amd64|64)/  =>  'x86_64',
      /(?i-mx:x64|64)/    =>  'x86_64',
      /(?i-mx:x32|32)/    =>  'i386'
  }

  $arch_pkg = $::operatingsystem ? {
    /(?i-mx:ubuntu|debian)/        => "linux-${arch}",
    /(?i-mx:centos|fedora|redhat)/ => "linux-${arch}",
    /(?i-mx:darwin)/ => $fr_global::params::unsupported
  }

}
