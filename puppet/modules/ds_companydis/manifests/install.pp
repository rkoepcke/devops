class
  ds_companydis::install
inherits
  ds_companydis
{

  $ds_companydis_required_paths = [
    $ds_companydis::ds_companydis_deploy_dir,
    "$ds_companydis::ds_companydis_deploy_dir/bin",
    "$ds_companydis::ds_companydis_deploy_dir/conf",
    "$ds_companydis::ds_companydis_deploy_dir/lib",
    "$ds_companydis::ds_companydis_deploy_dir/pllib",
    "$ds_companydis::ds_companydis_deploy_dir/plconf",
    $ds_companydis::ds_companydis_frlogdir,
    "${ds_companydis::ds_companydis_frlogdir}/logs",
    "${ds_companydis::ds_companydis_frlogdir}/dpflogs",
  ]

  file { $ds_companydis_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_companydis::_ds_companydis_user,
    group  => $ds_companydis::_ds_companydis_group,
    before => File['fetch ep dbf library tar file'],
  }

  file {'fetch ep dbf library tar file':
     ensure  => 'file',
     require => File[$ds_companydis_required_paths],
     path    => "${fr_global::params::dir_root}/download/dpf-lib.tar",
     owner   => $ds_companydis::_ds_companydis_user,
     group   => $ds_companydis::_ds_companydis_group,
     source  => "puppet:///modules/ds_companydis/dpf-lib.tar"
  }

   exec { 'extract-ep-dpf-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/dpf-lib.tar --directory ${ds_companydis_deploy_dir}/lib",
     require => File[$ds_companydis_required_paths],
     user    => $ds_companydis::_ds_companydis_user,
     group   => $ds_companydis::_ds_companydis_group,
     path    => '/bin',
  }

  File['fetch ep dbf library tar file'] -> Exec['extract-ep-dpf-lib']

  file {'fetch companyDiscovery library tar file':
     ensure  => 'file',
     require => File[$ds_companydis_required_paths],
     path    => "${fr_global::params::dir_root}/download/companydis-lib.tar",
     owner   => $ds_companydis::_ds_companydis_user,
     group   => $ds_companydis::_ds_companydis_group,
     source  => "puppet:///modules/ds_companydis/companydis-lib.tar"
  }

   exec { 'extract-companyDiscovery-lib':
     command => "tar -xvf ${fr_global::params::dir_root}/download/companydis-lib.tar --directory ${ds_companydis_deploy_dir}/pllib",
     require => File[$ds_companydis_required_paths],
     user    => $ds_companydis::_ds_companydis_user,
     group   => $ds_companydis::_ds_companydis_group,
     path    => '/bin',
  }

  File['fetch companyDiscovery library tar file'] -> Exec['extract-companyDiscovery-lib']

  file { "${ds_companydis::ds_companydis_deploy_dir}/conf/log4j2.xml":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    content => template('ds_companydis/log4j2.xml.erb'),
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/conf/config.properties":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    content => template('ds_companydis/config.properties.erb'),
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/bin/startup.sh":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0755',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    content => template('ds_companydis/startup.sh.erb'),
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/bin/shutdown.sh":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0755',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    content => template('ds_companydis/shutdown.sh.erb'),
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/plconf/LtWtCompanyTaggingDSPipeline.xml":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    content => template('ds_companydis/LtWtCompanyTaggingDSPipeline.xml.erb'),
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/dpf.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/dpf-1.2.jar",
  }


  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/basicutils-1.0.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/basicutils-1.0.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/DSEntityInfoCache.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/DSEntityInfoCache.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/CompanyAnalysisUtils.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/CompanyAnalysisUtils.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/LtWtCompanyTagging.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/LtWtCompanyTagging.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/MachineLearningUtils.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/MachineLearningUtils.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/lib/guava-19.0.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/guava-19.0.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/nlputils-1.0.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/nlputils-1.0.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/PatternBasedCompanyExtraction.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/PatternBasedCompanyExtraction.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/sqlutils-1.0.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/sqlutils-1.0.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/pllib/WordNetUtils.jar":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/WordNetUtils.jar",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/conf/spring-config.xml":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/spring-config.xml",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/conf/castormapping.xml":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/castormapping.xml",
  }

  file { "${ds_companydis::ds_companydis_deploy_dir}/conf/castor.properties":
    ensure  => file,
    require => File[$ds_companydis_required_paths],
    mode    => '0644',
    owner   => $ds_companydis::_ds_companydis_user,
    group   => $ds_companydis::_ds_companydis_group,
    source  =>  "puppet:///modules/ds_companydis/castor.properties",
  }

  file { "/etc/init.d/frcompanydiscovery":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => template('ds_companydis/frcompanydiscovery.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frcompanydiscovery.service":
        ensure  => file,
        mode    => '0644',
        require => File['/etc/init.d/frcompanydiscovery'],
        owner   => root,
        group   => root,
        content => template('ds_companydis/frcompanydiscovery.service.erb'),
      }
  }
}
