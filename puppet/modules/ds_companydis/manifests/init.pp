class
  ds_companydis
  (
    $ds_companydis_jdk_version = undef,
    $ds_companydis_user = undef,
    $ds_companydis_group = undef,
    $ds_companydis_storage_service = undef,
    $ds_companydis_dsdocs_solr_url = undef,
    $ds_companydis_entity_solr_url = undef,
    $ds_companydis_other_solr_url = undef,
    $ds_companydis_cachelocation = undef,
    $ds_companydis_mysql_tw = undef,
    $ds_companydis_ecp_solr_url = undef,
    $ds_companydis_api_url = undef,
    $ds_companydis_min_min = undef,
    $ds_companydis_max_mem = undef,
    $ds_companydis_jmxport = undef,
    $ds_companydis_start_site_doc_id = undef,
    $ds_companydis_dbuser = undef,
    $ds_companydis_dbpass = undef,
    $ds_companydis_dbhost = undef,
    $ds_companydis_app_name = "frcompanydiscovery",
  )
{
  include ds_companydis::params

  notify { 'ds_companydis deploy dir':
    message => $_deploy_dir
  }

  if $ds_companydis::ds_companydis_jdk_version == undef {
    $_ds_companydis_jdk_version = $ds_companydis::params::ds_companydis_jdk_version
  } else {
    $_ds_companydis_jdk_version = $ds_companydis::ds_companydis_jdk_version
  }

  if $ds_companydis::ds_companydis_user == undef {
    $_ds_companydis_user = $ds_companydis::params::ds_companydis_user
  } else {
    $_ds_companydis_user = $ds_companydis::ds_companydis_user
  }

  if $ds_companydis::ds_companydis_group == undef {
    $_ds_companydis_group = $ds_companydis::params::ds_companydis_group
  } else {
    $_ds_companydis_group = $ds_companydis::ds_companydis_group
  }

  if $ds_companydis::ds_companydis_storage_service == undef {
    $_ds_companydis_storage_service = $ds_companydis::params::ds_companydis_storage_service
  } else {
    $_ds_companydis_storage_service = $ds_companydis::ds_companydis_storage_service
  }

  if $ds_companydis::ds_companydis_dsdocs_solr_url == undef {
    $_ds_companydis_dsdocs_solr_url = $ds_companydis::params::ds_companydis_dsdocs_solr_url
  } else {
    $_ds_companydis_dsdocs_solr_url = $ds_companydis::ds_companydis_dsdocs_solr_url
  }

  if $ds_companydis::ds_companydis_entity_solr_url == undef {
    $_ds_companydis_entity_solr_url = $ds_companydis::params::ds_companydis_entity_solr_url
  } else {
    $_ds_companydis_entity_solr_url = $ds_companydis::ds_companydis_entity_solr_url
  }

  if $ds_companydis::ds_companydis_other_solr_url == undef {
    $_ds_companydis_other_solr_url = $ds_companydis::params::ds_companydis_other_solr_url
  } else {
    $_ds_companydis_other_solr_url = $ds_companydis::ds_companydis_other_solr_url
  }

  if $ds_companydis::ds_companydis_api_url == undef {
    $_ds_companydis_api_url = $ds_companydis::params::ds_companydis_api_url
  } else {
    $_ds_companydis_api_url = $ds_companydis::ds_companydis_api_url
  }

  if $ds_companydis::ds_companydis_ecp_solr_url == undef {
    $_ds_companydis_ecp_solr_url = $ds_companydis::params::ds_companydis_ecp_solr_url
  } else {
    $_ds_companydis_ecp_solr_url = $ds_companydis::ds_companydis_ecp_solr_url
  }

  if $ds_companydis::ds_companydis_mysql_tw == undef {
    $_ds_companydis_mysql_tw = $ds_companydis::params::ds_companydis_mysql_tw
  } else {
    $_ds_companydis_mysql_tw = $ds_companydis::ds_companydis_mysql_tw
  }

  if $ds_companydis::ds_companydis_jmxport == undef {
    $_ds_companydis_jmxport = $ds_companydis::params::ds_companydis_jmxport
  } else {
    $_ds_companydis_jmxport = $ds_companydis::ds_companydis_jmxport
  }

  if $ds_companydis::ds_companydis_cachelocation == undef {
    $_ds_companydis_cachelocation = $ds_companydis::params::ds_companydis_cachelocation
  } else {
    $_ds_companydis_cachelocation = $ds_companydis::ds_companydis_cachelocation
  }

  if $ds_companydis::ds_companydis_start_site_doc_id == undef {
    $_ds_companydis_start_site_doc_id = $ds_companydis::params::ds_companydis_start_site_doc_id
  } else {
    $_ds_companydis_start_site_doc_id = $ds_companydis::ds_companydis_start_site_doc_id
  }

  if $ds_companydis::ds_companydis_min_mem == undef {
    $_ds_companydis_min_mem = $ds_companydis::params::ds_companydis_min_mem
  } else {
    $_ds_companydis_min_mem = $ds_companydis::ds_companydis_min_mem
  }

  if $ds_companydis::ds_companydis_max_mem == undef {
    $_ds_companydis_max_mem = $ds_companydis::params::ds_companydis_max_mem
  } else {
    $_ds_companydis_max_mem = $ds_companydis::ds_companydis_max_mem
  }

  if $ds_companydis::ds_companydis_dbuser == undef {
    $_ds_companydis_dbuser = $ds_companydis::params::ds_companydis_dbuser
  } else {
    $_ds_companydis_dbuser = $ds_companydis::ds_companydis_dbuser
  }

  if $ds_companydis::ds_companydis_dbpass == undef {
    $_ds_companydis_dbpass = $ds_companydis::params::ds_companydis_dbpass
  } else {
    $_ds_companydis_dbpass = $ds_companydis::ds_companydis_dbpass
  }

  if $ds_companydis::ds_companydis_dbhost == undef {
    $_ds_companydis_dbhost = $ds_companydis::params::ds_companydis_dbhost
  } else {
    $_ds_companydis_dbhost = $ds_companydis::ds_companydis_dbhost
  }

  $ds_companydis_deploy_dir = "/fr/deploy/${ds_companydis_app_name}"
  $ds_companydis_frlogdir = "/frlogdir/${ds_companydis_app_name}"
  $ds_companydis_jdk_dir = "/fr/third-party/${_ds_companydis_jdk_version}"
  $ds_companydis_pl_lib = "${ds_companydis_deploy_dir}/pllib"
  $ds_companydis_pl_conf = "${ds_companydis_deploy_dir}/plconf"

  class{'ds_companydis::install':
    require => Class['fr_global::params'],
  }
}
