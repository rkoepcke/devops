class
  ds_companydis::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_companydis_jdk_version = "1.8.0_66"
  $ds_companydis_user = "serviceprod"
  $ds_companydis_group = "engops"
  $ds_companydis_doc_api_url = "http://updateApiUrl=http://sew4-l.ca.firstrain.net:8090"
  $ds_companydis_storage_service = "http://stage.frstorageservice.firstrain.com"
  $ds_companydis_min_mem = "512M"
  $ds_companydis_max_mem = "5120M"
}
