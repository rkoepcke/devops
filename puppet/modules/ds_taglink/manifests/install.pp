class
  ds_taglink::install
inherits
  ds_taglink
{

  $ds_taglink_required_paths = [
    $ds_taglink::ds_taglink_deploy_dir,
    "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF",
    "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/tags",
    "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/lib",
    "${ds_taglink::ds_taglink_deploy_dir}/css",
    "${ds_taglink::ds_taglink_deploy_dir}/img",
    $ds_taglink::ds_taglink_services_dir,
    "${ds_taglink::ds_taglink_services_dir}/bin",
    "${ds_taglink::ds_taglink_services_dir}/logs",
    "${ds_taglink::ds_taglink_services_dir}/conf",
    "${ds_taglink::ds_taglink_services_dir}/conf/Catalina",
    "${ds_taglink::ds_taglink_services_dir}/conf/Catalina/localhost",
    "${ds_taglink::ds_taglink_services_dir}/lib",
    "${ds_taglink::ds_taglink_services_dir}/temp",
    "${ds_taglink::ds_taglink_services_dir}/work",
    $ds_taglink::ds_taglink_frlogdir,
    "${ds_taglink::ds_taglink_frlogdir}/logs",
    "${ds_taglink::ds_taglink_frlogdir}/cpflogs",
  ]

  file { $ds_taglink_required_paths:
    ensure => directory,
    mode   => '0755',
    owner  => $ds_taglink::_ds_taglink_user,
    group  => $ds_taglink::_ds_taglink_group,
    before => Notify['finished-ds_taglink-jar-fetch']
  }

  notify { 'start-ds_taglink-jar-fetch':
    name    =>  'STARTING TagLinkingPipeline jar Fetch',
    message =>  "${ds_taglink::ds_taglink_deploy_dir}/TagLinkingCPFPipeline.jar"
  }

  notify { 'finished-ds_taglink-jar-fetch':
    name    =>  'FINISHED TagLinkingPipeline jar Fetch',
    message =>  "${ds_taglink::ds_taglink_deploy_dir}/TagLinkingCPFPipeline.jar}"
  }

  file { 'fetch TagLinkingPipeline Jar file':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/lib/TagLinkingCPFPipeline.jar",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/TagLinkingCPFPipeline.jar",
    before  =>  Notify['finished-ds_taglink-jar-fetch']
  }

  file { 'fetch SentenceMarker.war file':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/lib/SentenceMarker.jar",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/SentenceMarker.jar",
  }

  file { 'taglinking-lib-fetch':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/tlpipelineLib.tar",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/tlpipelineLib.tar",
  }

  exec { 'extract-taglinking-lib':
    command => "tar -xvf ${fr_global::params::dir_root}/download/tlpipelineLib.tar --directory ${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/lib",
    require =>  File[$ds_taglink_required_paths],
    user    => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    path    => '/usr/bin:/bin',
  }

  File['taglinking-lib-fetch'] -> Exec['extract-taglinking-lib']

  file { 'taglinking-css-fetch':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/tlcss.tar",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/tlcss.tar",
  }

  exec { 'extract-taglinking-css':
    command => "tar -xvf ${fr_global::params::dir_root}/download/tlcss.tar --directory ${ds_taglink::ds_taglink_deploy_dir}/css",
    require =>  File[$ds_taglink_required_paths],
    user    => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    path    => '/usr/bin:/bin',
  }

  File['taglinking-css-fetch'] -> Exec['extract-taglinking-css']

  file { 'taglinking-img-fetch':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${fr_global::params::dir_root}/download/tlimages.tar",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/tlimages.tar",
  }

  exec { 'extract-taglinking-img':
    command => "tar -xvf ${fr_global::params::dir_root}/download/tlimages.tar --directory ${ds_taglink::ds_taglink_deploy_dir}/img",
    require =>  File[$ds_taglink_required_paths],
    user    => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    path    => '/usr/bin:/bin',
  }

  File['taglinking-img-fetch'] -> Exec['extract-taglinking-img']

  file { 'fetch tag linking web.xml file':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/web.xml",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/web.xml",
  }

  file { 'fetch code tag':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/tags/code.tag",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/code.tag",
  }

  file { 'fetch htmlfooter tag':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/tags/htmlfooter.tag",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/htmlfooter.tag",
  }

  file { 'fetch htmlheader tag':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/WEB-INF/tags/htmlheader.tag",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/htmlheader.tag",
  }

  file { 'fetch taglinking JmxViewer jsp':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/JmxViewer.jsp",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/JmxViewer.jsp",
  }

  file { 'fetch taglinking PipelineComponentInfo jsp':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/PipelineComponentInfo.jsp",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/PipelineComponentInfo.jsp",
  }

  file { 'fetch taglinking PipelineInfo jsp':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/PipelineInfo.jsp",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/PipelineInfo.jsp",
  }

  file { 'fetch taglinking PipelineLogin jsp':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/PipelineLogin.jsp",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/PipelineLogin.jsp",
  }

  file { 'fetch taglinking PipelineMonitorXml jsp':
    ensure  =>  'file',
    require =>  File[$ds_taglink_required_paths],
    path    =>  "${ds_taglink::ds_taglink_deploy_dir}/PipelineMonitorXml.jsp",
    owner   =>  $ds_taglink::_ds_taglink_user,
    group   =>  $ds_taglink::_ds_taglink_group,
    source  =>  "puppet:///modules/ds_taglink/PipelineMonitorXml.jsp",
  }

  exec { 'copy-tomcat-bin-files for taglink':
    command => "cp ${ds_taglink::_ds_taglink_tomcat_home}/bin/*.sh ${ds_taglink::ds_taglink_bin}",
    require => [File[$ds_taglink_required_paths],Class['tomcat::install']],
    user    => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${ds_taglink::ds_taglink_bin}/catalina.sh",
  }

  exec { 'copy-tomcat-jar-files for taglink':
    command => "cp ${ds_taglink::_ds_taglink_tomcat_home}/bin/*.jar ${ds_taglink::ds_taglink_bin}",
    require => [File[$ds_taglink_required_paths],Class['tomcat::install']],
    user    => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    path    => '/bin',
    unless  => "/usr/bin/test -f ${ds_taglink::ds_taglink_bin}/bootstrap.jar",
  }

  file { "${ds_taglink::ds_taglink_conf}/server.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/server.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/logging.properties":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/logging.properties.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/catalina.policy":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/catalina.policy.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/context.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/context.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/tomcat-users.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/tomcat-users.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/web.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/web.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/TagLinkingCPFLog4j.properties":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/TagLinkingCPFLog4j.properties.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/TagLinkingPipelineConfig.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/TagLinkingPipelineConfig.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_conf}/Catalina/localhost/frtaglinking.xml":
    ensure  => file,
    require => File[$ds_taglink_required_paths],
    mode    => '0644',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/Catalina/localhost/frtaglinking.xml.erb'),
  }

  file { "${ds_taglink::ds_taglink_bin}/setenv.sh":
    ensure  => file,
    require => [File[$ds_taglink_required_paths],Exec['copy-tomcat-bin-files for taglink']],
    mode    => '0755',
    owner   => $ds_taglink::_ds_taglink_user,
    group   => $ds_taglink::_ds_taglink_group,
    content => template('ds_taglink/setenv.sh.erb'),
  }
  
  file { "/etc/init.d/frtaglinking":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    replace => false,
    content => template('ds_taglink/frtaglinking.erb'),
  }

  if ( $::operatingsystem == "Centos" ) and ( $::operatingsystemmajrelease == "7" ) {
      file { "/etc/systemd/system/frtaglinking.service":
	ensure  => file,
	mode    => '0644',
        require => File['/etc/init.d/frtaglinking'],
	owner   => root,
	group   => root,
	content => template('ds_taglink/frtaglinking.service.erb'),
      }
  }
}
