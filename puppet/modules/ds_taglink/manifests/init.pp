class
  ds_taglink
  (
    $ds_taglink_tomcat_home = undef,
    $ds_taglink_http_port = undef,
    $ds_taglink_https_port = undef,
    $ds_taglink_shutdown_port = undef,
    $ds_taglink_ajp_port = undef,
    $ds_taglink_jdk_version = undef,
    $ds_taglink_user = undef,
    $ds_taglink_group = undef,
    $ds_taglink_mi32 = undef,
    $ds_taglink_entity_solr = undef,
    $ds_taglink_solr = undef,
    $ds_taglink_neo_url = undef,
    $ds_taglink_service_url = undef,
    $ds_taglink_storage_vs = undef,
    $ds_taglink_index_dir = undef,
    $ds_taglink_app_name = "frtaglinking",
  )
{
  include ds_taglink::params

  notify { 'ds_taglink deploy dir':
    message => $_deploy_dir
  }

  if $ds_taglink::ds_taglink_jdk_version == undef {
    $_ds_taglink_jdk_version = $ds_taglink::params::ds_taglink_jdk_version
  } else {
    $_ds_taglink_jdk_version = $ds_taglink::ds_taglink_jdk_version
  }

  if $ds_taglink::ds_taglink_shutdown_port == undef {
    $_ds_taglink_shutdown_port = $ds_taglink::params::ds_taglink_shutdown_port
  } else {
    $_ds_taglink_shutdown_port = $ds_taglink::ds_taglink_shutdown_port
  }

  if $ds_taglink::ds_taglink_http_port == undef {
    $_ds_taglink_http_port = $ds_taglink::params::ds_taglink_http_port
  } else {
    $_ds_taglink_http_port = $ds_taglink::ds_taglink_http_port
  }

  if $ds_taglink::ds_taglink_https_port == undef {
    $_ds_taglink_https_port = $ds_taglink::params::ds_taglink_https_port
  } else {
    $_ds_taglink_https_port = $ds_taglink::ds_taglink_https_port
  }

  if $ds_taglink::ds_taglink_ajp_port == undef {
    $_ds_taglink_ajp_port = $ds_taglink::params::ds_taglink_ajp_port
  } else {
    $_ds_taglink_ajp_port = $ds_taglink::ds_taglink_ajp_port
  }

  if $ds_taglink::ds_taglink_tomcat_home == undef {
    $_ds_taglink_tomcat_home = $ds_taglink::params::ds_taglink_tomcat_home
  } else {
    $_ds_taglink_tomcat_home = $ds_taglink::ds_taglink_tomcat_home
  }

  if $ds_taglink::ds_taglink_user == undef {
    $_ds_taglink_user = $ds_taglink::params::ds_taglink_user
  } else {
    $_ds_taglink_user = $ds_taglink::ds_taglink_user
  }

  if $ds_taglink::ds_taglink_group == undef {
    $_ds_taglink_group = $ds_taglink::params::ds_taglink_group
  } else {
    $_ds_taglink_group = $ds_taglink::ds_taglink_group
  }

  if $ds_taglink::ds_taglink_mi32 == undef {
    $_ds_taglink_mi32 = $ds_taglink::params::ds_taglink_mi32
  } else {
    $_ds_taglink_mi32 = $ds_taglink::ds_taglink_mi32
  }

  if $ds_taglink::ds_taglink_solr == undef {
    $_ds_taglink_solr = $ds_taglink::params::ds_taglink_solr
  } else {
    $_ds_taglink_solr = $ds_taglink::ds_taglink_solr
  }

  if $ds_taglink::ds_taglink_entity_solr == undef {
    $_ds_taglink_entity_solr = $ds_taglink::params::ds_taglink_entity_solr
  } else {
    $_ds_taglink_entity_solr = $ds_taglink::ds_taglink_entity_solr
  }

  if $ds_taglink::ds_taglink_neo_url == undef {
    $_ds_taglink_neo_url = $ds_taglink::params::ds_taglink_neo_url
  } else {
    $_ds_taglink_neo_url = $ds_taglink::ds_taglink_neo_url
  }

  if $ds_taglink::ds_taglink_service_url == undef {
    $_ds_taglink_service_url = $ds_taglink::params::ds_taglink_service_url
  } else {
    $_ds_taglink_service_url = $ds_taglink::ds_taglink_service_url
  }

  if $ds_taglink::ds_taglink_storage_vs == undef {
    $_ds_taglink_storage_vs = $ds_taglink::params::ds_taglink_storage_vs
  } else {
    $_ds_taglink_storage_vs = $ds_taglink::ds_taglink_storage_vs
  }

  if $ds_taglink::ds_taglink_index_dir == undef {
    $_ds_taglink_index_dir = $ds_taglink::params::ds_taglink_index_dir
  } else {
    $_ds_taglink_index_dir = $ds_taglink::ds_taglink_index_dir
  }

  $ds_taglink_deploy_dir = "/fr/deploy/${ds_taglink_app_name}"
  $ds_taglink_services_dir = "/fr/services/${ds_taglink_app_name}"
  $ds_taglink_frlogdir = "/frlogdir/${ds_taglink_app_name}"
  $ds_taglink_conf = "/fr/services/${ds_taglink_app_name}/conf"
  $ds_taglink_bin = "/fr/services/${ds_taglink_app_name}/bin"
  $ds_taglink_jdk_dir = "/fr/third-party/${_ds_taglink_jdk_version}"

  class{'ds_taglink::install':
    require => Class['fr_global::params'],
  }

}
