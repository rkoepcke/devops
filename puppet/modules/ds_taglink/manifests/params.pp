class
  ds_taglink::params
{
  include fr_global::params
  
  $deploy_dir_default = $fr_global::params::dir_bin

  if $::operatingsystem == 'Darwin' {
    fail('Mac OS (and related) are not currently supported')
  }

  $ds_taglink_shutdown_port = 8005
  $ds_taglink_http_port = 8080
  $ds_taglink_https_port = 8443
  $ds_taglink_ajp_port = 8009
  $ds_taglink_jdk_version = "1.6.0_30"
  $ds_taglink_user = "serviceprod"
  $ds_taglink_group = "serviceprod"
  $ds_taglink_mi32 = "PSPYDB2-RW"
  $ds_taglink_entity_solr = "solr.froovy.firstrain.com"
  $ds_taglink_solr = "10.10.12.110:8080"
  $ds_taglink_neo_url = "http://sneoapp1-rl.ca.firstrain.net:46300/frneo/"
  $ds_taglink_index_dir = "/fr/services/frtaglink/temp"
  $ds_taglink_service_url = "sdsweb1-l.ca.firstrain.net:7080"
}
